package com.miroslav727.euniverzitet.util;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.miroslav727.euniverzitet.model.Event;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleEmitter;

public class EventValueEventListener implements ValueEventListener {

    private SingleEmitter<List<Event>> emitter;
    private String uId;

    public EventValueEventListener(SingleEmitter<List<Event>> emitter, String uId) {
        this.emitter = emitter;
        this.uId = uId;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        List<Event> events = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            Event event = snapshot.getValue(Event.class);
            event.setId(snapshot.getKey());
            //adds any event if there's no user id specified, or events created by user if user id
            //is specified
            if (uId == null || uId.equals(event.getCreatorId())) {
                events.add(event);
            }
        }
        emitter.onSuccess(events);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        emitter.onError(databaseError.toException());
    }
}
