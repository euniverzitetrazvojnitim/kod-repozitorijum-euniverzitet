package com.miroslav727.euniverzitet.util;

import android.text.TextUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;

public class ExactSearchHandler {

    private String lastSearch;
    private boolean exactSearch;
    private boolean handled;

    public ExactSearchHandler(String lastSearch, boolean exactSearch) {
        this.setLastSearch(lastSearch);
        this.setExactSearch(exactSearch);

    }

    public String getLastSearch() {
        return lastSearch;
    }

    public void setLastSearch(String lastSearch) {
        this.lastSearch = lastSearch;
    }

    public boolean isExactSearch() {
        return exactSearch;
    }

    public void setExactSearch(boolean exactSearch) {
        this.exactSearch = exactSearch;
    }

    public boolean isHandled() {
        return handled;
    }

    public void setHandled(boolean handled) {
        this.handled = handled;
    }

    public void performSearch(int actionId, TextInputEditText courseSearch,
                                 DatabaseReference databaseReference, TextView emptyView,
                                 ListView courseSearchResults, RefreshAdapter adapter,
                                 AppCompatActivity activity) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            handled = true;
            String searchText = courseSearch.getText().toString().trim();
            if (TextUtils.isEmpty(searchText)) {
                Util.hideEmptyListAndShowEmptyView(emptyView, courseSearchResults);
            } else {
                exactSearch = Util.searchCourseDatabaseForExactResult(searchText,
                        null, databaseReference, emptyView, courseSearchResults,
                        adapter);
                lastSearch = searchText;
            }
            Util.closeKeyboard(activity);
        }
    }

}
