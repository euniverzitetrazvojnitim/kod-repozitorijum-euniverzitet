package com.miroslav727.euniverzitet.util;

import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.Course;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_CODE;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_NAME;

public class CourseValueEventListener implements ValueEventListener {

    private ArrayList<Course> courses;
    private boolean readyToDraw;
    private String searchText;
    private Query query;
    private TextView emptyView;
    private ListView courseListView;
    private RefreshAdapter<?> courseAdapter;


    public CourseValueEventListener(ArrayList<Course> courses, Query query, String searchText,
                                    TextView emptyView, ListView courseListView,
                                    RefreshAdapter<?> courseAdapter) {
        this.courses = courses;
        this.searchText = searchText;
        this.query = query;
        this.emptyView = emptyView;
        this.courseListView = courseListView;
        this.courseAdapter = courseAdapter;
        this.readyToDraw = query == null;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            Map<String, String> coursePairs = (HashMap<String,String>) snapshot.getValue();
            Course course = new Course();
            course.setId(snapshot.getKey());
            for (Map.Entry<String, String> mappedCourse : coursePairs.entrySet()) {
                switch (mappedCourse.getKey()) {
                    case COURSES_CHILD_CODE:
                        course.setCode(mappedCourse.getValue());
                        break;
                    case COURSES_CHILD_NAME:
                        course.setName(mappedCourse.getValue());
                        break;
                    default: break;
                }
            }
            if (!readyToDraw || !courses.contains(course)) {
                courses.add(course);
            }
        }
        if (readyToDraw) {
            Util.setViewVisibilityAndUpdateAdapter
                    (courses, emptyView, courseListView, courseAdapter);
        } else {
            query.addListenerForSingleValueEvent
                    (new CourseValueEventListener(courses, null, searchText,
                            emptyView, courseListView, courseAdapter));
        }


    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(CourseValueEventListener.class.getSimpleName(), databaseError.getDetails());
    }
}
