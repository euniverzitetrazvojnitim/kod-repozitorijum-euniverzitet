package com.miroslav727.euniverzitet.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.view.ActionMode;
import androidx.core.os.ConfigurationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.CoursesActivity;
import com.miroslav727.euniverzitet.adminModule.LocationsActivity;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.adminModule.UsersActivity;
import com.miroslav727.euniverzitet.eventsModule.EventCreateActivity;
import com.miroslav727.euniverzitet.eventsModule.EventDisplayActivity;
import com.miroslav727.euniverzitet.eventsModule.UserCreatedEventsActivity;
import com.miroslav727.euniverzitet.login.ChangePasswordActivity;
import com.miroslav727.euniverzitet.login.LoginScreenActivity;
import com.miroslav727.euniverzitet.login.RegistrationActivity;
import com.miroslav727.euniverzitet.model.Answer;
import com.miroslav727.euniverzitet.model.ChatMeta;
import com.miroslav727.euniverzitet.model.Course;
import com.miroslav727.euniverzitet.model.Event;
import com.miroslav727.euniverzitet.model.Location;
import com.miroslav727.euniverzitet.model.Question;
import com.miroslav727.euniverzitet.model.Test;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.testModule.TestsDisplayActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Maybe;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.eventsModule.UserCreatedEventsActivity.FLAG_EVENT_ID;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_USER_ROLE;
import static com.miroslav727.euniverzitet.util.Constants.ANSWERS_CHILD_QUESTION;
import static com.miroslav727.euniverzitet.util.Constants.ANSWERS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.CHATS_META_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_CODE;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_LESSONS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_NAME;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_STUDENTS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_TEACHERS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.EVENTS_CHILD_INVITEES;
import static com.miroslav727.euniverzitet.util.Constants.EVENTS_CHILD_TYPE;
import static com.miroslav727.euniverzitet.util.Constants.EVENTS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.FIREBASE_API_KEY;
import static com.miroslav727.euniverzitet.util.Constants.FIREBASE_APP_ID;
import static com.miroslav727.euniverzitet.util.Constants.FIREBASE_DATABASE_URL;
import static com.miroslav727.euniverzitet.util.Constants.FIREBASE_PROJECT_ID;
import static com.miroslav727.euniverzitet.util.Constants.LOCATIONS_CHILD_EVENTS;
import static com.miroslav727.euniverzitet.util.Constants.LOCATIONS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.MILLIS_IN_DAY;
import static com.miroslav727.euniverzitet.util.Constants.MILLIS_IN_HOUR;
import static com.miroslav727.euniverzitet.util.Constants.MILLIS_IN_MINUTE;
import static com.miroslav727.euniverzitet.util.Constants.QUESTIONS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.QUESTION_CHILD_CORRECT_ANSWERS;
import static com.miroslav727.euniverzitet.util.Constants.QUESTION_CHILD_INCORRECT_ANSWERS;
import static com.miroslav727.euniverzitet.util.Constants.QUESTION_CHILD_TESTS;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_ADMINISTRATOR;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_DELETED_USER;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_NON_TEACHING_STAFF;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_STUDENT;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEACHING_STAFF;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEMP_USER;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_ACCEPTED;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_INVITED;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_REJECTED;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_CHILD_COURSE;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_CHILD_QUESTIONS;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_CLASSROOM;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_CONFERENCE_ROOM;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_EXAM;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_FREE_ANSWER;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_INVALID;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_LESSON;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_MEETING_ROOM;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_MIDTERM;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_MULTIPLE_CORRECT;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_OTHER;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_PRIVATE_EVENT;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_PUBLIC_EVENT;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_SINGLE_CORRECT;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_WEEKLY;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_CONTACTS;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_EMAIL;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_EVENTS;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_PASSED_TESTS;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_ROLE;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_STUDIES;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_TEACHES;
import static com.miroslav727.euniverzitet.util.Constants.USERS_TABLE;

public class Util {

    public static void closeKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //region mappers
    public static String roleMapper(Context context, int roleId) {
        switch (roleId) {
            case ROLE_STUDENT:
                return context.getString(R.string.student);
            case ROLE_ADMINISTRATOR:
                return context.getString(R.string.administrator);
            case ROLE_TEACHING_STAFF:
                return context.getString(R.string.teaching_staff);
            case ROLE_NON_TEACHING_STAFF:
                return context.getString(R.string.non_teaching_staff);
            case ROLE_TEMP_USER:
                return context.getString(R.string.temp_user);
            case ROLE_DELETED_USER:
                return context.getString(R.string.deleted_user);
            default:
                return context.getString(R.string.all);
        }
    }

    public static String locationTypeMapper(Context context, int typeId) {
        switch (typeId) {
            case TYPE_CLASSROOM:
                return context.getString(R.string.classroom);
            case TYPE_MEETING_ROOM:
                return context.getString(R.string.meeting_room);
            case TYPE_CONFERENCE_ROOM:
                return context.getString(R.string.conference_room);
            case TYPE_OTHER:
                return context.getString(R.string.other_location);
            default:
                return context.getString(R.string.all);
        }
    }

    public static String testTypeMapper(Context context, int typeId) {
        switch (typeId) {
            case TYPE_WEEKLY:
                return context.getString(R.string.weekly);
            case TYPE_MIDTERM:
                return context.getString(R.string.midterm);
            case TYPE_EXAM:
                return context.getString(R.string.exam);
            default:
                return context.getString(R.string.all);
        }
    }

    public static String questionTypeMapper(Context context, int typeId) {
        switch (typeId) {
            case TYPE_FREE_ANSWER:
                return context.getString(R.string.free_answer);
            case TYPE_SINGLE_CORRECT:
                return context.getString(R.string.single_correct);
            case TYPE_MULTIPLE_CORRECT:
                return context.getString(R.string.multiple_correct);
            default:
                return context.getString(R.string.all);
        }
    }

    public static String eventTypeMapper(Context context, int typeId) {
        switch (typeId) {
            case TYPE_PUBLIC_EVENT:
                return context.getString(R.string.public_event);
            case TYPE_PRIVATE_EVENT:
                return context.getString(R.string.private_event);
            case TYPE_LESSON:
                return context.getString(R.string.lesson_or_exercise);
            default:
                return context.getString(R.string.all);
        }
    }

    public static String eventAttendanceMapper(Context context, int position) {
        switch (position) {
            default:
                return null;
            case 1:
                return STATUS_INVITED;
            case 2:
                return STATUS_ACCEPTED;
            case 3:
                return STATUS_REJECTED;
        }
    }
    //endregion

    //region Firebase
    private static FirebaseOptions buildFirebaseOptions() {
        return new FirebaseOptions.Builder()
                .setApiKey(FIREBASE_API_KEY)
                .setDatabaseUrl(FIREBASE_DATABASE_URL)
                .setProjectId(FIREBASE_PROJECT_ID)
                .setApplicationId(FIREBASE_APP_ID)
                .build();
    }

    public static FirebaseApp initialiseFirebaseApp(Context context, String appIdentifier) {
        return FirebaseApp.initializeApp(context, buildFirebaseOptions(), appIdentifier);
    }

    public static FirebaseApp getFirebaseApp(Context context, String appIdentifier) {
        try {
            return FirebaseApp.getInstance(appIdentifier);
        } catch (IllegalStateException e) {
            return initialiseFirebaseApp(context, appIdentifier);
        }
    }

    public static void destroyFirebaseApp(FirebaseApp firebaseApp) {
        if (firebaseApp != null) firebaseApp.delete();
    }

    public static Task<Void> removeItemFromDatabaseTable(@NonNull DatabaseReference databaseReference,
                                                   @NonNull String tableName,
                                                   @NonNull String itemId) {
        return databaseReference.child(tableName).child(itemId).removeValue();
    }

    public static void removeUserFromDatabaseTable(@NonNull String uid) {
        performUserCleanup(uid).subscribeWith(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {}

            @Override
            public void onComplete() {
                getRootReference().child(USERS_TABLE).child(uid).removeValue();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(Util.class.getSimpleName(), e.getMessage());
            }
        });
    }

    private static Completable performUserCleanup(String uid) {
        //TODO 30.07 extend this once further cleanup is necessary
        return removeUserFromAllCourses(uid).andThen(removeUserFromAllEvents(uid)
                .andThen(removeUserFromAllChats(uid)));
    }

    private static Completable removeUserFromAllCourses(String uId) {
        return Completable.create(emitter -> getRootReference().child(USERS_TABLE)
                .child(uId).child(USERS_CHILD_STUDIES)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            removeUserFromCourse(snapshot.getKey(), uId, true);
                        }
                        getRootReference().child(USERS_TABLE).child(uId)
                                .child(USERS_CHILD_TEACHES)
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                            removeUserFromCourse(snapshot.getKey(), uId, false);
                                        }
                                        emitter.onComplete();
                                    }

                                    @Override
                                    public void onCancelled
                                            (@NonNull DatabaseError databaseError) {
                                        emitter.onError(databaseError.toException());
                                    }
                                });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                }));
    }

    private static void removeUserFromCourse(String courseId, String uId, boolean studies) {
        final String column = studies ? COURSES_CHILD_STUDENTS : COURSES_CHILD_TEACHERS;
        getRootReference()
                .child(COURSES_TABLE).child(courseId).child(column).child(uId).removeValue();
    }

    private static Completable removeUserFromAllEvents(String uId) {
        return Completable.create(emitter -> getRootReference().child(USERS_TABLE)
                .child(uId).child(USERS_CHILD_EVENTS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            removeUserFromEvent(snapshot.getKey(), uId);
                        }
                        emitter.onComplete();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                }));
    }

    private static void removeUserFromEvent(String eventId, String uId) {
        getRootReference().child(EVENTS_TABLE).child(eventId).child(EVENTS_CHILD_INVITEES)
                .child(uId).removeValue();
    }

    private static Completable removeUserFromAllChats(String uId) {
        return Completable.create(emitter -> getRootReference().child(USERS_TABLE)
                .child(uId).child(CHATS_META_TABLE)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            removeUserFromChat(snapshot.getKey(), uId);
                        }
                        emitter.onComplete();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                }));
    }

    private static void removeUserFromChat(String otherUid, String uId) {
        getRootReference().child(USERS_TABLE).child(otherUid).child(CHATS_META_TABLE)
                .child(uId).removeValue();
    }

    private static void removeUserFromContacts(String deletedUid, String uId) {
        getRootReference().child(USERS_TABLE).child(uId).child(USERS_CHILD_CONTACTS)
                .child(deletedUid).removeValue();
    }

    public static DatabaseReference getRootReference() {
        return FirebaseDatabase.getInstance().getReference();
    }

    /**
     * Gets the Observable which emits a single user object based on the user found in database
     * under id from parameter uid
     * @param uid the id of user to observe
     * @return Observable which emits the retrieved user
     */
    public static Single<User> observeUserFromDatabase(String uid) {
        return Single.create(emitter ->
                getRootReference().child(USERS_TABLE).child(uid)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                User user = dataSnapshot.getValue(User.class);
                                user.setUid(dataSnapshot.getKey());
                                emitter.onSuccess(user);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        })
        );
    }

    /**
     * Gets the Maybe which emits a single user object based on the user found in database
     * or nothing if no user is found
     * @param email email of searched user
     * @return Maybe which emits the retrieved user
     */
    public static Maybe<User> observeUserByEmail(String email) {
        return Maybe.create(emitter ->
                getRootReference().child(USERS_TABLE).orderByChild(USERS_CHILD_EMAIL).equalTo(email)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChildren()) {
                                    DataSnapshot snap = dataSnapshot.getChildren().iterator().next();
                                    User user = snap.getValue(User.class);
                                    user.setUid(snap.getKey());
                                    emitter.onSuccess(user);
                                } else {
                                    emitter.onComplete();
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        })
        );
    }

    public static void addUserToDatabase(String uid, String email, int role, String pass) {
        User user = new User(email, role, uid, pass, true, getCurrentLocale());
        getRootReference().child(USERS_TABLE).child(uid).setValue(user);
    }

    /**
     * Replaces the user in database with the user object from parameter
     * @param user the user to be edited
     */
    public static void editUser(User user) {
        getRootReference().child(USERS_TABLE).child(user.getUid()).setValue(user);
    }

    public static Single<Integer> getUserRole(String uId) {
        return observeUserFromDatabase(uId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(User::getRole);
    }

    public static Task<Void> editUserRole(String uid, int role) {
        return getRootReference().child(USERS_TABLE).child(uid).child(USERS_CHILD_ROLE)
                .setValue(role);
    }

    public static Single<Course> observeCourseById(String courseId) {
        return Single.create(emitter ->
                getRootReference().child(COURSES_TABLE).child(courseId)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Course course = dataSnapshot.getValue(Course.class);
                                course.setId(courseId);
                                emitter.onSuccess(course);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        })
        );
    }

    public static Single<String> getCourseCode(String courseId) {
        return observeCourseById(courseId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(Course::getCode);
    }

    public static Single<String> getCourseIdentifier(String courseId) {
        return observeCourseById(courseId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(Course::formCourseIdentifier);
    }

    public static boolean safeSearchCourseDatabaseAndListResults(String searchText,
                                                              ActionMode actionMode,
                                                              DatabaseReference databaseReference,
                                                              TextView emptyView,
                                                              ListView courseListView,
                                                              RefreshAdapter<?> courseAdapter) {
        if (TextUtils.isEmpty(searchText)) {
            hideEmptyListAndShowEmptyView(emptyView, courseListView);
        } else {
            return searchCourseDatabaseAndListResults(searchText, actionMode, databaseReference,
                    emptyView,
                    courseListView, courseAdapter);
        }
        return false;
    }

    /**
     * @return false to indicate exact search mode is not on
     */
    public static boolean searchCourseDatabaseAndListResults(String searchText,
                                                             ActionMode actionMode,
                                                             DatabaseReference databaseReference,
                                                             TextView emptyView,
                                                             ListView courseListView,
                                                             RefreshAdapter<?> courseAdapter) {
        if (actionMode != null) actionMode.finish();
        ArrayList<Course> courses = new ArrayList<>();
        Query searchByCode = databaseReference.child(COURSES_TABLE).orderByChild(COURSES_CHILD_CODE)
                .startAt(searchText)
                .endAt(searchText+"\uf8ff");
        Query searchByName = databaseReference.child(COURSES_TABLE).orderByChild(COURSES_CHILD_NAME)
                .startAt(searchText)
                .endAt(searchText+"\uf8ff");
        searchByCode.addListenerForSingleValueEvent
                (new CourseValueEventListener(courses, searchByName, searchText, emptyView,
                        courseListView, courseAdapter));
        return false;
    }

    /**
     * @return true to indicate exact search mode is on
     */
    public static boolean searchCourseDatabaseForExactResult(String searchText,
                                                    ActionMode actionMode,
                                                    DatabaseReference databaseReference,
                                                    TextView emptyView,
                                                    ListView courseListView,
                                                    RefreshAdapter<?> courseAdapter) {
        if (actionMode != null) actionMode.finish();
        ArrayList<Course> courses = new ArrayList<>();
        Query searchByCode = databaseReference.child(COURSES_TABLE).orderByChild(COURSES_CHILD_CODE)
                .equalTo(searchText);
        Query searchByName = databaseReference.child(COURSES_TABLE).orderByChild(COURSES_CHILD_NAME)
                .equalTo(searchText);
        searchByCode.addListenerForSingleValueEvent
                (new CourseValueEventListener(courses, searchByName, searchText, emptyView,
                        courseListView, courseAdapter));
        return true;
    }

    public static SearchMetadata onSearchTextChanged(TextInputEditText searchInput,
                                                     ActionMode actionMode,
                                                     String lastSearch, boolean exactSearch,
                                                     DatabaseReference databaseReference,
                                                     TextView emptyView,
                                                     ListView searchResults,
                                                     RefreshAdapter<?> adapter) {
        SearchMetadata result = new SearchMetadata(lastSearch, exactSearch);
        String searchText = searchInput.getText().toString().trim();
        if (lastSearch != null) {
            if (!lastSearch.equals(searchText) || !exactSearch) {
                result.setExactSearch(Util.safeSearchCourseDatabaseAndListResults(searchText,
                        actionMode, databaseReference, emptyView, searchResults, adapter));
            } else {
                result.setExactSearch(Util.searchCourseDatabaseForExactResult(searchText,
                        actionMode, databaseReference, emptyView, searchResults,
                        adapter));
                result.setLastSearch(searchText);
            }
        }
        result.setLastSearch(searchText);
        return result;
    }

    public static Single<List<Course>> observeTeachersCourses(String uid) {
        return Single.create(emitter -> getRootReference().child(COURSES_TABLE)
                .orderByChild(COURSES_CHILD_TEACHERS.concat("/"+uid))
                .equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Course> courses = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Map<String, String> coursePairs =
                            (HashMap<String, String>) snapshot.getValue();
                    Course course = new Course();
                    course.setId(snapshot.getKey());
                    for (Map.Entry<String, String> mappedCourse : coursePairs.entrySet()) {
                        switch (mappedCourse.getKey()) {
                            case COURSES_CHILD_CODE:
                                course.setCode(mappedCourse.getValue());
                                break;
                            case COURSES_CHILD_NAME:
                                course.setName(mappedCourse.getValue());
                                break;
                            default:
                                break;
                        }
                    }
                    courses.add(course);
                }
                emitter.onSuccess(courses);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                emitter.onError(databaseError.toException());
            }
        }));
    }

    public static Single<List<Course>> observeStudentsCourses(String uid) {
        return Single.create(emitter -> getRootReference().child(COURSES_TABLE)
                .orderByChild(COURSES_CHILD_STUDENTS.concat("/"+uid))
                .equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        List<Course> courses = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Course course = snapshot.getValue(Course.class);
                            course.setId(snapshot.getKey());
                            courses.add(course);
                        }
                        emitter.onSuccess(courses);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                }));
    }

    public static Single<List<Test>> observeCoursesTests(String courseId) {
        return Single.create(emitter -> getRootReference().child(TESTS_TABLE)
                .orderByChild(TESTS_CHILD_COURSE).equalTo(courseId).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                List<Test> tests = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Test test = snapshot.getValue(Test.class);
                                    test.setId(snapshot.getKey());
                                    tests.add(test);
                                }
                                emitter.onSuccess(tests);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        }
                ));
    }

    public static Single<List<Test>> observeAllTests() {
        return Single.create(emitter -> getRootReference().child(TESTS_TABLE)
                .orderByChild(TESTS_CHILD_COURSE).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                List<Test> tests = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Test test = snapshot.getValue(Test.class);
                                    test.setId(snapshot.getKey());
                                    tests.add(test);
                                }
                                emitter.onSuccess(tests);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        }
                ));
    }

    public static Single<List<Test>> observeFilteredCoursesTests(String courseId, int testType,
                                                                 Boolean active) {
        return Single.create(emitter -> getRootReference().child(TESTS_TABLE)
                .orderByChild(TESTS_CHILD_COURSE).equalTo(courseId).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                List<Test> tests = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Test test = snapshot.getValue(Test.class);
                                    test.setId(snapshot.getKey());
                                    if ((testType == -1 || test.getType() == testType) &&
                                            (active == null || active == test.isActive())) {
                                        tests.add(test);
                                    }
                                }
                                emitter.onSuccess(tests);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        }
                ));
    }

    public static Single<List<Test>> observeFilteredAllTests(int testType,
                                                             Boolean active) {
        return Single.create(emitter -> getRootReference().child(TESTS_TABLE)
                .orderByChild(TESTS_CHILD_COURSE).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                List<Test> tests = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Test test = snapshot.getValue(Test.class);
                                    test.setId(snapshot.getKey());
                                    if ((testType == -1 || test.getType() == testType) &&
                                            (active == null || active == test.isActive())) {
                                        tests.add(test);
                                    }
                                }
                                emitter.onSuccess(tests);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        }
                ));
    }

    public static Single<Test> observeTestById(String testId) {
        return Single.create(emitter ->
                getRootReference().child(TESTS_TABLE).child(testId)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Test test = dataSnapshot.getValue(Test.class);
                                test.setId(testId);
                                emitter.onSuccess(test);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        })
        );
    }

    public static Single<List<String>> observePassedTestIds(String userId) {
        return Single.create(emitter ->
                getRootReference().child(USERS_TABLE).child(userId).child(USERS_CHILD_PASSED_TESTS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        List<String> passedTestIds = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            passedTestIds.add(snapshot.getKey());
                        }
                        emitter.onSuccess(passedTestIds);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                }));
    }

    public static Single<Map<String, Double>> observeUserPassedTests(String userId) {
        return Single.create(emitter ->
                getRootReference().child(USERS_TABLE).child(userId).child(USERS_CHILD_PASSED_TESTS)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Map<String, Double> passedTests = new HashMap<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    passedTests.put(snapshot.getKey(),
                                            Double.parseDouble(snapshot.getValue().toString()));
                                }
                                emitter.onSuccess(passedTests);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        }));
    }

    public static Single<Question> observeQuestionById(String questionId) {
        return Single.create(emitter ->
                getRootReference().child(QUESTIONS_TABLE).child(questionId)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Question question = dataSnapshot.getValue(Question.class);
                                question.setId(questionId);
                                emitter.onSuccess(question);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        })
        );
    }

    public static Single<List<Question>> observeTestsQuestions(String testId) {
        return Single.create(emitter -> getRootReference().child(QUESTIONS_TABLE)
                .orderByChild(QUESTION_CHILD_TESTS.concat("/"+testId)).equalTo(true)
                .addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                List<Question> questions = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Question question = snapshot.getValue(Question.class);
                                    question.setId(snapshot.getKey());
                                    questions.add(question);
                                }
                                emitter.onSuccess(questions);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        }
                ));
    }

    public static Single<Integer> getTestNumberOfQuestions(String testId) {
        return observeTestsQuestions(testId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(List::size);
    }

    public static Single<List<Answer>> observeQuestionsAnswers(String questionId) {
        return Single.create(emitter -> getRootReference().child(ANSWERS_TABLE)
                .orderByChild(ANSWERS_CHILD_QUESTION).equalTo(questionId)
                .addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                List<Answer> answers = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Answer answer = snapshot.getValue(Answer.class);
                                    answer.setId(snapshot.getKey());
                                    answers.add(answer);
                                }
                                emitter.onSuccess(answers);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        }
                ));
    }

    public static Single<List<Test>> observeQuestionsTests(String questionId) {
        return Single.create(emitter -> getRootReference().child(TESTS_TABLE)
                .orderByChild(TESTS_CHILD_QUESTIONS.concat("/"+questionId)).equalTo(true)
                .addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                List<Test> tests = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Test test = snapshot.getValue(Test.class);
                                    test.setId(snapshot.getKey());
                                    tests.add(test);
                                }
                                emitter.onSuccess(tests);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        }
                ));
    }

    public static Task<Void> removeQuestion(String questionId) {
        return getRootReference().child(QUESTIONS_TABLE).child(questionId)
                .removeValue();
    }

    public static void removeQuestionAndItsAnswers(String questionId, Scheduler thread,
                                                   CompositeDisposable compositeDisposable) {
        removeQuestion(questionId).addOnSuccessListener(aVoid ->
            removeQuestionAnswers(questionId, thread, compositeDisposable)
        );
    }

    public static Completable removeQuestionAnswers(String questionId, Scheduler thread,
                                             CompositeDisposable compositeDisposable) {
        return Completable.create(emitter -> compositeDisposable.add(
                observeQuestionsAnswers(questionId)
                        .observeOn(thread)
                        .subscribeOn(thread)
                        .subscribe(answers -> {
                            for (Answer answer : answers) {
                                removeAnswer(answer.getId());
                            }
                            emitter.onComplete();
                        })
        ));

    }

    public static void removeAnswer(String answerId) {
        getRootReference().child(ANSWERS_TABLE).child(answerId).removeValue();
    }

    public static Single<List<Answer>> observeQuestionsFilteredAnswers(String questionId,
                                                                      List<String> filteredIds) {
        return observeQuestionsAnswers(questionId).map(answers -> {
            List<Answer> filteredAnswers = new ArrayList<>();
            for (Answer answer : answers) {
                if (filteredIds.contains(answer.getId())) {
                    filteredAnswers.add(answer);
                }
            }
            return filteredAnswers;
        });
    }

    public static Single<List<String>> getQuestionFilteredAnswerIds(String questionId,
                                                                  boolean correct) {
        return Single.create(emitter -> getRootReference().child(QUESTIONS_TABLE).child(questionId)
                .child(correct ? QUESTION_CHILD_CORRECT_ANSWERS : QUESTION_CHILD_INCORRECT_ANSWERS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> filteredAnswerIds = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    filteredAnswerIds.add(snapshot.getKey());
                }
                emitter.onSuccess(filteredAnswerIds);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                emitter.onError(databaseError.toException());
            }
        }));
    }

    public static Task<Void> saveTestResults(String testId, String userId, String result) {
        return getRootReference().child(USERS_TABLE).child(userId).child(USERS_CHILD_PASSED_TESTS)
                .child(testId).setValue(result);
    }

    public static Completable rxSaveTestResults(String testId, String userId, String result) {
        return Completable.create(emitter ->
                saveTestResults(testId, userId, result)
                .addOnCompleteListener(task -> emitter.onComplete()));
    }

    public static Completable removeQuestionFromTests(String questionId,
                                               CompositeDisposable compositeDisposable) {
        return Completable.create(emitter -> {
            Scheduler thread = Schedulers.newThread();
            compositeDisposable.add(Util.observeQuestionsTests(questionId)
                    .observeOn(thread)
                    .subscribeOn(thread)
                    .subscribe(tests -> {
                        for (Test test : tests) {
                            removeQuestionFromTest(questionId, test.getId());
                        }
                        emitter.onComplete();
                    })
            );
        });

    }

    public static void removeQuestionFromTest(String questionId, String testId) {
        Util.getRootReference().child(TESTS_TABLE).child(testId).child(TESTS_CHILD_QUESTIONS)
                .child(questionId).removeValue();
    }

    public static Single<Location> observeLocationById(String locationId) {
        return Single.create(emitter ->
                getRootReference().child(LOCATIONS_TABLE).child(locationId)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Location location = dataSnapshot.getValue(Location.class);
                                location.setId(locationId);
                                emitter.onSuccess(location);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        })
        );
    }

    public static Single<Event> observeEventById(String eventId) {
        return Single.create(emitter ->
                getRootReference().child(EVENTS_TABLE).child(eventId)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Event event = dataSnapshot.getValue(Event.class);
                                event.setId(eventId);
                                emitter.onSuccess(event);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        })
        );
    }

    public static Single<List<User>> observeCoursesStudents(String courseId) {
        return Single.create(emitter -> getRootReference().child(USERS_TABLE)
                .orderByChild(USERS_CHILD_STUDIES.concat("/"+courseId))
                .equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        List<User> students = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            User student = snapshot.getValue(User.class);
                            student.setUid(snapshot.getKey());
                            students.add(student);
                        }
                        emitter.onSuccess(students);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                }));
    }

    public static Single<List<String>> observeCoursesStudentIds(String courseId) {
        return observeCoursesStudents(courseId).map(students -> {
            List<String> ids = new ArrayList<>();
            for (User student : students) {
                ids.add(student.getUid());
            }
            return ids;
        });
    }

    public static void addEventToUser(String uId, String eventId, String status) {
        Util.getRootReference().child(USERS_TABLE)
                .child(uId)
                .child(USERS_CHILD_EVENTS)
                .child(eventId)
                .setValue(status);
    }

    public static void addUserToEvent(String eventId, String uId, String status) {
        Util.getRootReference().child(EVENTS_TABLE)
                .child(eventId)
                .child(EVENTS_CHILD_INVITEES)
                .child(uId)
                .setValue(status);
    }

    public static void inviteUserToEvent(String eventId, String uId, String status) {
        addUserToEvent(eventId, uId, status);
        addEventToUser(uId, eventId, status);
    }

    public static Single<List<String>> observeAllEventInviteesIds(String eventId) {
        return Single.create(emitter -> Util.getRootReference()
                .child(EVENTS_TABLE)
                .child(eventId)
                .child(EVENTS_CHILD_INVITEES)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        List<String> ids = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            ids.add(snapshot.getKey());
                        }
                        emitter.onSuccess(ids);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                })
        );
    }

    public static Single<List<Event>> observeAllEventsOfType(int eventType) {
        return Single.create(emitter -> {
            if (eventType == TYPE_INVALID) {
                Util.getRootReference()
                        .child(EVENTS_TABLE)
                        .orderByChild(EVENTS_CHILD_TYPE)
                        .addListenerForSingleValueEvent(new EventValueEventListener(emitter, null));
            } else {
                Util.getRootReference()
                        .child(EVENTS_TABLE)
                        .orderByChild(EVENTS_CHILD_TYPE).equalTo(eventType)
                        .addListenerForSingleValueEvent(new EventValueEventListener(emitter, null));
            }
        });
    }

    public static Single<List<Event>> observeEventsCreatedByUser(String uId, int eventType) {
        return Single.create(emitter -> {
            if (eventType == TYPE_INVALID) {
                Util.getRootReference()
                        .child(EVENTS_TABLE)
                        .orderByChild(EVENTS_CHILD_TYPE)
                        .addListenerForSingleValueEvent(new EventValueEventListener(emitter, uId));
            } else {
                Util.getRootReference()
                        .child(EVENTS_TABLE)
                        .orderByChild(EVENTS_CHILD_TYPE).equalTo(eventType)
                        .addListenerForSingleValueEvent(new EventValueEventListener(emitter, uId));
            }
        });
    }

    public static void removeEventFromLocation(String eventId, String locationId) {
        Util.getRootReference().child(LOCATIONS_TABLE).child(locationId)
                .child(LOCATIONS_CHILD_EVENTS).child(eventId).removeValue();
    }

    public static void removeEventFromCourse(String eventId, String courseId) {
        Util.getRootReference().child(COURSES_TABLE).child(courseId).child(COURSES_CHILD_LESSONS)
                .child(eventId).removeValue();
    }

    public static void removeEventFromUser(String eventId, String uId) {
        Util.getRootReference().child(USERS_TABLE).child(uId).child(USERS_CHILD_EVENTS)
                .child(eventId).removeValue();
    }

    public static Single<List<Event>> observeEventsUserIsInvitedToInDays(String uId, int days,
                                                                          String status) {
        return Single.create(emitter -> {
            DatabaseReference idReference =
                    Util.getRootReference().child(USERS_TABLE).child(uId).child(USERS_CHILD_EVENTS);
            DatabaseReference eventReference = Util.getRootReference().child(EVENTS_TABLE);
            idReference
                    .addListenerForSingleValueEvent
                            (new EventByIdEventListener(emitter, eventReference, status, days));
        });
    }

    private static class EventByIdEventListener implements ValueEventListener {

        SingleEmitter<List<Event>> emitter;
        DatabaseReference eventReference;
        String status;
        int days;
        long cutOffTimeLowBound;
        long cutOffTimeHighBound;

        public EventByIdEventListener(SingleEmitter<List<Event>> emitter,
                                      DatabaseReference eventReference, String status, int days) {
            this.emitter = emitter;
            this.eventReference = eventReference;
            this.status = status;
            this.days = days;
            cutOffTimeLowBound = Calendar.getInstance().getTimeInMillis();
            cutOffTimeHighBound = cutOffTimeLowBound + days * MILLIS_IN_DAY;
        }

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            List<Event> events = new ArrayList<>();
            List<Event> invalidEvents = new ArrayList<>();
            long foundEvents = dataSnapshot.getChildrenCount();
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                String foundStatus = snapshot.getValue().toString();
                eventReference.child(snapshot.getKey())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Event event = dataSnapshot.getValue(Event.class);
                                event.setId(dataSnapshot.getKey());
                                long startTime = Util
                                        .formTimeFromDateAndOffsets(event.getStartDate(),
                                                event.getStartHour(), event.getStartMinute());
                                if ((!TextUtils.isEmpty(status) && !foundStatus.equals(status))
                                || startTime > cutOffTimeHighBound || startTime < cutOffTimeLowBound) {
                                    invalidEvents.add(event);
                                } else {
                                    events.add(event);
                                }
                                if ((events.size() + invalidEvents.size()) == foundEvents) {
                                    emitter.onSuccess(events);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        });
            }
            if (foundEvents == 0) {
                emitter.onSuccess(new ArrayList<>());
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            emitter.onError(databaseError.toException());
        }
    }

    public static Single<String> observeUserEventInviteStatus(String uId, String eventId) {
        return Single.create(emitter -> Util.getRootReference().child(USERS_TABLE)
                .child(uId)
                .child(USERS_CHILD_EVENTS)
                .child(eventId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            emitter.onSuccess(dataSnapshot.getValue().toString());
                        } else {
                            emitter.onSuccess("");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                }));
    }

    public static Single<List<Event>> observePublicEventsInDays(String uid, int days, String status) {
        return Single.create(emitter -> {
            if (TextUtils.isEmpty(status) || status.equals(STATUS_INVITED)) {
                Util.getRootReference().child(EVENTS_TABLE)
                        .orderByChild(EVENTS_CHILD_TYPE).equalTo(TYPE_PUBLIC_EVENT)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                long cutOffTimeLowBound = Calendar.getInstance().getTimeInMillis();
                                long cutOffTimeHighBound = cutOffTimeLowBound + days * MILLIS_IN_DAY;
                                List<Event> events = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    Event event = snapshot.getValue(Event.class);
                                    long startTime = Util
                                            .formTimeFromDateAndOffsets(event.getStartDate(),
                                                    event.getStartHour(), event.getStartMinute());
                                    if (!(startTime > cutOffTimeHighBound
                                            || startTime < cutOffTimeLowBound)) {
                                        event.setId(snapshot.getKey());
                                        events.add(event);
                                    }
                                }
                                emitter.onSuccess(events);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        });
            } else {
                emitter.onSuccess(new ArrayList<>());
            }
        });
    }

    public static Single<List<String>> observeUserContactIds(String uId) {
        return Single.create(emitter ->
                Util.getRootReference().child(USERS_TABLE).child(uId).child(USERS_CHILD_CONTACTS)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                List<String> ids = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    ids.add(snapshot.getKey());
                                }
                                emitter.onSuccess(ids);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                emitter.onError(databaseError.toException());
                            }
                        }
                ));
    }

    public static Single<List<User>> observeUserContacts(String uId) {
        return Single.create(emitter ->
                Util.getRootReference().child(USERS_TABLE).child(uId).child(USERS_CHILD_CONTACTS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        long foundContacts = dataSnapshot.getChildrenCount();
                        List<User> contacts = new ArrayList<>();
                        List<Object> invalid = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Util.getRootReference().child(USERS_TABLE).child(snapshot.getKey())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange
                                                (@NonNull DataSnapshot dataSnapshot) {
                                            User contact = dataSnapshot.getValue(User.class);
                                            if (contact != null) {
                                                contact.setUid(dataSnapshot.getKey());
                                                contacts.add(contact);
                                            } else {
                                                invalid.add(new Object());
                                                removeUserFromContacts(snapshot.getKey(), uId);
                                            }
                                            if (contacts.size() + invalid.size() == foundContacts) {
                                                Collections.sort(contacts);
                                                emitter.onSuccess(contacts);
                                            }
                                        }

                                        @Override
                                        public void onCancelled
                                                (@NonNull DatabaseError databaseError) {
                                            emitter.onError(databaseError.toException());
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                }));
    }

    public static void addContact(String uId, String contactId) {
        Util.getRootReference().child(USERS_TABLE)
                .child(uId)
                .child(USERS_CHILD_CONTACTS)
                .child(contactId)
                .setValue(true);
    }

    public static Single<List<ChatMeta>> observeUnreadChats(String uId) {
        DatabaseReference chats = Util.getRootReference().child(USERS_TABLE).child(uId)
                .child(CHATS_META_TABLE);
        return Single.create(emitter -> chats.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        List<ChatMeta> unreadChats = new ArrayList<>();
                        List<String> readChats = new ArrayList<>();
                        long foundChats = dataSnapshot.getChildrenCount();
                        if (foundChats == 0) {
                            emitter.onSuccess(new ArrayList<>());
                        } else {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                ChatMeta chatMeta = snapshot.getValue(ChatMeta.class);
                                if (chatMeta != null && !chatMeta.isRead()) {
                                    unreadChats.add(chatMeta);
                                } else {
                                    readChats.add(snapshot.getKey());
                                }
                                if ((unreadChats.size() + readChats.size()) == foundChats) {
                                    emitter.onSuccess(unreadChats);
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        emitter.onError(databaseError.toException());
                    }
                }));
    }
    //endregion

    public static SharedPreferences getAppPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SharedPreferences.Editor getAppPreferencesEditor(Context context) {
        return getAppPreferences(context).edit();
    }

    public static String getCurrentLocale() {
        return ConfigurationCompat.getLocales
                (Resources.getSystem().getConfiguration()).get(0).toString().substring(0, 2);
    }

    /**
     * @return regular expression for email addresses, to be used for matching email
     */
    private static String getEmailRegExp() {
        return "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
    }

    /**
     * generates a random string between 11 and 15 characters, to be used for random password
     * generation
     */
    @NonNull
    public static String passwordGenerator() {
        final String characters
                = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!£$%^&*()_+-=#";
        StringBuilder builder = new StringBuilder();
        Random randomGenerator = new Random();
        int length = 10 + randomGenerator.nextInt(5);
        for (int i = 0; i <= length; i++) {
            builder.append(characters.charAt(randomGenerator.nextInt(characters.length())));
        }
        return builder.toString();
    }

    /**
     * Convert a String using MD5 algorithm, to be used for passwords
     * @param beforeConversion plain text input
     * @return MD5 hashed text output
     */
    public static String convertUsingMD5(String beforeConversion) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(beforeConversion.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean adminModuleOptionsActions(Context context, int itemId) {
        switch (itemId) {
            case R.id.menuitem_admin_module_users:
                context.startActivity
                        (new Intent(context, UsersActivity.class));
                return true;
            case R.id.menuitem_admin_module_tests: {
                Intent startIntent = new Intent(context, TestsDisplayActivity.class);
                startIntent.putExtra(FLAG_USER_ROLE, ROLE_ADMINISTRATOR);
                context.startActivity(startIntent);
                return true;
            }
            case R.id.menuitem_admin_module_events: {
                Intent startIntent = new Intent(context, UserCreatedEventsActivity.class);
                startIntent.putExtra(FLAG_USER_ROLE, ROLE_ADMINISTRATOR);
                context.startActivity(startIntent);
                return true;
            }
            case R.id.menuitem_admin_module_classes:
                context.startActivity(new Intent(context, CoursesActivity.class));
                return true;
            case R.id.menuitem_admin_module_locations:
                context.startActivity(new Intent(context, LocationsActivity.class));
                return true;
            default:
                return false;
        }
    }

    public static void replaceFragment(@NonNull FragmentManager manager, Fragment fragment,
                                       int container) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * Checks whether the input String is a valid email address
     * @param input String to be checked
     * @return true if input is a valid email address, false otherwise
     */
    public static boolean isValidEmail(@NonNull String input) {
        return input.matches(getEmailRegExp());
    }

    /**
     * Checks whether the input String matches the pattern of Metropolitan email addresses
     * @param email input email
     * @return true if input matches the pattern of Metropolitan email addresses
     */
    public static boolean isMetropolitanEmail(@NonNull String email) {
        int atIndex = email.indexOf("@");
        return email.substring(atIndex).contains("metropolitan");
    }

    /**
     * Checks whether the input String is a valid email address that matches the pattern of
     * Metropolitan email addresses
     * @param input String to be checked
     * @return true if input is a valid email address that atches the pattern of Metropolitan email
     * addresses
     */
    public static boolean isValidMetropolitanEmail(@NonNull String input) {
        return isValidEmail(input) && isMetropolitanEmail(input);
    }

    public static void startPasswordChange(@NonNull Activity starter,
                                           String accountCreated,
                                           String changeGenericPassword) {
        Intent changePassword = new Intent(starter, ChangePasswordActivity.class);
        if (!TextUtils.isEmpty(accountCreated)) {
            changePassword.putExtra(ChangePasswordActivity.EXTRA_ACCOUNT_CREATED, accountCreated);
        }
        if (!TextUtils.isEmpty(changeGenericPassword)) {
            changePassword.putExtra(ChangePasswordActivity.EXTRA_MESSAGE, changeGenericPassword);
        }
        starter.startActivity(changePassword);
    }

    public static void startLogin(@NonNull Activity starter) {
        starter.startActivity(new Intent(starter, LoginScreenActivity.class));
    }

    public static void startRegister(@NonNull Activity starter) {
        starter.startActivity(new Intent(starter, RegistrationActivity.class));
    }

    public static void showEvent(@NonNull Activity starter, int role, String eventId) {
        Intent showEvent = new Intent(starter, EventDisplayActivity.class);
        showEvent.putExtra(FLAG_USER_ROLE, role);
        showEvent.putExtra(FLAG_EVENT_ID, eventId);
        starter.startActivity(showEvent);
    }

    public static void startEditEvent(@NonNull Activity starter, int role, String eventId) {
        Intent startEventEdit = new Intent(starter, EventCreateActivity.class);
        startEventEdit.putExtra(FLAG_USER_ROLE, role);
        startEventEdit.putExtra(FLAG_EVENT_ID, eventId);
        starter.startActivity(startEventEdit);
    }

    //region UI interactions
    public static void enableView(@NonNull View view) {
        view.setAlpha(1.0f);
        view.setEnabled(true);
    }

    public static void disableView(@NonNull View view) {
        view.setAlpha(0.6f);
        view.setEnabled(false);
    }

    public static void setViewVisibilityAndUpdateAdapter(@NonNull List list, TextView emptyView,
                                                         ListView listView,
                                                         RefreshAdapter adapter) {
        if (list.isEmpty()) {
            hideEmptyListAndShowEmptyView(emptyView, listView);
        } else {
            emptyView.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            adapter.refreshList(list);
        }
    }

    public static void hideEmptyListAndShowEmptyView(@NonNull TextView emptyView,
                                                     @NonNull ListView listView) {
        listView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
    }

    public static void hideEmptyViewAndShowList(@NonNull TextView emptyView,
                                                     @NonNull ListView listView) {
        listView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
    }

    /**
     * Creates a timestamp in the form dd.MM.yyyy. HH:mm, for example 25.07.2019 13:45, from the
     * input day in milliseconds and offset of hours and minutes added onto it.
     */
    public static String formTimestamp(long dateInMillis, int hoursOffset, int minutesOffset) {
        SimpleDateFormat dayFormatter = new SimpleDateFormat("dd.MM.yyyy. HH:mm");
        return dayFormatter.format(new Date
                (formTimeFromDateAndOffsets(dateInMillis, hoursOffset, minutesOffset)));
    }

    /**
     * Creates a timestamp in the form HH:mm, for example 13:45, from the input day in milliseconds
     * and offset of hours and minutes added onto it.
     */
    public static String formShortTimestamp(long dateInMillis, int hoursOffset, int minutesOffset) {
        SimpleDateFormat dayFormatter = new SimpleDateFormat("HH:mm");
        return dayFormatter.format(new Date
                (formTimeFromDateAndOffsets(dateInMillis, hoursOffset, minutesOffset)));
    }

    public static String formatTimeString(int timeUnit) {
        return (timeUnit > 9 ? "" : "0") + timeUnit;
    }

    public static long formTimeFromDateAndOffsets
            (long dateInMillis, int hoursOffset, int minutesOffset) {
        return dateInMillis + (hoursOffset * MILLIS_IN_HOUR) + (minutesOffset * MILLIS_IN_MINUTE);
    }
    //endregion

}
