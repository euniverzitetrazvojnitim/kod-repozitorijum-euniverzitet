package com.miroslav727.euniverzitet.util;

public class Constants {

    //region firebase constants
    public static final String FIREBASE_API_KEY = "AIzaSyDa2xfdmQjGDS2kXWqEhSy2AiDh6k8vFV4";
    public static final String FIREBASE_DATABASE_URL = "https://miroslav727-euniver.firebaseio.com";
    public static final String FIREBASE_PROJECT_ID = "miroslav727-euniver";
    public static final String FIREBASE_APP_ID = "1:947128112698:android:7a03fb427b949c44";
    public static final String FIREBASE_SECONDARY_NAME = "SECONDARY";
    //endregion

    //region Users table constants
    public static final String USERS_TABLE = "users";
    public static final String USERS_CHILD_EMAIL = "email";
    public static final String USERS_CHILD_ROLE = "role";
    public static final String USERS_CHILD_PASSWORD = "password";
    public static final String USERS_CHILD_GENERIC = "generic";
    public static final String USERS_CHILD_RESET = "reset";
    public static final String USERS_CHILD_STUDIES = "studies";
    public static final String USERS_CHILD_TEACHES = "teaches";
    public static final String USERS_CHILD_PASSED_TESTS = "passedTests";
    public static final String USERS_CHILD_EVENTS = "events";
    public static final String USERS_CHILD_CONTACTS = "contacts";
    //endregion

    //region Courses table constants
    public static final String COURSES_TABLE = "courses";
    public static final String COURSES_CHILD_CODE = "code";
    public static final String COURSES_CHILD_NAME = "name";
    public static final String COURSES_CHILD_STUDENTS = "students";
    public static final String COURSES_CHILD_TEACHERS = "teachers";
    public static final String COURSES_CHILD_TESTS = "tests";
    public static final String COURSES_CHILD_LESSONS = "lessons";
    //endregion

    //region Locations table constants
    public static final String LOCATIONS_TABLE = "locations";
    public static final String LOCATIONS_CHILD_NAME = "name";
    public static final String LOCATIONS_CHILD_LOCATION = "location";
    public static final String LOCATIONS_CHILD_TYPE = "type";
    public static final String LOCATIONS_CHILD_EVENTS = "events";
    //endregion

    //region Tests table constants
    public static final String TESTS_TABLE = "tests";
    public static final String TESTS_CHILD_COURSE = "course";
    public static final String TESTS_CHILD_TYPE = "type";
    public static final String TESTS_CHILD_NUMBER = "number";
    public static final String TESTS_CHILD_DURATION = "duration";
    public static final String TESTS_CHILD_QUESTION_NUMBER = "questionNumber";
    public static final String TESTS_CHILD_VALUE = "value";
    public static final String TESTS_CHILD_ACTIVE = "active";
    public static final String TESTS_CHILD_QUESTIONS ="questions";
    //endregion

    //region Questions table constants
    public static final String QUESTIONS_TABLE = "questions";
    public static final String QUESTIONS_CHILD_COURSE = "course";
    public static final String QUESTIONS_CHILD_TYPE = "type";
    public static final String QUESTIONS_CHILD_DESCRIPTION = "description";
    public static final String QUESTIONS_CHILD_VALUE = "value";
    public static final String QUESTION_CHILD_CORRECT_ANSWERS = "correct_answers";
    public static final String QUESTION_CHILD_INCORRECT_ANSWERS = "incorrect_answers";
    public static final String QUESTION_CHILD_TESTS = "tests";
    //endregion

    //region Answers table constants
    public static final String ANSWERS_TABLE = "answers";
    public static final String ANSWERS_CHILD_ANSWER = "answer";
    public static final String ANSWERS_CHILD_QUESTION = "question";
    //endregion

    //region Events table constants
    public static final String EVENTS_TABLE = "events";
    public static final String EVENTS_CHILD_TITLE = "title";
    public static final String EVENTS_CHILD_DESCRIPTION = "description";
    public static final String EVENTS_CHILD_START_DATE = "startDate";
    public static final String EVENTS_CHILD_START_HOUR = "startHour";
    public static final String EVENTS_CHILD_START_MINUTE = "startMinute";
    public static final String EVENTS_CHILD_END_DATE = "endDate";
    public static final String EVENTS_CHILD_END_HOUR = "endHour";
    public static final String EVENTS_CHILD_END_MINUTE = "endMinute";
    public static final String EVENTS_CHILD_TYPE = "type";
    public static final String EVENTS_CHILD_CREATOR_ID = "creatorId";
    public static final String EVENTS_CHILD_LOCATION_ID = "locationId";
    public static final String EVENTS_CHILD_COURSE_ID = "courseId";
    public static final String EVENTS_CHILD_INVITEES = "invitees";
    //endregion

    //region Messages table constants
    public static final String MESSAGES_TABLE = "messages";
    public static final String MESSAGES_CHILD_SENDER_ID = "senderId";
    public static final String MESSAGES_CHILD_SENDER_EMAIL = "senderEmail";
    public static final String MESSAGES_CHILD_RECEIVER_ID = "receiverId";
    public static final String MESSAGES_CHILD_RECEIVER_EMAIL = "receiverEmail";
    public static final String MESSAGES_CHILD_SENT_AT = "sentAt";
    public static final String MESSAGES_CHILD_TEXT = "text";
    public static final String MESSAGES_CHILD_READ = "read";
    //endregion

    //region Chat table constants
    public static final String CHATS_META_TABLE = "chats";
    public static final String CHATS_META_CHILD_READ = "read";
    public static final String CHATS_META_CHILD_LAST_MESSAGE = "lastMessage";
    public static final String CHATS_META_CHILD_SENDER_EMAIL = "senderEmail";
    public static final String CHATS_META_CHILD_SENDER_ID = "senderId";
    //endregion

    //region roles mapped to int
    public static final int ROLE_STUDENT = 0;
    public static final int ROLE_ADMINISTRATOR = 1;
    public static final int ROLE_TEACHING_STAFF = 2;
    public static final int ROLE_NON_TEACHING_STAFF = 3;
    public static final int ROLE_TEMP_USER = 4;
    public static final int ROLE_DELETED_USER = 5;
    //endregion

    public static final int TYPE_INVALID = -1;

    //region location types mapped to int
    public static final int TYPE_CLASSROOM = 0;
    public static final int TYPE_MEETING_ROOM = 1;
    public static final int TYPE_CONFERENCE_ROOM = 2;
    public static final int TYPE_OTHER = 3;
    //endregion

    //region test types mapped to int
    public static final int TYPE_WEEKLY = 0;
    public static final int TYPE_MIDTERM = 1;
    public static final int TYPE_EXAM = 2;
    //endregion

    //region question types mapped to int
    public static final int TYPE_FREE_ANSWER = 0;
    public static final int TYPE_SINGLE_CORRECT = 1;
    public static final int TYPE_MULTIPLE_CORRECT = 2;
    //endregion

    //region event types mapped to int
    public static final int TYPE_PUBLIC_EVENT = 0;
    public static final int TYPE_PRIVATE_EVENT = 1;
    public static final int TYPE_LESSON = 2;
    //endregion

    //region event user statuses mapped to String
    public static final String STATUS_INVITED = "invited";
    public static final String STATUS_ACCEPTED = "accepted";
    public static final String STATUS_REJECTED = "rejected";
    //endregion

    //region message types
    public static final int TYPE_SENT = 0;
    public static final int TYPE_RECEIVED = 1;
    //endregion

    //region TimeConstants
    public static final int MILLIS_IN_MINUTE = 60000;
    public static final int MILLIS_IN_HOUR = 3600000;
    public static final int MILLIS_IN_DAY = 86400000;
    //endregion
}
