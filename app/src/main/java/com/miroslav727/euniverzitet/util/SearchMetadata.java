package com.miroslav727.euniverzitet.util;

public class SearchMetadata {

    private String lastSearch;
    private boolean exactSearch;

    public SearchMetadata() {
    }

    public SearchMetadata(String lastSearch, boolean exactSearch) {
        this.setLastSearch(lastSearch);
        this.setExactSearch(exactSearch);
    }


    public String getLastSearch() {
        return lastSearch;
    }

    public void setLastSearch(String lastSearch) {
        this.lastSearch = lastSearch;
    }

    public boolean isExactSearch() {
        return exactSearch;
    }

    public void setExactSearch(boolean exactSearch) {
        this.exactSearch = exactSearch;
    }
}
