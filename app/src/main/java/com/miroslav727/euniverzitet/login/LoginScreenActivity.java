package com.miroslav727.euniverzitet.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.miroslav727.euniverzitet.MainActivity;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.util.Constants;
import com.miroslav727.euniverzitet.util.Util;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.util.Constants.FIREBASE_SECONDARY_NAME;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_RESET;
import static com.miroslav727.euniverzitet.util.Constants.USERS_TABLE;

public class LoginScreenActivity extends AppCompatActivity {

    public static final String KEY_UID = "KEY_UID";
    public static final String KEY_EMAIL = "KEY_EMAIL";
    public static final String KEY_PASS = "KEY_PASS";

    private FirebaseAuth auth;

    private FirebaseApp secondary;

    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        compositeDisposable = new CompositeDisposable();

        auth = FirebaseAuth.getInstance();

        secondary = Util.getFirebaseApp(this, FIREBASE_SECONDARY_NAME);

        Button loginButton = findViewById(R.id.button_login);
        Button resetButton = findViewById(R.id.button_resetPassword);
        loginButton.setOnClickListener(view -> {
            TextInputLayout emailViewWrapper = findViewById(R.id.til_loginEmailInput);
            TextInputLayout passwordViewWrapper = findViewById(R.id.til_loginPasswordInput);
            String email = emailViewWrapper.getEditText().getText().toString();
            String pass = passwordViewWrapper.getEditText().getText().toString();
            if (Util.isValidEmail(email) && !TextUtils.isEmpty(pass)) {
                setButtonsEnabled(loginButton, resetButton, false);
                FirebaseAuth.getInstance(secondary).signInWithEmailAndPassword
                        (email, Util.convertUsingMD5(pass)).addOnCompleteListener(task -> {
                    FirebaseUser foundUser = null;
                    FirebaseAuth.getInstance(secondary).signOut();
                    if (task.isSuccessful()) foundUser = task.getResult().getUser();
                    if (foundUser != null) {
                        String id = foundUser.getUid();
                        compositeDisposable.add(
                                Util.observeUserFromDatabase(id)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(user -> {
                                    if (markedForDeletion(user.getRole())) {
                                        showDeleteDialog
                                                (user.getUid(), email, pass, user.isGeneric());
                                        performCleanup(true);
                                        return;
                                    }
                                    emailViewWrapper.setError(null);
                                    emailViewWrapper.setErrorEnabled(false);
                                    passwordViewWrapper.setError(null);
                                    passwordViewWrapper.setErrorEnabled(false);
                                    performLogin(email, Util.convertUsingMD5(pass),
                                            loginButton, resetButton);
                                })
                        );
                    } else {
                        FirebaseAuth.getInstance(secondary).signInWithEmailAndPassword
                                (email, pass).addOnCompleteListener(genericTask -> {
                            FirebaseAuth.getInstance(secondary).signOut();
                            if (genericTask.isSuccessful()) {
                                auth.signInWithEmailAndPassword(email, pass)
                                        .addOnCompleteListener(signInTask ->
                                                Util.startPasswordChange(this, null,
                                                this.getString(R.string.change_generic_password)));
                            } else {
                                Util.closeKeyboard(this);
                                emailViewWrapper.setError(" ");
                                passwordViewWrapper.setError
                                        (getString(R.string.invalid_login_attempt));
                                setButtonsEnabled(loginButton, resetButton, true);
                            }
                        });
                    }
                });
            } else {
                Util.closeKeyboard(this);
                emailViewWrapper.setError(" ");
                passwordViewWrapper.setError
                        (getString(R.string.invalid_login_attempt));
            }
        });

        resetButton.setOnClickListener(view -> {
            TextInputLayout emailViewWrapper = findViewById(R.id.til_loginEmailInput);
            TextInputLayout passwordViewWrapper = findViewById(R.id.til_loginPasswordInput);
            String email = emailViewWrapper.getEditText().getText().toString();
//            if (validEmailInput(email)) {//todo 30.07 return this check once testing phase ends
            if (Util.isValidEmail(email)) {
                setButtonsEnabled(loginButton, resetButton, false);
                emailViewWrapper.setError(null);
                emailViewWrapper.setErrorEnabled(false);
                attemptUserPasswordReset
                        (email, emailViewWrapper, passwordViewWrapper, loginButton, resetButton);
            } else {
                setResetError(emailViewWrapper, passwordViewWrapper, loginButton, resetButton);
            }
        });

        TextView linkToRegister = findViewById(R.id.textview_loginNoAccount);
        linkToRegister.setOnClickListener(view -> Util.startRegister(this));
    }

    @Override
    protected void onStop() {
        performCleanup(true);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        performCleanup(false);
        super.onDestroy();
    }

    /**
     * Checking if the input yields an existing email and password combination in the database
     * @param email input email
     * @param pass input password
     * @return true if input email is a valid Metropolitan email and if the combination of email and
     * password exists in database
     */
    private boolean validLoginInput(String email, String pass) {
        return Util.isValidMetropolitanEmail(email);// todo 29.07 - use this method once testing ends
    }

    private void showDeleteDialog(String uid, String email, String pass, boolean generic) {
        DialogFragment deletedDialog = new DeleteUserDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_UID, uid);
        bundle.putString(KEY_EMAIL, email);
        bundle.putString(KEY_PASS, generic ? pass : Util.convertUsingMD5(pass));
        deletedDialog.setArguments(bundle);
        deletedDialog.show(getSupportFragmentManager(), DeleteUserDialog.class.getName());
    }

    private void performLogin(String email, String pass, Button loginButton, Button resetButton) {
        auth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(task -> {
           if (task.isSuccessful()) {
               performCleanup(false);
               startActivity(new Intent(this, MainActivity.class));
           } else {
               Toast.makeText(this, getString(R.string.login_fail), Toast.LENGTH_SHORT)
                       .show();
               setButtonsEnabled(loginButton, resetButton, true);
           }
        });
    }

    private void attemptUserPasswordReset(String email, TextInputLayout emailWrapper,
                                          TextInputLayout passwordWrapper, Button loginButton,
                                          Button resetButton) {
        compositeDisposable.add(
                Util.observeUserByEmail(email)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableMaybeObserver<User>() {
                            @Override
                            public void onSuccess(User user) {
                                auth.signInWithEmailAndPassword(email, user.getPassword())
                                        .addOnCompleteListener(task -> {
                                            if (task.isSuccessful()) {
                                                resetUserPassword(user);
                                            }
                                        });
                            }

                            @Override
                            public void onError(Throwable e) {
                                setResetError
                                        (emailWrapper, passwordWrapper, loginButton, resetButton);
                                Log.e(LoginScreenActivity.class.getSimpleName(), e.getMessage());
                            }

                            @Override
                            public void onComplete() {
                                setResetError
                                        (emailWrapper, passwordWrapper, loginButton, resetButton);
                            }
                        })
        );
    }

    private void resetUserPassword(User user) {
        String newPassword = Util.passwordGenerator();
        auth.getCurrentUser().updatePassword(newPassword);
        user.setPassword(newPassword);
        user.setGeneric(true);
        user.setLocale(Util.getCurrentLocale());
        DatabaseReference userReference = Util.getRootReference().child(USERS_TABLE)
                .child(user.getUid());
        userReference.setValue(user);
        userReference.child(USERS_CHILD_RESET).setValue(true);
        Util.startPasswordChange(this,null, this.getString(R.string.change_generic_password));
    }

    /**
     * Checking if the input yields an existing email in the database
     * @param email input email
     * @return true if input email is a valid Metropolitan email and if email exists in database
     */
    private boolean validEmailInput(String email) {
        return Util.isValidMetropolitanEmail(email);// todo 21.07 - && userExists()
    }

    private boolean markedForDeletion(int role) {
        return Constants.ROLE_DELETED_USER == role;
    }

    private void setButtonsEnabled(Button loginButton, Button resetButton, boolean enabled) {
        loginButton.setEnabled(enabled);
        resetButton.setEnabled(enabled);
    }

    private void setResetError(TextInputLayout emailWrapper, TextInputLayout passwordWrapper,
                               Button loginButton, Button resetButton) {
        passwordWrapper.setError(null);
        passwordWrapper.setErrorEnabled(false);
        emailWrapper.setError(getString(R.string.unable_to_reset_password));
        setButtonsEnabled(loginButton, resetButton, true);
    }

    /**
     * Cleans up background resources, disposes all disposables, destroys secondary firebase app
     * instance
     */
    private void performCleanup(boolean reuse) {
        if (reuse) {
            compositeDisposable.clear();
        } else {
            compositeDisposable.dispose();
            Util.destroyFirebaseApp(secondary);
        }
    }

    /**
     * Changes the role of deleted user to temporary user and opens main activity when the role is
     * changed
     * @param uid id of the user to save from deletion
     */
    public void saveUser(String uid, String email, String pass) {
        Util.editUserRole(uid, Constants.ROLE_TEMP_USER).addOnCompleteListener(task -> {
            Button login = findViewById(R.id.button_login);
            Button reset = findViewById(R.id.button_resetPassword);
            performLogin(email, pass, login, reset);
        });
    }

    /**
     * Deletes the user from application by deleting it both from Firebase auth and Firebase
     * database. Once these tasks are complete, starts registration activity
     * @param email email of the user to delete
     * @param pass pass of the user to delete - necessary in order to log in as user and call delete
     *             method, which can only be called by logged in user.
     */
    public void deleteUser(String email, String pass) {
        auth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(task -> {
            String uid = task.getResult().getUser().getUid();
            task.getResult().getUser().delete().addOnCompleteListener(deleteTask -> {
                auth.signOut();
                Util.removeUserFromDatabaseTable(uid);
                startActivity(new Intent(this, RegistrationActivity.class));
            });
        });
    }
}
