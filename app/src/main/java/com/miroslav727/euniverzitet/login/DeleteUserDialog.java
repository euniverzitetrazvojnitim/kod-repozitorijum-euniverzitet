package com.miroslav727.euniverzitet.login;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.miroslav727.euniverzitet.R;

import static com.miroslav727.euniverzitet.login.LoginScreenActivity.KEY_EMAIL;
import static com.miroslav727.euniverzitet.login.LoginScreenActivity.KEY_PASS;
import static com.miroslav727.euniverzitet.login.LoginScreenActivity.KEY_UID;

public class DeleteUserDialog extends DialogFragment {

    private boolean saveUser;

    private String email;

    private String pass;
    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LoginScreenActivity activity = (LoginScreenActivity) getActivity();
        LayoutInflater inflater = activity.getLayoutInflater();
        Bundle arguments = getArguments();
        String uid = arguments.getString(KEY_UID);
        email = arguments.getString(KEY_EMAIL);
        pass = arguments.getString(KEY_PASS);

        View dialogLayout = inflater.inflate(R.layout.delete_user_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity,
                R.style.generalDialogTheme);
        builder.setTitle(R.string.deleted_user_dialog_title)
                .setView(dialogLayout)
                .setPositiveButton(R.string.ok_dialog, (DialogInterface dialog, int id) -> {
                    activity.saveUser(uid, email, pass);
                    saveUser = true;
                    Toast.makeText(getContext(), "Saved user", Toast.LENGTH_SHORT).show();
                })
                .setNegativeButton(R.string.cancel_dialog, (DialogInterface dialog, int id) -> {
                    saveUser = false;
                    dismiss();
                });
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView title = this.getDialog()
                .findViewById(androidx.appcompat.R.id.alertTitle);
        if (title != null) {
            title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        if (!saveUser && getActivity() != null) {
            ((LoginScreenActivity)getActivity()).deleteUser(email,pass);
        }
        super.onDismiss(dialog);
    }
}
