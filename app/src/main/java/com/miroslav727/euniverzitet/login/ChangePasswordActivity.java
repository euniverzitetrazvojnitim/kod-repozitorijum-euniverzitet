package com.miroslav727.euniverzitet.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.miroslav727.euniverzitet.MainActivity;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.util.Util;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ChangePasswordActivity extends AppCompatActivity {

    public static final String EXTRA_ACCOUNT_CREATED = "account_created";
    public static final String EXTRA_MESSAGE = "message";

    private FirebaseAuth auth;

    private User currentUser;
    private boolean genericScreen;

    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        auth = FirebaseAuth.getInstance();

        compositeDisposable = new CompositeDisposable();

        compositeDisposable
                .add(Util.observeUserFromDatabase(auth.getUid())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(user -> {
                            currentUser = user;
                        }));


        Intent startIntent = getIntent();
        String accountCreated = startIntent.getStringExtra(EXTRA_ACCOUNT_CREATED);
        String message = startIntent.getStringExtra(EXTRA_MESSAGE);

        TextView accountCreatedView = findViewById(R.id.textview_cp_accountCreated);
        setViewTextAndVisibility(accountCreatedView, accountCreated);

        TextView messageView = findViewById(R.id.textview_cp_message);
        setViewTextAndVisibility(messageView, message);

        TextView titleView = findViewById(R.id.textview_cp_title);
        if (!TextUtils.isEmpty(message)) {
            titleView.setVisibility(View.GONE);
            genericScreen = true;
        }

        TextInputLayout oldPasswordWrapper = findViewById(R.id.til_cp_oldPasswordInput);
        TextInputLayout newPasswordWrapper = findViewById(R.id.til_cp_newPasswordInput);
        TextInputLayout confirmPasswordWrapper = findViewById(R.id.til_cp_confirmPasswordInput);

        Button changePasswordButton = findViewById(R.id.button_changePassword);

        changePasswordButton.setOnClickListener(view -> {
            String oldPassword = oldPasswordWrapper.getEditText().getText().toString();
            String newPassword = newPasswordWrapper.getEditText().getText().toString();
            String confirmPassword = confirmPasswordWrapper.getEditText().getText().toString();

            boolean oldPasswordOk = validInputOfOldPassword(oldPassword);

            boolean newPasswordOk = validateLength(newPassword)
                    && validateNoWhitespace(newPassword)
                    && validateLowercase(newPassword)
                    && validateUppercase(newPassword)
                    && validateDigits(newPassword);

            boolean confirmPasswordOk = newPassword.equals(confirmPassword);

            if (oldPasswordOk) {
                oldPasswordWrapper.setErrorEnabled(false);
            } else {
                oldPasswordWrapper.setError(getString(R.string.passwords_not_matching));
            }

            if (newPasswordOk) {
                newPasswordWrapper.setErrorEnabled(false);
            } else if (!validateLength(newPassword)) {
                newPasswordWrapper.setError(getString(R.string.password_min_length));
            } else if (!validateNoWhitespace(newPassword)) {
                newPasswordWrapper.setError(getString(R.string.password_no_whitespaces));
            } else if (!validateLowercase(newPassword)) {
                newPasswordWrapper.setError(getString(R.string.password_lowercase));
            } else if (!validateUppercase(newPassword)) {
                newPasswordWrapper.setError(getString(R.string.password_uppercase));
            } else if (!validateDigits(newPassword)) {
                newPasswordWrapper.setError(getString(R.string.password_digit));
            }

            if (oldPasswordOk && newPasswordOk && confirmPasswordOk) {
                changeUserPassword(newPassword);
            }
        });

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String pass = newPasswordWrapper.getEditText().getText().toString();
                String confirmPass = confirmPasswordWrapper.getEditText().getText().toString();
                if (TextUtils.isEmpty(pass) && TextUtils.isEmpty(confirmPass)) {
                    confirmPasswordWrapper.setErrorEnabled(false);
                    return;
                }
                if (pass.equals(confirmPass)) {
                    confirmPasswordWrapper.setErrorTextAppearance(R.style.authModulePassMatch);
                    confirmPasswordWrapper.setError(getString(R.string.passwords_match));
                } else {
                    confirmPasswordWrapper.setErrorTextAppearance(R.style.authModuleErrorTextStyle);
                    confirmPasswordWrapper.setError(getString(R.string.passwords_not_matching));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        };
        newPasswordWrapper.getEditText().addTextChangedListener(watcher);
        confirmPasswordWrapper.getEditText().addTextChangedListener(watcher);

    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (genericScreen) {
            startActivity(new Intent(this, LoginScreenActivity.class));
        } else {
            super.onBackPressed();
        }
    }

    private void setViewTextAndVisibility(@NonNull TextView textView, String text) {
        if (!TextUtils.isEmpty(text)) {
            textView.setText(text);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    /**
     * Gets password from parameter, converts it using md5 algorithm, sets it in Firebase Auth and
     * Firebase database, in case of generic passwords removes generic flag from user and starts
     * main activity
     * @param password the new password user input
     */
    private void changeUserPassword(String password) {
        String md5Password = Util.convertUsingMD5(password);
        FirebaseUser currentAuthUser = auth.getCurrentUser();
        if (currentAuthUser != null) currentAuthUser.updatePassword(md5Password);
        currentUser.setPassword(md5Password);
        if (genericScreen) {
            currentUser.setGeneric(false);
        }
        Util.editUser(currentUser);
        startActivity(new Intent(this, MainActivity.class));
    }

    /**
     * Checks whether the input password matches the one saved in database
     * @param oldPassword the input password
     * @return true if the input matches database entry
     */
    private boolean validInputOfOldPassword(@NonNull String oldPassword) {
        String password = genericScreen ? oldPassword : Util.convertUsingMD5(oldPassword);
        return password.equals(currentUser.getPassword());
    }

    /**
     * Checks whether the input new password satisfies the length condition
     * @param password the input password
     * @return true if the password is longer than 8 characters
     */
    private boolean validateLength(@NonNull String password) {
        return password.length() >= 8;
    }

    /**
     * Checks whether the input new password satisfies the uppercase condition
     * @param password the input password
     * @return true if the password contains at least one uppercase Unicode character
     */
    private boolean validateUppercase(@NonNull String password) {
        return password.matches("(?=.*\\p{Lu}).*$");
    }

    /**
     * Checks whether the input new password satisfies the lowercase condition
     * @param password the input password
     * @return true if the password contains at least one lowercase Unicode character
     */
    private boolean validateLowercase(@NonNull String password) {
        return password.matches("(?=.*\\p{Ll}).*$");
    }

    /**
     * Checks whether the input new password satisfies the digits condition
     * @param password the input password
     * @return true if the password contains at least one digit
     */
    private boolean validateDigits(@NonNull String password) {
        return password.matches("(?=.*\\d).*$");
    }

    /**
     * Checks whether the input new password satisfies the no whitespaces condition
     * @param password the input password
     * @return true if the password does not contain any whitespaces
     */
    private boolean validateNoWhitespace(@NonNull String password) {
        return password.matches("(?!.*\\s).*$");
    }
}
