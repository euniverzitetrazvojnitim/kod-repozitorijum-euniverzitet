package com.miroslav727.euniverzitet.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.util.Util;

import static com.miroslav727.euniverzitet.MainActivity.FLAG_USER_REGISTERED;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_STUDENT;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEMP_USER;


public class RegistrationActivity extends AppCompatActivity {

     private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        auth = FirebaseAuth.getInstance();

        TextInputLayout emailViewWrapper = findViewById(R.id.til_registrationEmailInput);

        Button registerButton = findViewById(R.id.button_registerUser);
        registerButton.setOnClickListener(view -> {
            String email = emailViewWrapper.getEditText().getText().toString();
            if (validInput(email)) {
                emailViewWrapper.setError(null);
                emailViewWrapper.setErrorEnabled(false);
                registerUser(email, emailViewWrapper, registerButton);
            } else {
                handleError(emailViewWrapper, registerButton);
            }
        });

        TextView linkToLogin = findViewById(R.id.textview_registrationExistingUser);
        linkToLogin.setOnClickListener(view -> Util.startLogin(this));
    }

    private boolean validInput(String input) {
        return Util.isValidEmail(input); //TODO 21.07 metropolitan check removed for testing purposes, remove later
//        return Util.isValidMetropolitanEmail(input);
    }

    private void registerUser(@NonNull String email, @NonNull TextInputLayout wrapper,
                              @NonNull Button registerButton) {
        final String genericPassword = Util.passwordGenerator();
        registerButton.setEnabled(false);
        auth.createUserWithEmailAndPassword(email, genericPassword)
                .addOnCompleteListener((task -> {
                    if (task.isSuccessful()) {
                        createUserInDatabase
                                (task.getResult().getUser().getUid(), email, genericPassword);
                        setUserRegisteredFlagInPrefs();
                        Util.startPasswordChange(this, getString(R.string.account_created),
                                getString(R.string.change_generic_password));
                    } else {
                        boolean emailExists =
                                task.getException() instanceof FirebaseAuthUserCollisionException;
                        if (emailExists) {
                            handleError(wrapper, registerButton);
                        }

                    }
                }));
    }

    private void createUserInDatabase(String uId, String email, String password) {
        int role = isStudent(email) ? ROLE_STUDENT : ROLE_TEMP_USER;
        Util.addUserToDatabase(uId, email, role, password);
    }

    private boolean isStudent(@NonNull String email) {
        String localAddress = email.substring(0, email.indexOf('@'));
        return localAddress.matches("(?=.*\\d).*$");
    }

    private void handleError(@NonNull  TextInputLayout emailViewWrapper,
                             @NonNull Button registerButton) {
        Util.closeKeyboard(this);
        emailViewWrapper.setError(getString(R.string.register_error));
        registerButton.setEnabled(true);
    }

    private void setUserRegisteredFlagInPrefs() {
        SharedPreferences.Editor editor = Util.getAppPreferencesEditor(this);
        editor.putBoolean(FLAG_USER_REGISTERED, true);
        editor.apply();
    }
}
