package com.miroslav727.euniverzitet.adminModule;

import android.content.Context;
import androidx.annotation.NonNull;
import android.widget.ArrayAdapter;

import java.util.List;

public abstract class RefreshAdapter<T> extends ArrayAdapter {

    public RefreshAdapter(@NonNull Context context, @NonNull List objects) {
        super(context, -1, objects);
    }

    public abstract void refreshList(List<T> objects);


}
