package com.miroslav727.euniverzitet.adminModule;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.util.Util;

import static com.miroslav727.euniverzitet.adminModule.CoursesActivity.KEY_COURSE_CODE;
import static com.miroslav727.euniverzitet.adminModule.CoursesActivity.KEY_COURSE_ID;
import static com.miroslav727.euniverzitet.adminModule.CoursesActivity.KEY_COURSE_NAME;

public class CoursesDialogFragment extends DialogFragment {

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        CoursesActivity activity = (CoursesActivity) getActivity();
        Bundle arguments = getArguments();
        String courseCode = arguments != null ? arguments.getString(KEY_COURSE_CODE) : null;
        String courseName = arguments != null ? arguments.getString(KEY_COURSE_NAME) : null;
        String courseId = arguments != null ? arguments.getString(KEY_COURSE_ID) : null;
        boolean addCourse = TextUtils.isEmpty(courseCode);

        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogLayout = inflater.inflate(R.layout.add_or_edit_course_dialog, null);
        TextInputEditText codeField = dialogLayout
                .findViewById(R.id.tiledittext_coursedialog_course_code);
        TextInputEditText nameField = dialogLayout
                .findViewById(R.id.tiledittext_coursedialog_course_name);

        if (!addCourse) {
            TextInputLayout codeLayout = dialogLayout
                    .findViewById(R.id.til_coursedialog_course_code);
            codeLayout.setHint(getString(R.string.course_code_label));
            TextInputLayout nameLayout = dialogLayout
                    .findViewById(R.id.til_coursedialog_course_name);
            nameLayout.setHint(getString(R.string.course_name_label));
            codeField.setText(courseCode);
            nameField.setText(courseName);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity,
                R.style.generalDialogTheme);

        builder.setTitle(addCourse ?
                activity.getString(R.string.new_course) : activity.getString(R.string.edit_course))
                .setView(dialogLayout)
                .setPositiveButton(R.string.ok_dialog, (DialogInterface dialog, int id) -> {
                    String code = codeField.getText().toString().trim();
                    String name = nameField.getText().toString().trim();

                    if (addCourse) {
                        activity.showPeopleFragment(code, name, activity.createCourse(code, name));
                        dismiss();
                    } else {
                        activity.editCourseCodeAndName(courseId, code, name);
                    }
                })
                .setNegativeButton(R.string.cancel_dialog,  (DialogInterface dialog, int id) -> {
                    Log.d(CoursesDialogFragment.this.getTag(), "Canceled");
                });

        AlertDialog courseDialog = builder.create();
        courseDialog.show();

        final Button confirmButton = courseDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        if (addCourse) Util.disableView(confirmButton);

        TextWatcher confirmController = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Has to be overridden, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String code = codeField.getText().toString().trim();
                String name = nameField.getText().toString().trim();
                if (!TextUtils.isEmpty(code) && !TextUtils.isEmpty(name)) {
                    Util.enableView(confirmButton);
                } else {
                    Util.disableView(confirmButton);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Has to be overridden, not needed
            }
        };
        codeField.addTextChangedListener(confirmController);
        nameField.addTextChangedListener(confirmController);

        return courseDialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView title = this.getDialog()
                .findViewById(androidx.appcompat.R.id.alertTitle);
        if (title != null) {
            title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        Fragment parentFragment = getParentFragment();
        if (parentFragment instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) parentFragment).onDismiss(dialog);
        }
    }
}
