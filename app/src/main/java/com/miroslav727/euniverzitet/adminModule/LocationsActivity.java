package com.miroslav727.euniverzitet.adminModule;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.Location;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.miroslav727.euniverzitet.util.Constants.LOCATIONS_CHILD_NAME;
import static com.miroslav727.euniverzitet.util.Constants.LOCATIONS_TABLE;

public class LocationsActivity extends AppCompatActivity {

    public static final String KEY_LOCATION_ID = "KEY_LOCATION_ID";
    public static final String KEY_LOCATION_NAME = "KEY_LOCATION_NAME";
    public static final String KEY_LOCATION_TYPE = "KEY_LOCATION_TYPE";
    public static final String KEY_LOCATION_ACTUAL_LOCATION = "KEY_LOCATION_ACTUAL_LOCATION";

    private ActionMode actionMode;

    private int selectedCounter;

    private TextInputEditText searchView;

    private Spinner typeSpinner;

    private int selectedPosition;

    private TextView emptyListView;

    private ListView listView;

    private LocationArrayAdapter adapter;

    private List<Boolean> checkedForDeletion;

    private DatabaseReference databaseReference;

    private boolean exactSearch;

    //region action mode
    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.general_action_menu, menu);
            setActionModeTitle(mode);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menuitem_delete:

                    ArrayList<Location> remainingLocations = new ArrayList<>();

                    for (int i = 0; i < checkedForDeletion.size(); i++) {
                        Location location = adapter.locations.get(i);
                        if (checkedForDeletion.get(i)) {
                            removeLocation(location.getId());
                        } else {
                            remainingLocations.add(location);
                        }
                    }
                    checkedForDeletion.clear();
                    adapter.refreshList(remainingLocations);
                    for (int i = 0; i < remainingLocations.size(); i++) {
                        checkedForDeletion.set(i, false);
                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.setInActionMode(false);
            selectedCounter = 0;
            for (int i = 0; i < checkedForDeletion.size(); i++) {
                checkedForDeletion.set(i, false);
            }
            actionMode = null;
        }
    };

    private void setActionModeTitle (@NonNull ActionMode mode) {
        mode.setTitle(getResources()
                .getQuantityString(R.plurals.selected, selectedCounter, selectedCounter));
    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);

        searchView = findViewById(R.id.tiledittext_adminModuleLocationsSearch);
        emptyListView = findViewById(R.id.textview_adminLocationSearchEmptyList);

        checkedForDeletion = new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference();

        final String[] locationTypes = getResources().getStringArray(R.array.allLocationTypes);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item,
                locationTypes);
        typeSpinner = findViewById(R.id.spinner_adminModuleLocationType);
        typeSpinner.setAdapter(spinnerAdapter);

        listView = findViewById(R.id.listview_adminModuleLocationSearchResults);
        adapter = new LocationArrayAdapter(this, new ArrayList<>());
        listView.setAdapter(adapter);

        addListeners();
    }

    //region listeners
    private void addListeners() {
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchText = searchView.getText().toString().trim();
                safeSearchLocationDatabaseAndListResults(searchText);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        searchView.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                handled = true;
                String searchText = searchView.getText().toString().trim();
                if (TextUtils.isEmpty(searchText)) {
                    Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
                } else {
                    searchLocationDatabaseForExactResult(searchText);
                }
                Util.closeKeyboard(this);
            }
            return handled;
        });

        final FloatingActionButton fab = findViewById(R.id.fab_adminModuleAddNewLocation);
        fab.setOnClickListener( (View v) ->
                showDialogFragment(null));

        listView.setOnItemClickListener((AdapterView<?> parent, View view,
                                          int position, long id) -> {
            if (actionMode != null) {
                ViewHolderItem holder = (ViewHolderItem) view.getTag();
                if (holder.deleteCheckbox.isChecked()) {
                    holder.deleteCheckbox.setChecked(false);
                    checkedForDeletion.set(position, false);
                } else {
                    holder.deleteCheckbox.setChecked(true);
                    checkedForDeletion.set(position, true);
                }
            }
        });

        listView.setOnItemLongClickListener((AdapterView<?> parent, View view,
                                              int position, long id) -> {
            if (actionMode == null) {
                actionMode = ((AppCompatActivity) view.getContext())
                        .startSupportActionMode(actionModeCallback);
                adapter.setInActionMode(true);
                ViewHolderItem holder = (ViewHolderItem) view.getTag();
                checkedForDeletion.set(position, true);
                holder.deleteCheckbox.setChecked(true);
                selectedCounter = 1;
            }
            return true;
        });

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (selectedPosition != position) {
                    selectedPosition = position;
                    String searchText = searchView.getText().toString().trim();
                    safeSearchLocationDatabaseAndListResults(searchText);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //must override, not needed
            }
        });
    }
    //endregion

    //region options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.admin_module_menu, menu);
        menu.findItem(R.id.menuitem_admin_module_locations).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        boolean result = Util.adminModuleOptionsActions(this, menuItem.getItemId());
        if (!result) {
            super.onOptionsItemSelected(menuItem);
        }
        return result;
    }
    //endregion

    private void showDialogFragment(Bundle bundle) {
        DialogFragment dialog = new LocationsDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), LocationsDialogFragment.class.getName());
    }

    //region Firebase interactions
    private void safeSearchLocationDatabaseAndListResults(String searchText) {
        if (TextUtils.isEmpty(searchText) && typeSpinner.getSelectedItemPosition() == 0) {
            Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
        } else {
            searchLocationDatabaseAndListResults(searchText);
        }
    }

    private void searchLocationDatabaseAndListResults(String searchText) {
        exactSearch = false;
        if (actionMode != null) actionMode.finish();
        databaseReference.child(LOCATIONS_TABLE).orderByChild(LOCATIONS_CHILD_NAME)
                .startAt(searchText)
                .endAt(searchText+"\uf8ff")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundLocations(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(UsersActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void searchLocationDatabaseForExactResult(String searchText) {
        exactSearch = true;
        if (actionMode != null) actionMode.finish();
        databaseReference.child(LOCATIONS_TABLE).orderByChild(LOCATIONS_CHILD_NAME)
                .equalTo(searchText)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundLocations(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(UsersActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void displayFoundLocations(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<Location> locations = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            Location location = snapshot.getValue(Location.class);
            if (location == null) {
                Log.e("display locations", "failed to create location from snapshot");
                return;
            }
            int searchType = typeSpinner.getSelectedItemPosition() - 1;
            if (searchType == -1 || searchType == location.getLocationType()) {
                location.setId(snapshot.getKey());
                locations.add(location);
            }
        }
        Util.setViewVisibilityAndUpdateAdapter
                (locations, emptyListView, listView, adapter);
    }

    public void addLocation(@NonNull Location location) {
        databaseReference.child(LOCATIONS_TABLE).push().setValue(location);
        performSearch();
    }

    public void editLocation(@NonNull Location location) {
        databaseReference.child(LOCATIONS_TABLE).child(location.getId()).setValue(location);
        performSearch();
    }

    private void performSearch() {
        String searchText = searchView.getText().toString().trim();
        if (exactSearch) {
            searchLocationDatabaseForExactResult(searchText);
        } else {
            safeSearchLocationDatabaseAndListResults(searchText);
        }
    }

    private void removeLocation(@NonNull String locationId) {
        Util.removeItemFromDatabaseTable(databaseReference, LOCATIONS_TABLE, locationId);
    }
    //end region

    private class LocationArrayAdapter extends RefreshAdapter<Location> {

        private Context context;
        private List<Location> locations;
        private boolean inActionMode;

        private LocationArrayAdapter(Context context, List<Location> locations) {
            super(context, locations);
            this.context = context;
            this.locations = locations;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater
                        .inflate(R.layout.admin_search_location_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.nameView = convertView.findViewById(R.id.textview_adminLocationSearch_name);
                holder.typeView = convertView
                        .findViewById(R.id.textview_adminLocationSearch_locationType);
                holder.locationView = convertView
                        .findViewById(R.id.textview_adminLocationSearch_location);
                holder.editButtonView = convertView
                        .findViewById(R.id.button_adminLocationSearch_edit);
                holder.deleteCheckbox = convertView
                        .findViewById(R.id.checkbox_adminLocationSearch_delete);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
                holder.deleteCheckbox.setOnCheckedChangeListener(null);
                holder.deleteCheckbox.setChecked(checkedForDeletion.get(position));
            }

            holder.nameView.setText(locations.get(position).getName());
            holder.typeView.setText
                    (Util.locationTypeMapper(context, locations.get(position).getLocationType()));
            holder.locationView.setText(locations.get(position).getLocation());

            holder.editButtonView.setOnClickListener( (View v) -> {
                Location location = locations.get(position);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_LOCATION_ID, location.getId());
                bundle.putString(KEY_LOCATION_NAME, location.getName());
                bundle.putInt(KEY_LOCATION_TYPE, location.getLocationType());
                bundle.putString(KEY_LOCATION_ACTUAL_LOCATION, location.getLocation());
                showDialogFragment(bundle);
            });

            holder.deleteCheckbox.setVisibility(inActionMode ? View.VISIBLE : View.GONE);
            holder.deleteCheckbox.setTag(String.valueOf(position));

            holder.deleteCheckbox.setOnCheckedChangeListener(
                    (CompoundButton buttonView, boolean isChecked) -> {
                        int pos = Integer.parseInt(buttonView.getTag().toString());
                        checkedForDeletion.set(pos, isChecked);
                        if (isChecked) {
                            selectedCounter++;
                            setActionModeTitle(actionMode);
                        } else {
                            selectedCounter--;
                            if (selectedCounter == 0) {
                                actionMode.finish();
                            } else {
                                setActionModeTitle(actionMode);
                            }
                        }
                    });

            return convertView;
        }

        void setInActionMode(boolean inActionMode) {
            this.inActionMode = inActionMode;
            notifyDataSetChanged();
        }

        @Override
        public void refreshList(List<Location> newLocations) {
            this.locations.clear();
            checkedForDeletion.clear();
            if (newLocations.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
            } else {
                this.locations.addAll(newLocations);
                for (int i = 0; i < newLocations.size(); i++) {
                    checkedForDeletion.add(false);
                }
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {

        TextView nameView;
        TextView typeView;
        TextView locationView;
        Button editButtonView;
        CheckBox deleteCheckbox;
    }
}
