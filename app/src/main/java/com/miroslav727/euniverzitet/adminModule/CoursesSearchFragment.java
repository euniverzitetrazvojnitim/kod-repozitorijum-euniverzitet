package com.miroslav727.euniverzitet.adminModule;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.Course;
import com.miroslav727.euniverzitet.util.ExactSearchHandler;
import com.miroslav727.euniverzitet.util.SearchMetadata;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.miroslav727.euniverzitet.adminModule.CoursesActivity.KEY_COURSE_CODE;
import static com.miroslav727.euniverzitet.adminModule.CoursesActivity.KEY_COURSE_ID;
import static com.miroslav727.euniverzitet.adminModule.CoursesActivity.KEY_COURSE_NAME;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_STUDENTS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_TEACHERS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_STUDIES;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_TEACHES;
import static com.miroslav727.euniverzitet.util.Constants.USERS_TABLE;

/**
 *
 */
public class CoursesSearchFragment extends Fragment implements DialogInterface.OnDismissListener {

    private TextInputEditText searchView;

    private TextView emptyView;

    private ListView courseListView;

    private CourseArrayAdapter courseAdapter;

    private List<Boolean> checkedForDeletion;

    private DatabaseReference databaseReference;

    private int selectedCounter;

    private ActionMode actionMode;

    private boolean exactSearch;

    private String lastSearch = "";


    //region action mode
    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.general_action_menu, menu);
            setActionModeTitle(mode);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menuitem_delete:

                    ArrayList<Course> remainingCourses = new ArrayList<>();

                    for (int i = 0; i < checkedForDeletion.size(); i++) {
                        Course course = courseAdapter.courses.get(i);
                        if (checkedForDeletion.get(i)) {
                            removeCourse(course.getId());
                        } else {
                            remainingCourses.add(course);
                        }
                    }
                    checkedForDeletion.clear();
                    courseAdapter.refreshList(remainingCourses);
                    for (int i = 0; i < remainingCourses.size(); i++) {
                        checkedForDeletion.set(i, false);
                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            courseAdapter.setInActionMode(false);
            selectedCounter = 0;
            for (int i = 0; i < checkedForDeletion.size(); i++) {
                checkedForDeletion.set(i, false);
            }
            actionMode = null;
        }
    };

    private void setActionModeTitle (@NonNull ActionMode mode) {
        mode.setTitle(getResources()
                .getQuantityString(R.plurals.selected, selectedCounter, selectedCounter));
    }
    //endregion

    public CoursesSearchFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater
                .inflate(R.layout.fragment_admin_module_courses_search, container, false);
        searchView = rootView.findViewById(R.id.tiledittext_adminModuleCourseSearch);
        emptyView = rootView.findViewById(R.id.textview_adminModuleCourseSearchEmptyList);
        courseListView = rootView.findViewById(R.id.listview_adminModuleCourseSearchResults);

        checkedForDeletion = new ArrayList<>();

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CoursesActivity activity = (CoursesActivity) getActivity();
        databaseReference = activity.getDatabaseReference();

        courseAdapter = new CourseArrayAdapter(activity, new ArrayList<>());
        courseListView.setAdapter(courseAdapter);

        addListeners(activity);
    }

    //region listeners
    private void addListeners(CoursesActivity activity) {
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SearchMetadata searchMetadata = Util.onSearchTextChanged(searchView, actionMode,
                        lastSearch, exactSearch, databaseReference,
                        emptyView, courseListView, courseAdapter);
                lastSearch = searchMetadata.getLastSearch();
                exactSearch = searchMetadata.isExactSearch();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        searchView.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            ExactSearchHandler handler = new ExactSearchHandler(lastSearch, exactSearch);
            handler.performSearch(actionId, searchView, databaseReference, emptyView,
                    courseListView, courseAdapter, activity);
            lastSearch = handler.getLastSearch();
            exactSearch = handler.isExactSearch();
            return handler.isHandled();
        });

        courseListView.setOnItemClickListener((AdapterView<?> parent, View view,
                                          int position, long id) -> {
            if (actionMode != null) {
                ViewHolderItem holder = (ViewHolderItem) view.getTag();
                if (holder.deleteCheckbox.isChecked()) {
                    holder.deleteCheckbox.setChecked(false);
                    checkedForDeletion.set(position, false);
                } else {
                    holder.deleteCheckbox.setChecked(true);
                    checkedForDeletion.set(position, true);
                }
            }
        });

        courseListView.setOnItemLongClickListener((AdapterView<?> parent, View view,
                                              int position, long id) -> {
            if (actionMode == null) {
                actionMode = activity.startSupportActionMode(actionModeCallback);
                courseAdapter.setInActionMode(true);
                ViewHolderItem holder = (ViewHolderItem) view.getTag();
                checkedForDeletion.set(position, true);
                holder.deleteCheckbox.setChecked(true);
                selectedCounter = 1;
            }
            return true;
        });

        final FloatingActionButton fab = getView().findViewById(R.id.fab_adminModuleAddNewCourse);
        fab.setOnClickListener( (View v) ->
                showDialogFragment(null));
    }
    //endregion

    private void showDialogFragment(Bundle bundle) {
        DialogFragment dialog = new CoursesDialogFragment();
        if (bundle == null) {
            bundle = new Bundle();
        }
        dialog.setArguments(bundle);
        dialog.show(getChildFragmentManager(), CoursesDialogFragment.class.getName());
    }

    //region Firebase interactions
    private void removeCourse(String courseId) {
        removeAllUsersFromCourse(courseId);
        try {
            //Wait 1 second to avoid race condition
            Thread.sleep(1000);
        } catch (Exception e) {
            Log.e("unable to wait", e.getMessage());
        }
        Util.removeItemFromDatabaseTable(databaseReference, COURSES_TABLE, courseId);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        String searchText = searchView.getText().toString().trim();
        if (exactSearch) {
            exactSearch = Util.searchCourseDatabaseForExactResult(searchText, actionMode,
                    databaseReference, emptyView, courseListView, courseAdapter);
            lastSearch = searchText;
        } else {
            exactSearch = Util.safeSearchCourseDatabaseAndListResults(searchText, actionMode,
                    databaseReference, emptyView, courseListView, courseAdapter);
        }
    }

    private void removeAllUsersFromCourse(String courseId) {
        removeAllStudentsFromCourse(courseId);
        removeAllTeachersFromCourse(courseId);
    }

    private void removeAllStudentsFromCourse(String courseId) {
        databaseReference.child(COURSES_TABLE).child(courseId).child(COURSES_CHILD_STUDENTS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            removeCourseFromUser(snapshot.getKey(), courseId, true);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e("Remove students error", databaseError.getMessage());
                    }
                });
    }

    private void removeAllTeachersFromCourse(String courseId) {
        databaseReference.child(COURSES_TABLE).child(courseId).child(COURSES_CHILD_TEACHERS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("teachers", " = "+dataSnapshot.getChildrenCount());
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            removeCourseFromUser(snapshot.getKey(), courseId, false);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e("Remove teachers error", databaseError.getMessage());
                    }
                });
    }

    private void removeCourseFromUser(String uId, String courseId, boolean student) {
        final String column = student ? USERS_CHILD_STUDIES : USERS_CHILD_TEACHES;
        databaseReference.child(USERS_TABLE).child(uId).child(column).child(courseId).removeValue();
    }
    //endregion

    private class CourseArrayAdapter extends RefreshAdapter<Course> {
        private Context context;
        private List<Course> courses;
        private boolean inActionMode;

        CourseArrayAdapter(@NonNull Context context, @NonNull List<Course> courses) {
            super(context, courses);
            this.context = context;
            this.courses = courses;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate
                        (R.layout.admin_search_course_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.resultView = convertView.findViewById(R.id.textview_admincoursesearch_course);
                holder.editButtonView = convertView.findViewById(R.id.button_admincoursesearch_edit);
                holder.courseTakersButtonView = convertView
                        .findViewById(R.id.button_admincoursesearch_coursetakers);
                holder.deleteCheckbox = convertView
                        .findViewById(R.id.checkbox_admincoursesearch_delete);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
                holder.deleteCheckbox.setOnCheckedChangeListener(null);
                holder.deleteCheckbox.setChecked(checkedForDeletion.get(position));
            }

            Course course = courses.get(position);
            String courseId = course.getId();
            String courseCode = course.getCode();
            String courseName = course.getName();
            String courseIdentifier = course.formCourseIdentifier();
            holder.resultView.setText(courseIdentifier);

            holder.editButtonView.setOnClickListener((View v) -> {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_COURSE_ID, courseId);
                bundle.putString(KEY_COURSE_CODE, courseCode);
                bundle.putString(KEY_COURSE_NAME, courseName);
                showDialogFragment(bundle);
            });

            holder.courseTakersButtonView.setOnClickListener((View v) -> {
                ((CoursesActivity) context).showPeopleFragment(courseCode, courseName, courseId);
            });

            holder.deleteCheckbox.setVisibility(inActionMode ? View.VISIBLE : View.GONE);
            holder.deleteCheckbox.setTag(String.valueOf(position));

            holder.deleteCheckbox.setOnCheckedChangeListener(
                    (CompoundButton buttonView, boolean isChecked) -> {
                        int pos = Integer.parseInt(buttonView.getTag().toString());
                        checkedForDeletion.set(pos, isChecked);
                        if (isChecked) {
                            selectedCounter++;
                            setActionModeTitle(actionMode);
                        } else {
                            selectedCounter--;
                            if (selectedCounter == 0) {
                                actionMode.finish();
                            } else {
                                setActionModeTitle(actionMode);
                            }
                        }
                    });
            return convertView;
        }

        void setInActionMode(boolean inActionMode) {
            this.inActionMode = inActionMode;
            notifyDataSetChanged();
        }


        @Override
        public void refreshList(List<Course> refreshedList) {
            courses.clear();
            checkedForDeletion.clear();
            if (refreshedList.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyView, courseListView);
            } else {
                courses.addAll(refreshedList);
                for (int i = 0; i < courses.size(); i++) {
                    checkedForDeletion.add(false);
                }
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {
        TextView resultView;
        Button editButtonView;
        Button courseTakersButtonView;
        CheckBox deleteCheckbox;
    }
}
