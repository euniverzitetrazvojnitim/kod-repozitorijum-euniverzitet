package com.miroslav727.euniverzitet.adminModule;

import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.miroslav727.euniverzitet.util.Constants.FIREBASE_SECONDARY_NAME;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_DELETED_USER;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_EMAIL;
import static com.miroslav727.euniverzitet.util.Constants.USERS_TABLE;

public class UsersActivity extends AppCompatActivity {

    public static final String KEY_USER_EMAIL = "KEY_USER_EMAIL";
    public static final String KEY_ROLES_ARRAY = "KEY_ROLES_ARRAY";
    public static final String KEY_SELECTED_POSITION = "KEY_SELECTED_POSITION";
    public static final String KEY_USER_ID = "KEY_USER_ID";

    private ActionMode mActionMode;

    private int mSelectedCounter;

    private TextInputEditText mSearchView;

    private Spinner mRoleSpinner;

    private int mSelectedPosition;

    private TextView mEmptyListView;

    private ListView mListView;

    private UserArrayAdapter mAdapter;

    private List<Boolean> mCheckedForDeletion;

    private String[] mActiveRoles;

    private FirebaseApp mSecondary;

    private DatabaseReference mDatabase;

    private boolean mExactSearch;

    //region action mode
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.general_action_menu, menu);
            setActionModeTitle(mode);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menuitem_delete:

                    User user = null;

                    for (int i = 0; i < mCheckedForDeletion.size(); i++) {
                        if (mCheckedForDeletion.get(i)) {
                            user = mAdapter.mUsers.get(i);
                            editUserRole(user.getUid(), ROLE_DELETED_USER);
                        }
                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.setInActionMode(false);
            mSelectedCounter = 0;
            for (int i = 0; i < mCheckedForDeletion.size(); i++) {
                mCheckedForDeletion.set(i, false);
            }
            mActionMode = null;
        }
    };

    private void setActionModeTitle (@NonNull ActionMode mode) {
        mode.setTitle(getResources()
                .getQuantityString(R.plurals.selected, mSelectedCounter, mSelectedCounter));
    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        mSearchView = findViewById(R.id.tiledittext_adminModuleSearch);
        mEmptyListView = findViewById(R.id.textview_adminSearchEmptyList);

        mCheckedForDeletion = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mActiveRoles = getResources().getStringArray(R.array.activeRoles);

        final String[] roles = getResources().getStringArray(R.array.allRoles);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item,
                roles);
        mRoleSpinner = findViewById(R.id.spinner_adminModuleRole);
        mRoleSpinner.setAdapter(spinnerAdapter);

        mListView = findViewById(R.id.listview_adminModuleSearchResults);
        mAdapter = new UserArrayAdapter(this, new ArrayList<>());
        mListView.setAdapter(mAdapter);

        addListeners();
    }

    //region listeners
    private void addListeners() {
        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchText = mSearchView.getText().toString().trim();
                safeSearchUserDatabaseAndListResults(searchText);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        mSearchView.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                handled = true;
                String searchText = mSearchView.getText().toString().trim();
                if (TextUtils.isEmpty(searchText)) {
                    Util.hideEmptyListAndShowEmptyView(mEmptyListView, mListView);
                } else {
                    searchUserDatabaseForExactResult(searchText);
                }
                Util.closeKeyboard(this);
            }
            return handled;
        });

        final FloatingActionButton fab = findViewById(R.id.fab_adminModuleAddNew);
        fab.setOnClickListener( (View v) ->
                showDialogFragment(null, mActiveRoles, 0, null));

        mListView.setOnItemClickListener((AdapterView<?> parent, View view,
                                          int position, long id) -> {
            if (mActionMode != null) {
                ViewHolderItem holder = (ViewHolderItem) view.getTag();
                if (holder.deleteCheckbox.isChecked()) {
                    holder.deleteCheckbox.setChecked(false);
                    mCheckedForDeletion.set(position, false);
                } else {
                    holder.deleteCheckbox.setChecked(true);
                    mCheckedForDeletion.set(position, true);
                }
            }
        });

        mListView.setOnItemLongClickListener((AdapterView<?> parent, View view,
                                              int position, long id) -> {
            if (mActionMode == null) {
                AppCompatActivity a = (AppCompatActivity) view.getContext();
                mActionMode = a.startSupportActionMode(mActionModeCallback);
                mAdapter.setInActionMode(true);
                ViewHolderItem holder = (ViewHolderItem) view.getTag();
                mCheckedForDeletion.set(position, true);
                holder.deleteCheckbox.setChecked(true);
                mSelectedCounter = 1;
            }
            return true;
        });

        mRoleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mSelectedPosition != position) {
                    mSelectedPosition = position;
                    String searchText = mSearchView.getText().toString().trim();
                    safeSearchUserDatabaseAndListResults(searchText);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //must override, not needed
            }
        });
    }
    //endregion

    //region options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.admin_module_menu, menu);
        menu.findItem(R.id.menuitem_admin_module_users).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        boolean result = Util.adminModuleOptionsActions(this, menuItem.getItemId());
        if (!result) {
            super.onOptionsItemSelected(menuItem);
        }
        return result;
    }
    //endregion

    private void showDialogFragment(String email, String[] roles, int selectedPosition, String uid)
    {
        mSecondary = Util.initialiseFirebaseApp(this, FIREBASE_SECONDARY_NAME);

        DialogFragment dialog = new UserDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_USER_EMAIL, email);
        bundle.putStringArray(KEY_ROLES_ARRAY, roles);
        bundle.putInt(KEY_SELECTED_POSITION, selectedPosition);
        bundle.putString(KEY_USER_ID, uid);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), UserDialogFragment.class.getName());
    }

    //region Firebase interactions
    public void createNewUser(String email, int role) {
        String pass = Util.passwordGenerator();

        FirebaseAuth.getInstance(mSecondary).createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener( (Task<AuthResult> task) -> {
                    if (task.isSuccessful()) {
                        addUserToDatabase(task.getResult().getUser().getUid(), email, role, pass);
                        FirebaseAuth.getInstance(mSecondary).signOut();
                    } else {
                        Log.e("The write failed: " ,task.getException().getMessage());
                    }
                    Util.destroyFirebaseApp(mSecondary);
                });
    }

    public void editUserRole(String uid, int role) {
        Util.editUserRole(uid, role);
        Util.destroyFirebaseApp(mSecondary);
    }

    public void addUserToDatabase(String uid, String email, int role, String pass) {
        Util.addUserToDatabase(uid, email, role, pass);
        String searchText = mSearchView.getText().toString().trim();
        if (mExactSearch) {
            searchUserDatabaseForExactResult(searchText);
        } else {
            safeSearchUserDatabaseAndListResults(searchText);
        }
    }

    private void safeSearchUserDatabaseAndListResults(String searchText) {
        if (TextUtils.isEmpty(searchText) && mRoleSpinner.getSelectedItemPosition() == 0) {
            Util.hideEmptyListAndShowEmptyView(mEmptyListView, mListView);
        } else {
            searchUserDatabaseAndListResults(searchText);
        }
    }

    private void searchUserDatabaseAndListResults(String searchText) {
        mExactSearch = false;
        if (mActionMode != null) mActionMode.finish();
        mDatabase.child(USERS_TABLE).orderByChild(USERS_CHILD_EMAIL)
                .startAt(searchText)
                .endAt(searchText+"\uf8ff")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundUsers(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(UsersActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void searchUserDatabaseForExactResult(String searchText) {
        mExactSearch = true;
        if (mActionMode != null) mActionMode.finish();
        mDatabase.child(USERS_TABLE).orderByChild(USERS_CHILD_EMAIL)
                .equalTo(searchText)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundUsers(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(UsersActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void displayFoundUsers(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<User> users = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            User user = snapshot.getValue(User.class);
            if (user == null) {
                Log.e("display users", "failed to create user from snapshot");
                return;
            }
            int searchRole = mRoleSpinner.getSelectedItemPosition() - 1;
            if (searchRole == -1 || searchRole == user.getRole()) {
                user.setUid(snapshot.getKey());
                users.add(user);
            }
        }
        Util.setViewVisibilityAndUpdateAdapter
                (users, mEmptyListView, mListView, mAdapter);
    }

    public void destroyLocalFirebaseApp() {
        Util.destroyFirebaseApp(mSecondary);
    }
    //endregion

    private class UserArrayAdapter extends RefreshAdapter<User> {

        private Context mContext;
        private List<User> mUsers;
        private boolean mInActionMode;

        private UserArrayAdapter(Context context, List<User> users) {
            super(context, users);
            this.mContext = context;
            this.mUsers = users;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(R.layout.admin_search_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.emailView = convertView.findViewById(R.id.textview_adminsearch_email);
                holder.roleView = convertView.findViewById(R.id.textview_adminsearch_role);
                holder.editButtonView = convertView.findViewById(R.id.button_adminsearch_edit);
                holder.resetPasswordButtonView = convertView.findViewById
                        (R.id.button_adminsearch_resetpassword);
                holder.deleteCheckbox = convertView.findViewById(R.id.checkbox_adminsearch_delete);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
                holder.deleteCheckbox.setOnCheckedChangeListener(null);
                holder.deleteCheckbox.setChecked(mCheckedForDeletion.get(position));
            }

            holder.emailView.setText(mUsers.get(position).getEmail());
            holder.roleView.setText(Util.roleMapper(mContext, mUsers.get(position).getRole()));

            holder.editButtonView.setOnClickListener( (View v) -> {
                User user = mUsers.get(position);
                int selectedPosition = user.getRole() <= 3 ? user.getRole() : 0;
                showDialogFragment(user.getEmail(), mActiveRoles, selectedPosition, user.getUid());
            });

            holder.resetPasswordButtonView.setOnClickListener( (View v) -> {
                User user = mUsers.get(position);
                FirebaseAuth.getInstance().sendPasswordResetEmail(user.getEmail());
            });

            holder.deleteCheckbox.setVisibility(mInActionMode ? View.VISIBLE : View.GONE);
            holder.deleteCheckbox.setTag(String.valueOf(position));

            holder.deleteCheckbox.setOnCheckedChangeListener(
                    (CompoundButton buttonView, boolean isChecked) -> {
                        int pos = Integer.parseInt(buttonView.getTag().toString());
                        mCheckedForDeletion.set(pos, isChecked);
                        if (isChecked) {
                            mSelectedCounter++;
                            setActionModeTitle(mActionMode);
                        } else {
                            mSelectedCounter--;
                            if (mSelectedCounter == 0) {
                                mActionMode.finish();
                            } else {
                                setActionModeTitle(mActionMode);
                            }
                        }
                    });

            return convertView;
        }

        void setInActionMode(boolean inActionMode) {
            mInActionMode = inActionMode;
            notifyDataSetChanged();
        }

        @Override
        public void refreshList(List<User> users) {
            mUsers.clear();
            mCheckedForDeletion.clear();
            if (users.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(mEmptyListView, mListView);
            } else {
                mUsers.addAll(users);
                for (int i = 0; i < users.size(); i++) {
                    mCheckedForDeletion.add(false);
                }
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {

        TextView emailView;
        TextView roleView;
        Button editButtonView;
        Button resetPasswordButtonView;
        CheckBox deleteCheckbox;
    }
}
