package com.miroslav727.euniverzitet.adminModule;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.util.Constants;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.miroslav727.euniverzitet.adminModule.CoursesActivity.KEY_COURSE_CODE;
import static com.miroslav727.euniverzitet.adminModule.CoursesActivity.KEY_COURSE_ID;
import static com.miroslav727.euniverzitet.adminModule.CoursesActivity.KEY_COURSE_NAME;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_STUDENTS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_TEACHERS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_EMAIL;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_STUDIES;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_TEACHES;
import static com.miroslav727.euniverzitet.util.Constants.USERS_TABLE;

/**
 *
 */
public class CoursesPeopleFragment extends Fragment {

    private DatabaseReference databaseReference;

    private TextInputEditText searchView;

    private TextView emptyView;

    private ListView peopleListView;

    private PeopleArrayAdapter peopleAdapter;

    private String courseId;

    public CoursesPeopleFragment() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        CoursesActivity activity = (CoursesActivity) getActivity();
        databaseReference = activity.getDatabaseReference();
        View rootView = inflater.inflate(R.layout.fragment_courses_people, container, false);
        searchView = rootView.findViewById(R.id.tiledittext_adminModuleCoursePeopleSearch);
        emptyView = rootView.findViewById(R.id.textview_adminModuleCoursePeopleSearchEmptyList);
        peopleListView = rootView.findViewById(R.id.listview_adminModuleCoursePeopleSearchResults);

        if (arguments != null) {
            TextView courseView = rootView
                    .findViewById(R.id.textview_adminModuleCreateCourseTitleRow2);
            courseView.setText(getCourse(arguments));
            courseId = arguments.getString(KEY_COURSE_ID);
        }

        peopleAdapter = new PeopleArrayAdapter(activity, new ArrayList<>());
        peopleListView.setAdapter(peopleAdapter);
        addListeners(activity, rootView);
        return rootView;
    }

    //region listeners
    private void addListeners(@NonNull CoursesActivity activity, @NonNull View rootView) {
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchText = searchView.getText().toString().trim();
                safeSearchUserDatabaseAndListResults(searchText);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        searchView.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                handled = true;
                String searchText = searchView.getText().toString().trim();
                if (TextUtils.isEmpty(searchText)) {
                    Util.hideEmptyListAndShowEmptyView(emptyView, peopleListView);
                } else {
                    searchUserDatabaseForExactResult(searchText);
                }
                Util.closeKeyboard(activity);
            }
            return handled;
        });

        Button finishButton = rootView.findViewById(R.id.button_createCourse);
        finishButton.setOnClickListener((View v) -> {
            activity.removePeopleFragment();
        });
    }
    //endregion

    private String getCourse(@NonNull Bundle arguments) {
        return arguments.getString(KEY_COURSE_CODE) + (" - ") + arguments.getString(KEY_COURSE_NAME);
    }

    //region Firebase interactions
    private void safeSearchUserDatabaseAndListResults(String searchText) {
        if (TextUtils.isEmpty(searchText)) {
            Util.hideEmptyListAndShowEmptyView(emptyView, peopleListView);
        } else {
            searchUserDatabaseAndListResults(searchText);
        }
    }

    private void searchUserDatabaseAndListResults(String searchText) {
        databaseReference.child(USERS_TABLE).orderByChild(USERS_CHILD_EMAIL)
                .startAt(searchText)
                .endAt(searchText + "\uf8ff")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<User> users = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            User user = snapshot.getValue(User.class);
                            int roleId = user.getRole();
                            if (roleId == Constants.ROLE_STUDENT
                                    || roleId == Constants.ROLE_TEACHING_STAFF) {
                                user.setUid(snapshot.getKey());
                                users.add(user);
                            }
                        }
                        Util.setViewVisibilityAndUpdateAdapter
                                (users, emptyView, peopleListView, peopleAdapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(CoursesActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void searchUserDatabaseForExactResult(String searchText) {
        databaseReference.child(USERS_TABLE).orderByChild(USERS_CHILD_EMAIL)
                .equalTo(searchText)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<User> users = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            User user = snapshot.getValue(User.class);
                            int roleId = user.getRole();
                            if (roleId == Constants.ROLE_STUDENT
                                    || roleId == Constants.ROLE_TEACHING_STAFF) {
                                user.setUid(snapshot.getKey());
                                users.add(user);
                            }
                        }
                        Util.setViewVisibilityAndUpdateAdapter
                                (users, emptyView, peopleListView, peopleAdapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(CoursesActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void setupAddRemoveButton(Button button, String uId, boolean student, boolean teacher) {
        String idOfListToCheck = "";
        if (student) {
            idOfListToCheck = COURSES_CHILD_STUDENTS;
        } else if (teacher) {
            idOfListToCheck = COURSES_CHILD_TEACHERS;
        } else {
            return;
        }
        databaseReference.child(COURSES_TABLE).child(courseId).child(idOfListToCheck).child(uId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        boolean inCourse = dataSnapshot.getValue() != null;
                        button.setOnClickListener((View v) -> {
                            if (inCourse) {
                                removePersonFromCourse(uId, student);
                            } else {
                                addPersonToCourse(uId, student);
                            }
                        });
                        if (student && inCourse) {
                            button.setText(R.string.remove_student_from_course);
                        } else if (student) {
                            button.setText(R.string.add_student_to_course);
                        } else if (inCourse) {
                            button.setText(R.string.remove_teacher_from_course);
                        } else {
                            button.setText(R.string.add_teacher_to_course);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e("Database Error", databaseError.getMessage());
                    }
                });
    }

    /**
     * Add user to courses table and course to users table
     * @param uId id of user
     * @param student if true user is added as a student to courses table and course is added into
     *                studies of users table, otherwise user is added as a teacher to courses table
     *                and course is added into teaches of users table
     */
    private void addPersonToCourse(String uId, boolean student) {
        final String courseColumn = student ? COURSES_CHILD_STUDENTS : COURSES_CHILD_TEACHERS;
        final String usersColumn = student ? USERS_CHILD_STUDIES : USERS_CHILD_TEACHES;
        databaseReference.child(COURSES_TABLE)
                .child(courseId)
                .child(courseColumn)
                .child(uId)
                .setValue(true);
        databaseReference.child(USERS_TABLE)
                .child(uId)
                .child(usersColumn)
                .child(courseId)
                .setValue(true);
    }

    /**
     * Removes user from courses table and course from users table
     * @param uId id of user
     * @param student if true user is removed from students of course and course is removed from
     *                courses user studies. Otherwise user is removed from teachers of course and
     *                course is removed from courses user teaches
     */
    private void removePersonFromCourse(String uId, boolean student) {
        final String courseColumn = student ? COURSES_CHILD_STUDENTS : COURSES_CHILD_TEACHERS;
        final String usersColumn = student ? USERS_CHILD_STUDIES : USERS_CHILD_TEACHES;
        databaseReference.child(COURSES_TABLE)
                .child(courseId)
                .child(courseColumn)
                .child(uId)
                .removeValue();
        databaseReference.child(USERS_TABLE)
                .child(uId)
                .child(usersColumn)
                .child(courseId)
                .removeValue();
    }
    //endregion

    private class PeopleArrayAdapter extends RefreshAdapter<User> {

        private Context context;
        private List<User> people;

        PeopleArrayAdapter(@NonNull Context context, @NonNull List<User> people) {
            super(context, people);
            this.context = context;
            this.people = people;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate
                        (R.layout.admin_course_person_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.emailView = convertView.findViewById(R.id.textview_admincourseperson_email);
                holder.roleView = convertView.findViewById(R.id.textview_admincourseperson_role);
                holder.addRemoveButtonView = convertView
                        .findViewById(R.id.button_admincourseperson_add_remove);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
            }

            User person = people.get(position);
            String uId = person.getUid();
            holder.emailView.setText(person.getEmail());

            int roleId = person.getRole();
            boolean student = roleId == Constants.ROLE_STUDENT;
            boolean teacher = roleId == Constants.ROLE_TEACHING_STAFF;
            holder.roleView.setText(Util.roleMapper(context, roleId));

            setupAddRemoveButton(holder.addRemoveButtonView, uId, student, teacher);
            //todo listener gets added to wrong views randomly sometimes, investigate if time permits
            return convertView;
        }

        public void refreshList(List<User> users) {
            people.clear();
            if (users.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyView, peopleListView);
            } else {
                people.addAll(users);
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {
        TextView emailView;
        TextView roleView;
        Button addRemoveButtonView;
    }
}
