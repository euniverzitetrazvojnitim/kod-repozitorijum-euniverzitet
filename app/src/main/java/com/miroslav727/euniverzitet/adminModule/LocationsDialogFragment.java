package com.miroslav727.euniverzitet.adminModule;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.Location;
import com.miroslav727.euniverzitet.util.Util;

import static com.miroslav727.euniverzitet.adminModule.LocationsActivity.KEY_LOCATION_ACTUAL_LOCATION;
import static com.miroslav727.euniverzitet.adminModule.LocationsActivity.KEY_LOCATION_ID;
import static com.miroslav727.euniverzitet.adminModule.LocationsActivity.KEY_LOCATION_NAME;
import static com.miroslav727.euniverzitet.adminModule.LocationsActivity.KEY_LOCATION_TYPE;

public class LocationsDialogFragment extends DialogFragment {

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        LocationsActivity activity = (LocationsActivity) getActivity();

        Bundle arguments = getArguments();
        boolean addLocation = arguments == null || arguments.getString(KEY_LOCATION_ID) == null;

        String[] typeArray = getResources().getStringArray(R.array.activeLocationTypes);
        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<>(context, R.layout.custom_spinner_item, typeArray);

        String locationId = addLocation ? null : arguments.getString(KEY_LOCATION_ID);
        String name = addLocation ? null : arguments.getString(KEY_LOCATION_NAME);
        String actualLocation = addLocation ? null
                : arguments.getString(KEY_LOCATION_ACTUAL_LOCATION);
        int locationType = addLocation ? 0 : arguments.getInt(KEY_LOCATION_TYPE);

        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogLayout = inflater.inflate(R.layout.add_or_edit_location_dialog, null);
        TextInputEditText nameField = dialogLayout
                .findViewById(R.id.tiledittext_locationDialog_name);
        TextInputEditText locationField = dialogLayout
                .findViewById(R.id.tiledittext_locationDialog_location);

        Spinner typeSpinner = dialogLayout.findViewById(R.id.spinner_locationDialog_locationType);
        typeSpinner.setAdapter(spinnerAdapter);
        typeSpinner.setSelection(locationType);

        if (!addLocation) {
            TextInputLayout nameLayout = dialogLayout.findViewById(R.id.til_locationDialog_name);
            nameLayout.setHint(getString(R.string.location_name_label));
            TextInputLayout locationLayout = dialogLayout
                    .findViewById(R.id.til_locationDialog_location);
            locationLayout.setHint(getString(R.string.actual_location_label));
            nameField.setText(name);
            locationField.setText(actualLocation);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity,
                R.style.generalDialogTheme);

        builder.setTitle(addLocation ?
                activity.getString(R.string.new_location) :
                activity.getString(R.string.edit_location))
                .setView(dialogLayout)
                .setPositiveButton(R.string.ok_dialog, (DialogInterface dialog, int id) -> {

                    String locationName = nameField.getText().toString().trim();
                    String location = locationField.getText().toString().trim();
                    int type = typeSpinner.getSelectedItemPosition();

                    if (addLocation) {
                        activity.addLocation(new Location(locationName, location, type, null));
                    } else {
                        activity.editLocation(new Location(locationName, location, type, locationId));
                    }
                })
                .setNegativeButton(R.string.cancel_dialog,  (DialogInterface dialog, int id) -> {
                    Log.d(LocationsDialogFragment.this.getTag(), "Canceled");
                });

        AlertDialog locationDialog = builder.create();
        locationDialog.show();

        final Button confirmButton = locationDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        if (addLocation) Util.disableView(confirmButton);

        TextWatcher confirmController = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Has to be overridden, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String name = nameField.getText().toString().trim();
                String location = locationField.getText().toString().trim();
                if (!TextUtils.isEmpty(location) && !TextUtils.isEmpty(name)) {
                    Util.enableView(confirmButton);
                } else {
                    Util.disableView(confirmButton);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Has to be overridden, not needed
            }
        };
        locationField.addTextChangedListener(confirmController);
        nameField.addTextChangedListener(confirmController);

        return locationDialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView title = this.getDialog()
                .findViewById(androidx.appcompat.R.id.alertTitle);
        if (title != null) {
            title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
    }
}
