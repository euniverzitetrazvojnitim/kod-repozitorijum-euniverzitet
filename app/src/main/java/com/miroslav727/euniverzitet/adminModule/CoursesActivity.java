package com.miroslav727.euniverzitet.adminModule;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.Course;
import com.miroslav727.euniverzitet.util.Util;

import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_CODE;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_NAME;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_TABLE;

public class CoursesActivity extends AppCompatActivity {

    public static final String KEY_COURSE_ID = "KEY_COURSE_ID";
    public static final String KEY_COURSE_CODE = "KEY_COURSE_CODE";
    public static final String KEY_COURSE_NAME = "KEY_COURSE_NAME";

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (findViewById(R.id.layout_adminModuleCoursesFragmentContainer) != null) {
            if (savedInstanceState != null) {
                return;
            }

            CoursesSearchFragment searchFragment = new CoursesSearchFragment();

            getSupportFragmentManager().beginTransaction().add(
                    R.id.layout_adminModuleCoursesFragmentContainer, searchFragment).commit();
        }

    }

    //region options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.admin_module_menu, menu);
        menu.findItem(R.id.menuitem_admin_module_classes).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        boolean result = Util.adminModuleOptionsActions(this, menuItem.getItemId());
        if (!result) {
            super.onOptionsItemSelected(menuItem);
        }
        return result;
    }
    //endregion

    /**
     * Creates a new course in database and returns its key
     * @return Key of newly created database row
     */
    public String createCourse(String courseCode, String courseName) {
        Course course = new Course();
        course.setCode(courseCode);
        course.setName(courseName);
        //push does not actually commit, set value does, push generates key
        DatabaseReference dr = mDatabase.child(COURSES_TABLE).push();
        dr.setValue(course);
        return dr.getKey();
    }

    public void showPeopleFragment(String courseCode, String courseName, String courseId) {
        CoursesPeopleFragment peopleFragment = new CoursesPeopleFragment();
        Bundle arguments = new Bundle();
        arguments.putString(KEY_COURSE_CODE, courseCode);
        arguments.putString(KEY_COURSE_NAME, courseName);
        arguments.putString(KEY_COURSE_ID, courseId);
        peopleFragment.setArguments(arguments);
        Util.replaceFragment(getSupportFragmentManager(), peopleFragment,
                R.id.layout_adminModuleCoursesFragmentContainer);
    }

    public void removePeopleFragment() {
        getSupportFragmentManager().popBackStack();
    }

    public DatabaseReference getDatabaseReference() {
        return mDatabase;
    }

    public void editCourseCodeAndName(String courseId, String courseCode, String courseName) {
        DatabaseReference course = mDatabase.child(COURSES_TABLE).child(courseId);
        course.child(COURSES_CHILD_CODE).setValue(courseCode);
        course.child(COURSES_CHILD_NAME).setValue(courseName);
    }

}
