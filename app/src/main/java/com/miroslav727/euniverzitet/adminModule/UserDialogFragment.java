package com.miroslav727.euniverzitet.adminModule;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.util.Util;

import static com.miroslav727.euniverzitet.adminModule.UsersActivity.*;

public class UserDialogFragment extends DialogFragment {

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        UsersActivity activity = (UsersActivity)getActivity();
        Bundle arguments = getArguments();

        ArrayAdapter<String> spinnerAdapter = null;
        int selected = 0;
        final String userEmail = arguments != null ? arguments.getString(KEY_USER_EMAIL) : null;
        final String userId = arguments != null ? arguments.getString(KEY_USER_ID) : null;
        final boolean addUser = TextUtils.isEmpty(userEmail);

        if (arguments != null) {
            String[] rolesArray = arguments.getStringArray(KEY_ROLES_ARRAY);
            spinnerAdapter = new ArrayAdapter<>(context,
                    R.layout.custom_spinner_item,
                    rolesArray);
            selected = arguments.getInt(KEY_SELECTED_POSITION, 0);
        }

        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogLayout = inflater.inflate(R.layout.add_or_edit_user_dialog, null);
        TextInputEditText emailField = dialogLayout.findViewById(R.id.tiledittext_userdialog_email);

        if (!addUser) {
            TextInputLayout textInputLayout = dialogLayout.findViewById(R.id.til_userdialog_email);
            textInputLayout.setHint(getString(R.string.email_label));
            emailField.setText(userEmail);
            Util.disableView(emailField);
        }

        Spinner roleSpinner = dialogLayout.findViewById(R.id.spinner_userdialog_role);
        roleSpinner.setAdapter(spinnerAdapter);
        roleSpinner.setSelection(selected);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity,
                R.style.generalDialogTheme);

        builder.setTitle(addUser ?
                activity.getString(R.string.new_user) : activity.getString(R.string.edit_user))
                .setView(dialogLayout)
                .setPositiveButton(R.string.ok_dialog, (DialogInterface dialog, int id) -> {
                    if (addUser) {
                        activity.createNewUser(emailField.getText().toString().trim(),
                                roleSpinner.getSelectedItemPosition());
                    } else {
                        activity.editUserRole(userId, roleSpinner.getSelectedItemPosition());
                    }
                })
                .setNegativeButton(R.string.cancel_dialog,  (DialogInterface dialog, int id) -> {
                    activity.destroyLocalFirebaseApp();
                });
        AlertDialog userDialog = builder.create();
        userDialog.show();

        //Enabling confirm button only when a valid email is entered
        if (addUser) {
            final Button confirmButton = userDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            Util.disableView(confirmButton);
            emailField.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    //Has to be overridden, not needed
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String email = s.toString().trim();
                    if (!TextUtils.isEmpty(email) && Util.isValidEmail(email)) {
                        Util.enableView(confirmButton);
                    } else {
                        Util.disableView(confirmButton);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    //Has to be overridden, not needed
                }
            });
        }

        return userDialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView title = this.getDialog()
                .findViewById(androidx.appcompat.R.id.alertTitle);
        if (title != null) {
            title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
    }
}
