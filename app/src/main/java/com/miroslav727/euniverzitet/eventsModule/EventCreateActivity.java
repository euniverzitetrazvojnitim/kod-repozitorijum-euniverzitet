package com.miroslav727.euniverzitet.eventsModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.Course;
import com.miroslav727.euniverzitet.model.Event;
import com.miroslav727.euniverzitet.model.Location;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.testModule.SimpleCourseArrayAdapter;
import com.miroslav727.euniverzitet.util.ExactSearchHandler;
import com.miroslav727.euniverzitet.util.SearchMetadata;
import com.miroslav727.euniverzitet.util.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.eventsModule.UserCreatedEventsActivity.FLAG_EVENT_ID;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_USER_ROLE;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_LESSONS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.EVENTS_CHILD_INVITEES;
import static com.miroslav727.euniverzitet.util.Constants.EVENTS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.LOCATIONS_CHILD_EVENTS;
import static com.miroslav727.euniverzitet.util.Constants.LOCATIONS_CHILD_NAME;
import static com.miroslav727.euniverzitet.util.Constants.LOCATIONS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_ACCEPTED;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_INVITED;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_LESSON;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_PRIVATE_EVENT;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_PUBLIC_EVENT;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_EMAIL;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_EVENTS;
import static com.miroslav727.euniverzitet.util.Constants.USERS_TABLE;

public class EventCreateActivity extends AppCompatActivity {

    private TextView title;

    private RelativeLayout locationLayout;
    private ScrollView detailsLayout;
    private RelativeLayout courseLayout;
    private RelativeLayout inviteesLayout;

    private TextInputEditText searchLocations;
    private Spinner locationTypeFilter;
    private TextView emptyLocationsListView;
    private ListView locationsListView;
    private SimpleLocationArrayAdapter locationAdapter;
    private int selectedLocationType;
    private String oldLocationId;

    private Button actionButton;

    private TextInputLayout eventTitleWrapper;
    private TextInputEditText eventTitle;
    private TextInputEditText eventDescription;
    private TextView locationDisplay;
    private Button changeLocation;
    private TextView startDate;
    private Button setStartDate;
    private TextView startTime;
    private Button setStartTime;
    private TextView endDate;
    private Button setEndDate;
    private TextView endTime;
    private Button setEndTime;
    private Spinner eventTypeFilter;
    private RelativeLayout courseDisplayLayout;
    private TextView course;
    private Button setCourse;
    private String oldCourseId;

    private TextInputEditText searchCourses;
    private TextView emptyCoursesListView;
    private ListView coursesListView;
    private SimpleCourseArrayAdapter courseAdapter;
    private String lastCourseSearch;
    private boolean exactCourseSearch;

    private TextInputEditText searchUsers;
    private Spinner userRoleFilter;
    private TextView emptyUsersListView;
    private ListView userListView;
    private SimpleInviteesArrayAdapter inviteesAdapter;
    private int selectedUserRole;

    private Event draftEvent;

    private boolean edit;

    private int role;

    private DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_create);

        role = getIntent().getIntExtra(FLAG_USER_ROLE, -1);

        databaseReference = Util.getRootReference();
        auth = FirebaseAuth.getInstance();
        compositeDisposable = new CompositeDisposable();

        String eventId = getIntent().getStringExtra(FLAG_EVENT_ID);
        edit = !TextUtils.isEmpty(eventId);

        if (!edit) {
            draftEvent = new Event();
            draftEvent.setType(TYPE_PUBLIC_EVENT);
        } else {
            Scheduler thread = Schedulers.newThread();
            compositeDisposable.add(
                    Util.observeEventById(eventId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(thread)
                    .subscribe(event -> {
                        oldLocationId = event.getLocationId();
                        if (event.getType() == TYPE_LESSON) {
                            oldCourseId = event.getCourseId();
                        }
                        draftEvent = event;
                        eventTitle.setText(event.getTitle());
                        if (!TextUtils.isEmpty(event.getDescription())) {
                            eventDescription.setText(event.getDescription());
                        }
                        setStartTime(event.getStartHour(), event.getStartMinute());
                        setEndTime(event.getEndHour(), event.getEndMinute());
                        Calendar date = Calendar.getInstance();
                        date.setTimeInMillis(event.getStartDate());
                        setStartDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH),
                                date.get(Calendar.DAY_OF_MONTH));
                        date.setTimeInMillis(event.getEndDate());
                        setEndDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH),
                                date.get(Calendar.DAY_OF_MONTH));
                        eventTypeFilter.setSelection(event.getType());
                        compositeDisposable.add(
                                Util.observeLocationById(event.getLocationId())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribeOn(thread)
                                        .subscribe(location -> {
                                            locationDisplay.setText(location.getName());
                                            if (event.getType() == TYPE_LESSON) {
                                                compositeDisposable.add(
                                                        Util.observeCourseById(event.getCourseId())
                                                        .observeOn(AndroidSchedulers.mainThread())
                                                        .subscribeOn(thread)
                                                        .subscribe(foundCourse -> {
                                                            course.setText(foundCourse
                                                                    .formCourseIdentifier());
                                                            setCourse.setText(getString
                                                                    (R.string.change));
                                                            courseDisplayLayout.setVisibility
                                                                    (View.VISIBLE);
                                                            showDetails(true);
                                                        })
                                                );
                                            } else {
                                                if (event.getType() == TYPE_PRIVATE_EVENT) {
                                                    actionButton.setText(getString(R.string.next));
                                                }
                                                showDetails(true);
                                            }
                                        })
                        );
                        showDetails(true);
                    })
            );
        }

        initViews();

        addListeners();

        final String[] locationTypes = getResources().getStringArray(R.array.allLocationTypes);
        ArrayAdapter<String> locationSpinnerAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item,
                locationTypes);
        locationTypeFilter.setAdapter(locationSpinnerAdapter);

        locationAdapter = new SimpleLocationArrayAdapter(this, new ArrayList<>());
        locationsListView.setAdapter(locationAdapter);

        final String[] eventTypes = getResources().getStringArray(R.array.eventsByType);
        ArrayAdapter<String> eventSpinnerAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item,
                eventTypes);
        eventTypeFilter.setAdapter(eventSpinnerAdapter);

        courseAdapter = new SimpleCourseArrayAdapter(this, new ArrayList<>(),
                emptyCoursesListView, coursesListView);
        coursesListView.setAdapter(courseAdapter);

        final String[] roles = getResources().getStringArray(R.array.allActiveRoles);
        ArrayAdapter<String> roleSpinnerAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item,
                roles);
        userRoleFilter.setAdapter(roleSpinnerAdapter);

        inviteesAdapter = new SimpleInviteesArrayAdapter(this, new ArrayList<>());
        userListView.setAdapter(inviteesAdapter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    private void initViews() {
        title = findViewById(R.id.textview_eventCreate_title);

        locationLayout = findViewById(R.id.layout_eventCreate_location);
        detailsLayout = findViewById(R.id.layout_eventCreate_details);
        inviteesLayout = findViewById(R.id.layout_eventCreate_users);
        courseLayout = findViewById(R.id.layout_eventCreate_searchCourse);

        searchLocations = findViewById(R.id.tiledittext_eventCreate_location);
        locationTypeFilter = findViewById(R.id.spinner_eventCreate_locationType);
        emptyLocationsListView = findViewById(R.id.textview_eventCreate_locationEmptyList);
        locationsListView = findViewById(R.id.listview_eventCreate_locationSearchResults);

        actionButton = findViewById(R.id.button_eventCreate_action);

        eventTitleWrapper = findViewById(R.id.til_eventCreate_title);
        eventTitle = findViewById(R.id.tiledittext_eventCreate_title);
        eventDescription = findViewById(R.id.tiledittext_eventCreate_description);
        locationDisplay = findViewById(R.id.textview_eventCreate_locationDisplay);
        changeLocation = findViewById(R.id.button_eventCreate_changeLocation);
        startDate = findViewById(R.id.textview_eventCreate_startDate);
        setStartDate = findViewById(R.id.button_eventCreate_setStartDate);
        startTime = findViewById(R.id.textview_eventCreate_startTime);
        setStartTime = findViewById(R.id.button_eventCreate_setStartTime);
        endDate = findViewById(R.id.textview_eventCreate_endDate);
        setEndDate = findViewById(R.id.button_eventCreate_setEndDate);
        endTime = findViewById(R.id.textview_eventCreate_endTime);
        setEndTime = findViewById(R.id.button_eventCreate_setEndTime);
        eventTypeFilter = findViewById(R.id.spinner_eventCreate_type);
        courseDisplayLayout = findViewById(R.id.layout_eventCreate_courseDisplay);
        course = findViewById(R.id.textview_eventCreate_courseDisplay);
        setCourse = findViewById(R.id.button_eventCreate_setCourse);

        searchCourses = findViewById(R.id.tiledittext_eventCreate_courseSearch);
        emptyCoursesListView = findViewById(R.id.textview_eventCreate_courseSearchEmptyList);
        coursesListView = findViewById(R.id.listview_eventCreate_courseSearchResults);
        lastCourseSearch = "";

        searchUsers = findViewById(R.id.tiledittext_eventCreate_peopleSearch);
        userRoleFilter = findViewById(R.id.spinner_eventCreate_role);
        emptyUsersListView = findViewById(R.id.textview_eventCreate_peopleEmptyList);
        userListView = findViewById(R.id.listview_eventCreate_peopleSearchResults);

    }

    //region listeners
    private void addListeners() {
        searchLocations.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchText = searchLocations.getText().toString().trim();
                safeSearchLocationDatabaseAndListResults(searchText);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        searchLocations.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                handled = true;
                String searchText = searchLocations.getText().toString().trim();
                if (TextUtils.isEmpty(searchText)) {
                    Util.hideEmptyListAndShowEmptyView(emptyLocationsListView, locationsListView);
                } else {
                    searchLocationDatabaseForExactResult(searchText);
                }
                Util.closeKeyboard(this);
            }
            return handled;
        });

        emptyLocationsListView.setOnClickListener(view -> {
            showLocationsDialog();
        });

        locationsListView.setOnItemClickListener((adapterView, view, position, id) -> {
            selectLocation(locationAdapter.locations.get(position));
        });

        locationTypeFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (selectedLocationType != position) {
                    selectedLocationType = position;
                    String searchText = searchLocations.getText().toString().trim();
                    safeSearchLocationDatabaseAndListResults(searchText);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //must override, not needed
            }
        });

        eventTypeFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int previousPosition = draftEvent.getType();
                if (position != previousPosition) {
                    draftEvent.setType(position);
                    switch (position) {
                        case TYPE_PUBLIC_EVENT:
                            if (previousPosition == TYPE_PRIVATE_EVENT) {
                                actionButton.setText(getString(R.string.finish));
                            }
                            if (previousPosition == TYPE_LESSON) {
                                resetSelectedCourse();
                            }
                            break;
                        case TYPE_PRIVATE_EVENT:
                            actionButton.setText(getString(R.string.next));
                            if (previousPosition == TYPE_LESSON) {
                                resetSelectedCourse();
                            }
                            break;
                        case TYPE_LESSON:
                            if (previousPosition == TYPE_PRIVATE_EVENT) {
                                actionButton.setText(getString(R.string.finish));
                            }
                            courseDisplayLayout.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        changeLocation.setOnClickListener(view -> showLocations());

        setStartDate.setOnClickListener(view -> showDateSelector(true));

        setEndDate.setOnClickListener(view -> showDateSelector(false));

        setStartTime.setOnClickListener(view -> showTimeSelector(true));

        setEndTime.setOnClickListener(view -> showTimeSelector(false));

        setCourse.setOnClickListener(view -> showCourseSelector());

        searchCourses.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SearchMetadata searchMetadata = Util.onSearchTextChanged(searchCourses, null,
                        lastCourseSearch, exactCourseSearch, databaseReference,
                        emptyCoursesListView, coursesListView, courseAdapter);
                lastCourseSearch = searchMetadata.getLastSearch();
                exactCourseSearch = searchMetadata.isExactSearch();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        searchCourses.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            ExactSearchHandler handler = new ExactSearchHandler(lastCourseSearch, exactCourseSearch);
            handler.performSearch(actionId, searchCourses, databaseReference, emptyCoursesListView,
                    coursesListView, courseAdapter, this);
            lastCourseSearch = handler.getLastSearch();
            exactCourseSearch = handler.isExactSearch();
            return handler.isHandled();
        });

        searchUsers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchText = searchUsers.getText().toString().trim();
                safeSearchUserDatabaseAndListResults(searchText);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        searchUsers.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                handled = true;
                String searchText = searchUsers.getText().toString().trim();
                if (TextUtils.isEmpty(searchText)) {
                    Util.hideEmptyListAndShowEmptyView(emptyUsersListView, userListView);
                } else {
                    searchUserDatabaseForExactResult(searchText);
                }
                Util.closeKeyboard(this);
            }
            return handled;
        });

        userRoleFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (selectedUserRole != position) {
                    selectedUserRole = position;
                    String searchText = searchUsers.getText().toString().trim();
                    safeSearchUserDatabaseAndListResults(searchText);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //must override, not needed
            }
        });

        coursesListView.setOnItemClickListener((adapterView, view, position, l) -> {
            selectCourse(courseAdapter.getCourses().get(position));
        });

        actionButton.setOnClickListener(view -> {
            boolean details = detailsLayout.getVisibility() == View.VISIBLE;
            if (details) {
                if (TextUtils.isEmpty(eventTitle.getText().toString())) {
                    eventTitleWrapper.setError(getString(R.string.mandatory_field));
                    return;
                } else {
                    draftEvent.setTitle(eventTitle.getText().toString());
                    eventTitleWrapper.setError(null);
                    eventTitleWrapper.setErrorEnabled(false);
                }
                String noDateSet = getString(R.string.no_date_set);
                String noTimeSet = getString(R.string.no_time_set);
                if (startDate.getText().equals(noDateSet) || endDate.getText().equals(noDateSet) ||
                startTime.getText().equals(noTimeSet) || endTime.getText().equals(noTimeSet)) {
                    Toast.makeText(this, getString(R.string.input_event_time),
                            Toast.LENGTH_LONG).show();
                    return;
                }
                long currentTime = Calendar.getInstance().getTimeInMillis();
                long startTime = Util.formTimeFromDateAndOffsets(draftEvent.getStartDate(),
                        draftEvent.getStartHour(), draftEvent.getStartMinute());
                if (startTime < currentTime) {
                    Toast.makeText(this, getString(R.string.event_can_not_start_in_past),
                            Toast.LENGTH_LONG).show();
                    return;
                }
                long endTime = Util.formTimeFromDateAndOffsets(draftEvent.getEndDate(),
                        draftEvent.getEndHour(), draftEvent.getEndMinute());
                if (startTime > endTime) {
                    Toast.makeText(this, getString(R.string.event_can_not_end_before_it_starts),
                            Toast.LENGTH_LONG).show();
                    return;
                }
                switch (draftEvent.getType()) {
                    case TYPE_PUBLIC_EVENT:
                        createEvent();
                        finish();
                        break;
                    case TYPE_PRIVATE_EVENT:
                        createEvent();
                        showInvite();
                        break;
                    case TYPE_LESSON:
                        if (course.getText().equals(getString(R.string.no_course_set))) {
                            Toast.makeText(this, getString(R.string.course_required),
                                    Toast.LENGTH_LONG).show();
                            return;
                        }
                        createEvent();
                        inviteAllCourseTakers();
                        finish();
                        break;
                }
            } else {
                finish();
            }
        });
    }
    //endregion

    //region Firebase interactions
    private void safeSearchLocationDatabaseAndListResults(String searchText) {
        if (TextUtils.isEmpty(searchText) && locationTypeFilter.getSelectedItemPosition() == 0) {
            Util.hideEmptyListAndShowEmptyView(emptyLocationsListView, locationsListView);
        } else {
            searchLocationDatabaseAndListResults(searchText);
        }
    }

    private void searchLocationDatabaseAndListResults(String searchText) {
        databaseReference.child(LOCATIONS_TABLE).orderByChild(LOCATIONS_CHILD_NAME)
                .startAt(searchText)
                .endAt(searchText+"\uf8ff")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundLocations(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(EventCreateActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void searchLocationDatabaseForExactResult(String searchText) {
        databaseReference.child(LOCATIONS_TABLE).orderByChild(LOCATIONS_CHILD_NAME)
                .equalTo(searchText)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundLocations(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(EventCreateActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void displayFoundLocations(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<Location> locations = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            Location location = snapshot.getValue(Location.class);
            if (location == null) {
                Log.e("display locations", "failed to create location from snapshot");
                return;
            }
            int searchType = locationTypeFilter.getSelectedItemPosition() - 1;
            if (searchType == -1 || searchType == location.getLocationType()) {
                location.setId(snapshot.getKey());
                locations.add(location);
            }
        }
        Util.setViewVisibilityAndUpdateAdapter
                (locations, emptyLocationsListView, locationsListView, locationAdapter);
    }

    private void safeSearchUserDatabaseAndListResults(String searchText) {
        if (TextUtils.isEmpty(searchText) && userRoleFilter.getSelectedItemPosition() == 0) {
            Util.hideEmptyListAndShowEmptyView(emptyUsersListView, userListView);
        } else {
            searchUserDatabaseAndListResults(searchText);
        }
    }

    private void searchUserDatabaseAndListResults(String searchText) {
        databaseReference.child(USERS_TABLE).orderByChild(USERS_CHILD_EMAIL)
                .startAt(searchText)
                .endAt(searchText+"\uf8ff")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundUsers(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(EventCreateActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void searchUserDatabaseForExactResult(String searchText) {
        databaseReference.child(USERS_TABLE).orderByChild(USERS_CHILD_EMAIL)
                .equalTo(searchText)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundUsers(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(EventCreateActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void displayFoundUsers(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<User> users = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            User user = snapshot.getValue(User.class);
            if (user == null) {
                Log.e("display users", "failed to create user from snapshot");
                return;
            }
            int searchRole = userRoleFilter.getSelectedItemPosition() - 1;
            if (searchRole == -1 || searchRole == user.getRole()) {
                user.setUid(snapshot.getKey());
                users.add(user);
            }
        }
        Util.setViewVisibilityAndUpdateAdapter
                (users, emptyUsersListView, userListView, inviteesAdapter);
    }

    public void addLocation(@NonNull Location location) {
        DatabaseReference dr = databaseReference.child(LOCATIONS_TABLE).push();
        dr.setValue(location)
                .addOnSuccessListener(aVoid -> {
            location.setId(dr.getKey());
            selectLocation(location);
        });
    }

    public void createEvent() {
        if (TextUtils.isEmpty(eventDescription.getText().toString())) {
            draftEvent.setDescription("");
        } else {
            draftEvent.setDescription(eventDescription.getText().toString());
        }
        DatabaseReference dr;
        if (!edit) {
            draftEvent.setCreatorId(auth.getUid());
            dr = Util.getRootReference().child(EVENTS_TABLE).push();
            String eventId = dr.getKey();
            draftEvent.setId(eventId);
            dr.setValue(draftEvent).addOnSuccessListener(aVoid -> {
                addEventToLocation(draftEvent.getLocationId());
                if (draftEvent.getType() == TYPE_LESSON) {
                    addEventToCourse(draftEvent.getCourseId());
                }
            });
        } else {
            dr = Util.getRootReference().child(EVENTS_TABLE).child(draftEvent.getId());
            Scheduler thread = Schedulers.newThread();
            boolean lesson = draftEvent.getType() == TYPE_LESSON;
            if (!draftEvent.getLocationId().equals(oldLocationId)) {
                removeEventFromLocation(oldLocationId);
                addEventToLocation(draftEvent.getLocationId());
            }
            if (!TextUtils.isEmpty(oldCourseId) && !lesson) {
                removeEventFromCourse(oldCourseId);
            }
            if (lesson) {
                if (!TextUtils.isEmpty(oldCourseId)
                        && !oldCourseId.equals(draftEvent.getCourseId())) {
                    removeEventFromCourse(oldCourseId);
                    addEventToCourse(draftEvent.getCourseId());
                } else if (TextUtils.isEmpty(oldCourseId)){
                    addEventToCourse(draftEvent.getCourseId());
                }
                compositeDisposable.add(
                        Util.observeAllEventInviteesIds(draftEvent.getId())
                                .subscribeOn(thread)
                                .observeOn(thread)
                                .subscribe(ids -> {
                                    for (String id : ids) {
                                        removeUserFromEvent(id);
                                    }
                                    compositeDisposable.add(
                                            Util.observeCoursesStudentIds(draftEvent.getCourseId())
                                                    .subscribeOn(thread)
                                                    .observeOn(thread)
                                                    .subscribe(uids ->
                                                            dr.setValue(draftEvent)
                                                                    .addOnSuccessListener(aVoid -> {
                                                                for (String uid : uids) {
                                                                    inviteUserToEvent(uid, true);
                                                                }
                                                            }))
                                    );
                                })
                );

            } else {
                compositeDisposable.add(
                        Util.observeAllEventInviteesIds(draftEvent.getId())
                                .subscribeOn(thread)
                                .observeOn(thread)
                                .subscribe(ids ->
                                        dr.setValue(draftEvent).addOnSuccessListener(aVoid -> {
                                    for (String uid : ids) {
                                        inviteUserToEvent(uid, lesson);
                                    }
                                }))
                );
            }

        }

    }

    private void addEventToCourse(String courseId) {
        databaseReference.child(COURSES_TABLE).child(courseId).child(COURSES_CHILD_LESSONS)
                .child(draftEvent.getId()).setValue(true);
    }

    private void removeEventFromCourse(String courseId) {
        Util.removeEventFromCourse(draftEvent.getId(), courseId);
    }

    private void addEventToLocation(String locationId) {
        databaseReference.child(LOCATIONS_TABLE).child(locationId).child(LOCATIONS_CHILD_EVENTS)
                .child(draftEvent.getId()).setValue(true);
    }

    private void removeEventFromLocation(String locationId) {
        Util.removeEventFromLocation(draftEvent.getId(), locationId);
    }

    private void inviteAllCourseTakers() {
        Scheduler thread = Schedulers.newThread();
        compositeDisposable.add(
                Util.observeCoursesStudentIds(draftEvent.getCourseId())
                .observeOn(thread)
                .subscribeOn(thread)
                .subscribe(studentIds -> {
                    for (String studentId : studentIds) {
                        inviteUserToEvent(studentId, true);
                    }
                })
        );
    }

    private void inviteUserToEvent(String uId, boolean autoAccept) {
        Util.addEventToUser(uId, draftEvent.getId(), autoAccept ? STATUS_ACCEPTED : STATUS_INVITED);
        Util.addUserToEvent(draftEvent.getId(), uId, autoAccept ? STATUS_ACCEPTED : STATUS_INVITED);
    }

    private void setupSendCancelButton(Button button, String uId) {
        databaseReference.child(EVENTS_TABLE)
                .child(draftEvent.getId())
                .child(EVENTS_CHILD_INVITEES)
                .child(uId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        boolean invited = dataSnapshot.getValue() != null;
                        button.setOnClickListener((View v) -> {
                            if (invited) {
                                removeUserFromEvent(uId);
                            } else {
                                inviteUserToEvent(uId, false);
                            }
                        });
                        if (invited) {
                            button.setText(R.string.cancel_invitation);
                        } else {
                            button.setText(R.string.send_invitation);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e("Database Error", databaseError.getMessage());
                    }
                });
    }

    private void removeUserFromEvent(String uId) {
        databaseReference.child(EVENTS_TABLE)
                .child(draftEvent.getId())
                .child(EVENTS_CHILD_INVITEES)
                .child(uId)
                .removeValue();
        Util.removeEventFromUser(draftEvent.getId(), uId);
    }
    //endregion

    private void resetSelectedCourse() {
        courseDisplayLayout.setVisibility(View.GONE);
        draftEvent.setCourseId(null);
        course.setText(getString(R.string.no_course_set));
        setCourse.setText(getString(R.string.set));
    }

    private void selectLocation(Location location) {
        draftEvent.setLocationId(location.getId());
        locationDisplay.setText(location.getName());
        showDetails(true);
    }

    private void selectCourse(Course course) {
        draftEvent.setCourseId(course.getId());
        this.course.setText(course.formCourseIdentifier());
        setCourse.setText(getString(R.string.change));
        showDetails(false);
    }

    private void showLocationsDialog() {
        DialogFragment dialog = new EventLocationDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(FLAG_USER_ROLE, role);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), EventLocationDialogFragment.class.getName());
    }

    private void showDetails(boolean fromLocations) {
        title.setText(getString(R.string.input_event_details));
        if (fromLocations) {
            locationLayout.setVisibility(View.GONE);
        } else {
            courseLayout.setVisibility(View.GONE);
        }
        Util.closeKeyboard(this);
        actionButton.setVisibility(View.VISIBLE);
        detailsLayout.setVisibility(View.VISIBLE);
    }

    private void showLocations() {
        locationLayout.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.select_location));
        actionButton.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.GONE);
    }

    private void showInvite() {
        detailsLayout.setVisibility(View.GONE);
        inviteesLayout.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.invite_users));
        actionButton.setText(getString(R.string.finish));
    }

    private void showCourseSelector() {
        courseLayout.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.select_course));
        actionButton.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.GONE);
    }

    public void showTimeSelector(boolean start) {
        TimeSelectionFragment timeSelectionFragment = new TimeSelectionFragment();
        timeSelectionFragment.start = start;
        if (start && !startTime.getText().equals(getString(R.string.no_time_set))) {
            timeSelectionFragment.previousHour = draftEvent.getStartHour();
            timeSelectionFragment.previousMinute = draftEvent.getStartMinute();
        } else if (!start && !endTime.getText().equals(getString(R.string.no_time_set))) {
            timeSelectionFragment.previousHour = draftEvent.getEndHour();
            timeSelectionFragment.previousMinute = draftEvent.getEndMinute();
        } else {
            timeSelectionFragment.time = Calendar.getInstance();
        }
        timeSelectionFragment.show(getSupportFragmentManager(), "timeSelection");
    }

    private void setStartTime(int hour, int minute) {
        draftEvent.setStartHour(hour);
        draftEvent.setStartMinute(minute);
        String hourString = Util.formatTimeString(hour);
        String minuteString = Util.formatTimeString(minute);
        String time = hourString + ":"+minuteString;
        startTime.setText(time);
        setStartTime.setText(getString(R.string.change));
    }

    private void setEndTime(int hour, int minute) {
        draftEvent.setEndHour(hour);
        draftEvent.setEndMinute(minute);
        String hourString = Util.formatTimeString(hour);
        String minuteString = Util.formatTimeString(minute);
        String time = hourString + ":"+minuteString;
        endTime.setText(time);
        setEndTime.setText(getString(R.string.change));
    }

    public void showDateSelector(boolean start) {
        DateSelectionFragment dateSelectionFragment = new DateSelectionFragment();
        dateSelectionFragment.start = start;
        Calendar date = Calendar.getInstance();
        if (start && draftEvent.getStartDate() != 0) {
            date.setTimeInMillis(draftEvent.getStartDate());
        } else if (!start && draftEvent.getEndDate() != 0) {
            date.setTimeInMillis(draftEvent.getEndDate());
        }
        dateSelectionFragment.date = date;
        dateSelectionFragment.show(getSupportFragmentManager(), "dateSelection");
    }

    private void setStartDate(int year, int month, int day) {
        Calendar selected = Calendar.getInstance();
        selected.set(year, month, day);
        selected.set(Calendar.HOUR_OF_DAY, 0);
        selected.set(Calendar.MINUTE, 0);
        draftEvent.setStartDate(selected.getTimeInMillis());
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy.");
        startDate.setText(formatter.format(selected.getTime()));
        setStartDate.setText(getString(R.string.change));
    }

    private void setEndDate(int year, int month, int day) {
        Calendar selected = Calendar.getInstance();
        selected.set(year, month, day);
        selected.set(Calendar.HOUR_OF_DAY, 0);
        selected.set(Calendar.MINUTE, 0);
        draftEvent.setEndDate(selected.getTimeInMillis());
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy.");
        endDate.setText(formatter.format(selected.getTime()));
        setEndDate.setText(getString(R.string.change));
    }

    public static class TimeSelectionFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        private boolean start;
        private Calendar time;
        private int previousHour;
        private int previousMinute;

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int hour = time != null ? time.get(Calendar.HOUR_OF_DAY) : previousHour;
            int minute = time != null ? time.get(Calendar.MINUTE) : previousMinute;

            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            EventCreateActivity activity = (EventCreateActivity)getActivity();
            if (start) {
                activity.setStartTime(hourOfDay, minute);
            } else {
                activity.setEndTime(hourOfDay, minute);
            }
        }

    }

    public static class DateSelectionFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private boolean start;
        private Calendar date;

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int year = date.get(Calendar.YEAR);
            int month = date.get(Calendar.MONTH);
            int day = date.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            EventCreateActivity activity = (EventCreateActivity)getActivity();
            if (start) {
                activity.setStartDate(year, month, day);
            } else {
                activity.setEndDate(year, month, day);
            }
        }
    }

    private class SimpleLocationArrayAdapter extends RefreshAdapter<Location> {

        private Context context;
        private List<Location> locations;

        private SimpleLocationArrayAdapter(Context context, List<Location> locations) {
            super(context, locations);
            this.context = context;
            this.locations = locations;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            LocationViewHolderItem holder;
            Location location = locations.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.event_location_list_item, parent, false);

                holder = new LocationViewHolderItem();
                holder.locationView = convertView.findViewById(R.id.textview_eventLocation_location);
                holder.typeView = convertView.findViewById(R.id.textview_eventLocation_locationType);

                convertView.setTag(holder);
            } else {
                holder = (LocationViewHolderItem) convertView.getTag();
            }

            holder.locationView.setText(location.getName());
            holder.typeView.setText(Util.locationTypeMapper(context,location.getLocationType()));

            return convertView;
        }

        @Override
        public void refreshList(List<Location> newLocations) {
            locations.clear();
            if (newLocations.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyLocationsListView, locationsListView);
            } else {
                locations.addAll(newLocations);
                notifyDataSetChanged();
            }
        }
    }

    private static class LocationViewHolderItem {

        TextView locationView;
        TextView typeView;
    }

    private class SimpleInviteesArrayAdapter extends RefreshAdapter<User> {

        private Context context;
        private List<User> invitees;

        private SimpleInviteesArrayAdapter(Context context, List<User> invitees) {
            super(context, invitees);
            this.context = context;
            this.invitees = invitees;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;
            User user = invitees.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.event_user_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.emailView = convertView.findViewById(R.id.textview_eventUser_email);
                holder.roleView = convertView.findViewById(R.id.textview_eventUser_role);
                holder.sendCancelButton = convertView.findViewById(R.id.button_eventUser_sendCancel);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
            }

            holder.emailView.setText(invitees.get(position).getEmail());
            holder.roleView.setText(Util.roleMapper(context, invitees.get(position).getRole()));

            setupSendCancelButton(holder.sendCancelButton, user.getUid());

            return convertView;
        }

        @Override
        public void refreshList(List<User> users) {
            invitees.clear();
            if (users.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyUsersListView, userListView);
            } else {
                invitees.addAll(users);
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {

        TextView emailView;
        TextView roleView;
        Button sendCancelButton;
    }
}
