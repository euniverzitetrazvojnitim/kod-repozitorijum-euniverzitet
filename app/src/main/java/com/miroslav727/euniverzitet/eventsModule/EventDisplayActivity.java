package com.miroslav727.euniverzitet.eventsModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.eventsModule.UserCreatedEventsActivity.FLAG_EVENT_ID;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_USER_ROLE;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_ADMINISTRATOR;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_ACCEPTED;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_INVITED;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_REJECTED;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_LESSON;

public class EventDisplayActivity extends AppCompatActivity {

    private int role;
    private String eventId;

    private FirebaseAuth auth;
    private CompositeDisposable compositeDisposable;
    private User currentUser;

    private ScrollView detailsLayout;
    private RelativeLayout attendeesLayout;

    private TextView title;
    private TextView type;
    private TextView courseLabel;
    private TextView course;
    private TextView scheduledFor;
    private TextView endsAt;
    private TextView location;
    private TextView descriptionLabel;
    private TextView description;
    private TextView attendeeNumber;
    private ListView attendeesListView;
    private AttendeeArrayAdapter attendeeAdapter;
    private TextView noAttendees;
    private Button accept;
    private Button reject;
    private Button edit;
    private Button showAttendees;
    private Button hideAttendees;
    private TextView attendeesTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_display);
        Scheduler thread = Schedulers.newThread();

        auth = FirebaseAuth.getInstance();
        compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(
                Util.observeUserFromDatabase(auth.getUid())
                .subscribeOn(thread)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> currentUser = user)
        );


        role = getIntent().getIntExtra(FLAG_USER_ROLE, -1);
        eventId = getIntent().getStringExtra(FLAG_EVENT_ID);

        initViews();

        attendeeAdapter = new AttendeeArrayAdapter(this, new ArrayList<>());
        attendeesListView.setAdapter(attendeeAdapter);
        attendeeNumber.setText("0");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Scheduler thread = Schedulers.newThread();

        compositeDisposable.add(
                Util.observeEventById(eventId)
                .subscribeOn(thread)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(event -> {
                    compositeDisposable.add(
                            Util.observeLocationById(event.getLocationId())
                                    .subscribeOn(thread)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(eventLocation -> {
                                        location.setText(eventLocation.getName());
                                    })
                    );
                    if (event.getType() == TYPE_LESSON) {
                        compositeDisposable.add(
                                Util.observeCourseById(event.getCourseId())
                                        .subscribeOn(thread)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(eventCourse -> {
                                            course.setText(eventCourse.formCourseIdentifier());
                                        })
                        );
                    } else {
                        courseLabel.setVisibility(View.GONE);
                        course.setVisibility(View.GONE);
                    }
                    String titleText = getString(R.string.event_title).concat(event.getTitle());
                    String attendeesTitleText = getString(R.string.attendees_of_event)
                            .concat(event.getTitle());
                    title.setText(titleText);
                    attendeesTitle.setText(attendeesTitleText);
                    type.setText(Util.eventTypeMapper(this, event.getType()));
                    scheduledFor.setText(Util.formTimestamp
                            (event.getStartDate(), event.getStartHour(), event.getStartMinute()));
                    endsAt.setText(Util.formTimestamp
                            (event.getEndDate(), event.getEndHour(), event.getEndMinute()));
                    if (TextUtils.isEmpty(event.getDescription())) {
                        descriptionLabel.setVisibility(View.GONE);
                        description.setVisibility(View.GONE);
                    } else {
                        description.setText(event.getDescription());
                    }
                    if (!event.getCreatorId().equals(auth.getUid()) && role != ROLE_ADMINISTRATOR) {
                        edit.setVisibility(View.GONE);
                    }
                    if (event.getType() != TYPE_LESSON) {
                        compositeDisposable.add(
                                Util.observeUserEventInviteStatus(auth.getUid(), eventId)
                                .subscribeOn(thread)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(status -> {
                                    if (TextUtils.isEmpty(status) || status.equals(STATUS_INVITED)) {
                                        accept.setClickable(true);
                                        accept.setText(getString(R.string.accept_invitation));
                                        reject.setClickable(true);
                                        reject.setText(getString(R.string.reject_invitation));
                                    } else if (status.equals(STATUS_ACCEPTED)) {
                                        setupAttending();
                                    } else if (status.equals(STATUS_REJECTED)) {
                                        setupRejected();
                                    }
                                })
                        );
                    } else {
                        accept.setClickable(false);
                        accept.setText(getString(R.string.accepted_event));
                        reject.setVisibility(View.GONE);
                    }
                    detailsLayout.setVisibility(View.VISIBLE);
                })
        );

        refreshAttendees(thread);

        addListeners();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    private void initViews() {
        detailsLayout = findViewById(R.id.layout_eventDisplay_details);
        attendeesLayout = findViewById(R.id.layout_eventDisplay_attendees);
        title = findViewById(R.id.textview_eventDisplay_title);
        type = findViewById(R.id.textview_eventDisplay_type);
        courseLabel = findViewById(R.id.textview_eventDisplay_courseLabel);
        course = findViewById(R.id.textview_eventDisplay_course);
        scheduledFor = findViewById(R.id.textview_eventDisplay_scheduledFor);
        endsAt = findViewById(R.id.textview_eventDisplay_endsAt);
        location = findViewById(R.id.textview_eventDisplay_location);
        descriptionLabel = findViewById(R.id.textview_eventDisplay_descriptionLabel);
        description = findViewById(R.id.textview_eventDisplay_description);
        accept = findViewById(R.id.button_eventDisplay_accept);
        reject = findViewById(R.id.button_eventDisplay_reject);
        edit = findViewById(R.id.button_eventDisplay_edit);
        attendeeNumber = findViewById(R.id.textview_eventDisplay_attendeeNumber);
        attendeesListView = findViewById(R.id.listview_eventDisplay_attendees);
        noAttendees = findViewById(R.id.textview_eventDisplay_noAttendees);
        showAttendees = findViewById(R.id.button_eventDisplay_show);
        hideAttendees = findViewById(R.id.button_eventDisplay_hide);
        attendeesTitle = findViewById(R.id.textview_eventDisplay_attendeesTitle);
    }

    private void addListeners() {
        accept.setOnClickListener(view -> {
            Util.inviteUserToEvent(eventId, auth.getUid(), STATUS_ACCEPTED);
            setupAttending();
            attendeeAdapter.attendees.add(currentUser);
            attendeeAdapter.notifyDataSetChanged();
            int number = Integer.parseInt(attendeeNumber.getText().toString()) + 1;
            if (number == 1) {
                noAttendees.setVisibility(View.GONE);
                attendeesListView.setVisibility(View.VISIBLE);
            }
            attendeeNumber.setText(""+number);
        });

        reject.setOnClickListener(view -> {
            Util.inviteUserToEvent(eventId, auth.getUid(), STATUS_REJECTED);
            setupRejected();
            if (attendeeAdapter.attendees.contains(currentUser)) {
                attendeeAdapter.attendees.remove(currentUser);
                attendeeAdapter.notifyDataSetChanged();
                int number = Integer.parseInt(attendeeNumber.getText().toString()) - 1;
                if (number == 0) {
                    noAttendees.setVisibility(View.VISIBLE);
                    attendeesListView.setVisibility(View.GONE);
                }
                attendeeNumber.setText(""+number);
            }
        });

        edit.setOnClickListener(view -> Util.startEditEvent(this, role, eventId));

        showAttendees.setOnClickListener(view -> {
            detailsLayout.setVisibility(View.GONE);
            attendeesLayout.setVisibility(View.VISIBLE);
        });

        hideAttendees.setOnClickListener(view -> {
            detailsLayout.setVisibility(View.VISIBLE);
            attendeesLayout.setVisibility(View.GONE);
        });
    }

    private void refreshAttendees(Scheduler thread) {
        compositeDisposable.add(
                Util.observeAllEventInviteesIds(eventId)
                .subscribeOn(thread)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uIds -> {
                    List<User> attendees = new ArrayList<>();
                    List<String> nonAttendeeIds = new ArrayList<>();
                    for (String uId : uIds) {
                        compositeDisposable.add(
                                Util.observeUserEventInviteStatus(uId, eventId)
                                .subscribeOn(thread)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(status -> {
                                    if (status.equals(STATUS_ACCEPTED)) {
                                        compositeDisposable.add(
                                                Util.observeUserFromDatabase(uId)
                                                .subscribeOn(thread)
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(attendee -> {
                                                    attendees.add(attendee);
                                                    if ((attendees.size() + nonAttendeeIds.size())
                                                            == uIds.size()) {
                                                        attendeeNumber.setText(""+attendees.size());
                                                        Util.setViewVisibilityAndUpdateAdapter(
                                                                attendees, noAttendees,
                                                                attendeesListView, attendeeAdapter);
                                                    }
                                                })
                                        );

                                    } else {
                                        nonAttendeeIds.add(uId);
                                        if ((attendees.size() + nonAttendeeIds.size())
                                                == uIds.size()) {
                                            attendeeNumber.setText(""+attendees.size());
                                            Util.setViewVisibilityAndUpdateAdapter(attendees,
                                                    noAttendees, attendeesListView, attendeeAdapter);
                                        }
                                    }
                                })
                        );
                    }
                })
        );
    }

    private void setupAttending() {
        accept.setClickable(false);
        accept.setText(getString(R.string.accepted_event));
        reject.setClickable(true);
        reject.setText(getString(R.string.reject_invitation));
    }

    private void setupRejected() {
        accept.setClickable(true);
        accept.setText(getString(R.string.accept_invitation));
        reject.setClickable(false);
        reject.setText(getString(R.string.rejected_event));
    }

    private class AttendeeArrayAdapter extends RefreshAdapter<User> {
        private Context context;
        private List<User> attendees;

        private AttendeeArrayAdapter(Context context, List<User> attendees) {
            super(context, attendees);
            this.context = context;
            this.attendees = attendees;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;
            User attendee = attendees.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.event_attendee_list_item, parent, false);

                holder = new ViewHolderItem();

                holder.email = convertView.findViewById(R.id.textview_eventAttendee_email);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
            }

            holder.email.setText(attendee.getEmail());

            return convertView;
        }

        @Override
        public void refreshList(List<User> newAttendees) {
            attendees.clear();
            if (newAttendees.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(noAttendees, attendeesListView);
            } else {
                attendees.addAll(newAttendees);
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {
        TextView email;
    }
}
