package com.miroslav727.euniverzitet.eventsModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.Event;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_USER_ROLE;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_ADMINISTRATOR;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_NON_TEACHING_STAFF;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEACHING_STAFF;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_ACCEPTED;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_INVITED;
import static com.miroslav727.euniverzitet.util.Constants.STATUS_REJECTED;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_PRIVATE_EVENT;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_PUBLIC_EVENT;

public class EventsDisplayActivity extends AppCompatActivity {

    private int role;

    private Spinner attendanceFilter;
    private int selectedPosition;

    private TextInputLayout daysInputWrapper;
    private TextInputEditText daysInput;
    Button applyButton;

    private TextView emptyListView;
    private ListView listView;
    AcceptableEventArrayAdapter adapter;

    private FloatingActionButton fab;

    private FirebaseAuth auth;
    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_display);

        auth = FirebaseAuth.getInstance();
        compositeDisposable = new CompositeDisposable();

        role = getIntent().getIntExtra(FLAG_USER_ROLE, -1);

        daysInputWrapper = findViewById(R.id.til_eventsDisplay_showDaysWrapper);
        daysInput = findViewById(R.id.tiledittext_eventsDisplay_showDays);
        applyButton = findViewById(R.id.button_eventsDisplay_apply);
        emptyListView = findViewById(R.id.textview_eventsDisplay_emptyList);
        listView = findViewById(R.id.listview_eventsDisplay_searchResults);
        fab = findViewById(R.id.fab_eventsDisplay_userEvents);

        if (role != ROLE_TEACHING_STAFF && role != ROLE_NON_TEACHING_STAFF
                && role != ROLE_ADMINISTRATOR) {
            fab.setVisibility(View.GONE);
        }

        attendanceFilter = findViewById(R.id.spinner_eventsDisplay_attendance);
        ArrayAdapter<String> attendanceAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item_gravity_end,
                getResources().getStringArray(R.array.allEventsByAttendance));
        attendanceFilter.setAdapter(attendanceAdapter);

        adapter = new AcceptableEventArrayAdapter
                (this, new ArrayList<>());
        listView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();

        displayEvents();

        addListeners();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    private void addListeners() {
        applyButton.setOnClickListener(view -> {
            displayEvents();
        });


        fab.setOnClickListener(view -> {
            Intent startUserEvents = new Intent(this, UserCreatedEventsActivity.class);
            startUserEvents.putExtra(FLAG_USER_ROLE, role);
            startActivity(startUserEvents);
        });

        attendanceFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (selectedPosition != position) {
                    selectedPosition = position;
                    displayEvents();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        listView.setOnItemClickListener((adapterView, view, position, l) -> {
            Util.showEvent(this, role, adapter.events.get(position).getId());
        });
    }

    private void displayEvents() {
        attendanceFilter.setEnabled(false);
        applyButton.setEnabled(false);

        int receivedDays = TextUtils.isEmpty(daysInput.getText().toString()) ? 7 :
                Integer.parseInt(daysInput.getText().toString());

        int days = receivedDays > 21 ? 21 : receivedDays;

        if (receivedDays > 21) {
            daysInput.setText("21");
            daysInputWrapper.setError(getString(R.string.can_not_be_higher_than_twentyone));
        } else {
            daysInputWrapper.setError(null);
            daysInputWrapper.setErrorEnabled(false);
        }

        Scheduler thread = Schedulers.newThread();


        String status = Util
                .eventAttendanceMapper(this, attendanceFilter.getSelectedItemPosition());
        compositeDisposable.add(
                Util.observePublicEventsInDays(auth.getUid(), days, status)
                        .subscribeOn(thread)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(events -> compositeDisposable.add(
                                Util.observeEventsUserIsInvitedToInDays
                                        (auth.getUid(), days, status)
                                        .subscribeOn(thread)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(invitedEvents -> {
                                            for (Event event : invitedEvents) {
                                                if (!events.contains(event)) {
                                                    events.add(event);
                                                }
                                            }
                                            if (!TextUtils.isEmpty(status) &&
                                                    status.equals(STATUS_INVITED)) {
                                                determineEventsToDisplay(events, thread);
                                            } else {
                                                displayEventsAndEnableInput(events);
                                            }

                                        })
                        ))

        );
    }

    private void determineEventsToDisplay(List<Event> events, Scheduler thread) {
        List<Event> eventsToDispay = new ArrayList<>();
        List<Event> invalidEvents = new ArrayList<>();
        for (Event event : events) {
            if (event.getType() == TYPE_PRIVATE_EVENT) {
                eventsToDispay.add(event);
                if ((eventsToDispay.size()
                        + invalidEvents.size())
                        == events.size()) {
                    displayEventsAndEnableInput(eventsToDispay);
                }
            } else if (event.getType() == TYPE_PUBLIC_EVENT) {
                compositeDisposable.add(
                        Util.observeUserEventInviteStatus
                                (auth.getUid(), event.getId())
                                .subscribeOn(thread)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(inviteStatus -> {
                                    if (TextUtils.isEmpty(inviteStatus)
                                            || inviteStatus.equals(STATUS_INVITED)) {
                                        eventsToDispay.add(event);
                                    } else {
                                        invalidEvents.add(event);
                                    }
                                    if ((eventsToDispay.size()
                                            + invalidEvents.size())
                                            == events.size()) {
                                        displayEventsAndEnableInput(eventsToDispay);
                                    }
                                })
                );
            }

        }
    }

    private void displayEventsAndEnableInput(List<Event> events) {
        Collections.sort(events);
        Util.setViewVisibilityAndUpdateAdapter
                (events, emptyListView, listView, adapter);
        attendanceFilter.setEnabled(true);
        applyButton.setEnabled(true);
    }

    private class AcceptableEventArrayAdapter extends RefreshAdapter<Event> {

        private Context context;
        private List<Event> events;

        private AcceptableEventArrayAdapter(Context context, List<Event> events) {
            super(context, events);
            this.context = context;
            this.events = events;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;
            Event event = events.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.event_acceptable_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.titleView = convertView
                        .findViewById(R.id.textview_acceptableEventDisplay_title);
                holder.scheduledForView = convertView
                        .findViewById(R.id.textview_acceptableEventDisplay_scheduledFor);
                holder.endsAtView = convertView
                        .findViewById(R.id.textview_acceptableEventDisplay_endsAt);
                holder.buttonsLayout = convertView
                        .findViewById(R.id.layout_acceptableEventDisplay_buttons);
                holder.acceptButtonView = convertView
                        .findViewById(R.id.button_acceptableEventDisplay_accept);
                holder.rejectButtonView = convertView
                        .findViewById(R.id.button_acceptableEventDisplay_reject);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
            }

            holder.titleView.setText(event.getTitle());
            holder.scheduledForView.setText(Util.formTimestamp
                    (event.getStartDate(), event.getStartHour(), event.getStartMinute()));
            holder.endsAtView.setText(Util.formTimestamp
                    (event.getEndDate(), event.getEndHour(), event.getEndMinute()));

            if (event.getType() == TYPE_PUBLIC_EVENT || event.getType() == TYPE_PRIVATE_EVENT) {
                holder.buttonsLayout.setVisibility(View.VISIBLE);
                String status = Util
                        .eventAttendanceMapper(context, attendanceFilter.getSelectedItemPosition());
                if (!TextUtils.isEmpty(status)) {
                    if (status.equals(STATUS_ACCEPTED)) {
                        holder.acceptButtonView.setVisibility(View.GONE);
                        holder.rejectButtonView.setVisibility(View.VISIBLE);
                    } else if (status.equals(STATUS_REJECTED)) {
                        holder.acceptButtonView.setVisibility(View.VISIBLE);
                        holder.rejectButtonView.setVisibility(View.GONE);
                    } else if (status.equals((STATUS_INVITED)) &&
                            event.getType() == TYPE_PUBLIC_EVENT) {
                        holder.acceptButtonView.setVisibility(View.VISIBLE);
                        holder.rejectButtonView.setVisibility(View.VISIBLE);
                    }
                } else {
                    compositeDisposable.add(
                            Util.observeUserEventInviteStatus(auth.getUid(), event.getId())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe(inviteStatus -> {
                                if (TextUtils.isEmpty(inviteStatus)) {
                                    holder.acceptButtonView.setVisibility(View.VISIBLE);
                                    holder.rejectButtonView.setVisibility(View.VISIBLE);
                                } else {
                                    switch (inviteStatus) {
                                        case STATUS_ACCEPTED:
                                            holder.acceptButtonView.setVisibility(View.GONE);
                                            holder.rejectButtonView.setVisibility(View.VISIBLE);
                                            break;
                                        case STATUS_REJECTED:
                                            holder.acceptButtonView.setVisibility(View.VISIBLE);
                                            holder.rejectButtonView.setVisibility(View.GONE);
                                            break;
                                        case STATUS_INVITED:
                                            holder.acceptButtonView.setVisibility(View.VISIBLE);
                                            holder.rejectButtonView.setVisibility(View.VISIBLE);
                                            break;
                                    }
                                }
                            })
                    );
                }
            } else {
                holder.buttonsLayout.setVisibility(View.GONE);
            }

            holder.acceptButtonView.setOnClickListener(view -> {
                Util.inviteUserToEvent(event.getId(), auth.getUid(), STATUS_ACCEPTED);
                displayEvents();
            });

            holder.rejectButtonView.setOnClickListener(view -> {
                Util.inviteUserToEvent(event.getId(), auth.getUid(), STATUS_REJECTED);
                displayEvents();
            });

            return convertView;
        }

        @Override
        public void refreshList(List<Event> newEvents) {
            events.clear();
            if (newEvents.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
            } else {
                events.addAll(newEvents);
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {

        TextView titleView;
        TextView scheduledForView;
        TextView endsAtView;
        LinearLayout buttonsLayout;
        Button acceptButtonView;
        Button rejectButtonView;
    }
}
