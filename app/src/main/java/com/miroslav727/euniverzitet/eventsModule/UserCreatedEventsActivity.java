package com.miroslav727.euniverzitet.eventsModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.Event;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_USER_ROLE;
import static com.miroslav727.euniverzitet.util.Constants.EVENTS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_ADMINISTRATOR;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_NON_TEACHING_STAFF;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEACHING_STAFF;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_LESSON;

public class UserCreatedEventsActivity extends AppCompatActivity {

    private ActionMode actionMode;

    private int selectedCounter;

    private int selectedPosition;

    private int role;

    private List<Boolean> checkedForDeletion;

    private Spinner typeFilter;

    private TextView emptyListView;

    private ListView listView;

    private EditableEventArrayAdapter adapter;

    public static final String FLAG_EVENT_ID = "event_id";

    private DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private CompositeDisposable compositeDisposable;

    //region action mode
    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.general_action_menu, menu);
            setActionModeTitle(mode);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menuitem_delete:

                    ArrayList<Event> remainingEvents = new ArrayList<>();

                    for (int i = 0; i < checkedForDeletion.size(); i++) {
                        Event event = adapter.events.get(i);
                        if (checkedForDeletion.get(i)) {
                            removeEvent(event);
                        } else {
                            remainingEvents.add(event);
                        }
                    }
                    checkedForDeletion.clear();
                    adapter.refreshList(remainingEvents);
                    for (int i = 0; i < remainingEvents.size(); i++) {
                        checkedForDeletion.set(i, false);
                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.setInActionMode(false);
            selectedCounter = 0;
            for (int i = 0; i < checkedForDeletion.size(); i++) {
                checkedForDeletion.set(i, false);
            }
            actionMode = null;
        }
    };

    private void setActionModeTitle (@NonNull ActionMode mode) {
        mode.setTitle(getResources()
                .getQuantityString(R.plurals.selected, selectedCounter, selectedCounter));
    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_created_events);

        databaseReference = Util.getRootReference();
        auth = FirebaseAuth.getInstance();
        compositeDisposable = new CompositeDisposable();

        role = getIntent().getIntExtra(FLAG_USER_ROLE, -1);

        checkedForDeletion = new ArrayList<>();

        typeFilter = findViewById(R.id.spinner_userEventsDisplay_type);
        emptyListView = findViewById(R.id.textview_userEventsDisplay_emptyList);
        listView = findViewById(R.id.listview_userEventsDisplay_searchResults);

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item_gravity_end,
                getResources().getStringArray(R.array.allEventsByType));
        typeFilter.setAdapter(typeAdapter);

        adapter = new EditableEventArrayAdapter(this, new ArrayList<>());
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayEventsOfType(typeFilter.getSelectedItemPosition() - 1);

        addListeners();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    private void addListeners() {
        FloatingActionButton fab = findViewById(R.id.fab_userEventsDisplay_newEvent);
        fab.setOnClickListener(view -> {
            Intent startEventCreate = new Intent(this, EventCreateActivity.class);
            startEventCreate.putExtra(FLAG_USER_ROLE, role);
            startActivity(startEventCreate);
        });

        listView.setOnItemClickListener((AdapterView<?> parent, View view,
                                         int position, long id) -> {
            ViewHolderItem holder = (ViewHolderItem) view.getTag();
            if (actionMode != null) {
                if (holder.deleteCheckbox.isChecked()) {
                    holder.deleteCheckbox.setChecked(false);
                    checkedForDeletion.set(position, false);
                } else {
                    holder.deleteCheckbox.setChecked(true);
                    checkedForDeletion.set(position, true);
                }
            } else {
                Util.showEvent(this, role, adapter.events.get(position).getId());
            }
        });

        listView.setOnItemLongClickListener((AdapterView<?> parent, View view,
                                             int position, long id) -> {
            if (actionMode == null) {
                actionMode = ((AppCompatActivity) view.getContext())
                        .startSupportActionMode(actionModeCallback);
                adapter.setInActionMode(true);
                ViewHolderItem holder = (ViewHolderItem) view.getTag();
                checkedForDeletion.set(position, true);
                holder.deleteCheckbox.setChecked(true);
                selectedCounter = 1;
            }
            return true;
        });

        typeFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (selectedPosition != position) {
                    selectedPosition = position;
                    displayEventsOfType(selectedPosition - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //must override, not needed
            }
        });
    }

    private void displayEventsOfType(int type) {
        if (role == ROLE_ADMINISTRATOR) {
            compositeDisposable.add(
                    Util.observeAllEventsOfType(type)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.newThread())
                            .subscribe(this::displayEvents)
            );
        } else if (role == ROLE_TEACHING_STAFF || role == ROLE_NON_TEACHING_STAFF) {
            compositeDisposable.add(
                    Util.observeEventsCreatedByUser(auth.getUid(), type)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.newThread())
                            .subscribe(this::displayEvents)
            );
        }
    }

    private void displayEvents(List<Event> events) {
        Collections.sort(events);
        Util.setViewVisibilityAndUpdateAdapter
                (events, emptyListView, listView, adapter);
    }

    private void removeEvent(Event event) {
        String eventId = event.getId();
        if (event.getType() == TYPE_LESSON) {
            Util.removeEventFromCourse(eventId, event.getCourseId());
        }
        Util.removeEventFromLocation(eventId, event.getLocationId());
        Scheduler thread = Schedulers.newThread();
        compositeDisposable.add(
                Util.observeAllEventInviteesIds(eventId)
                .observeOn(thread)
                .subscribeOn(thread)
                .subscribe(ids -> {
                    for (String uid : ids) {
                        Util.removeEventFromUser(eventId, uid);
                    }
                    Util.removeItemFromDatabaseTable(databaseReference, EVENTS_TABLE, eventId);
                })
        );
    }

    private class EditableEventArrayAdapter extends RefreshAdapter<Event> {

        private Context context;
        private List<Event> events;
        private boolean inActionMode;

        private EditableEventArrayAdapter(Context context, List<Event> events) {
            super(context, events);
            this.context = context;
            this.events = events;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;
            Event event = events.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.event_editable_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.titleView = convertView
                        .findViewById(R.id.textview_editableEventDisplay_title);
                holder.scheduledForView = convertView
                        .findViewById(R.id.textview_editableEventDisplay_scheduledFor);
                holder.endsAtView = convertView
                        .findViewById(R.id.textview_editableEventDisplay_endsAt);
                holder.editButtonView = convertView
                        .findViewById(R.id.button_editableEventDisplay_edit);
                holder.deleteCheckbox = convertView
                        .findViewById(R.id.checkbox_editableEventDisplay_delete);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
                holder.deleteCheckbox.setOnCheckedChangeListener(null);
                holder.deleteCheckbox.setChecked(checkedForDeletion.get(position));
            }

            holder.titleView.setText(event.getTitle());
            holder.scheduledForView.setText(Util.formTimestamp
                    (event.getStartDate(), event.getStartHour(), event.getStartMinute()));
            holder.endsAtView.setText(Util.formTimestamp
                    (event.getEndDate(), event.getEndHour(), event.getEndMinute()));

            holder.editButtonView.setOnClickListener(view ->
                    Util.startEditEvent((UserCreatedEventsActivity)context, role, event.getId()));

            holder.deleteCheckbox.setVisibility(inActionMode ? View.VISIBLE : View.GONE);
            holder.deleteCheckbox.setTag(String.valueOf(position));

            holder.deleteCheckbox.setOnCheckedChangeListener(
                    (CompoundButton buttonView, boolean isChecked) -> {
                        int pos = Integer.parseInt(buttonView.getTag().toString());
                        checkedForDeletion.set(pos, isChecked);
                        if (isChecked) {
                            selectedCounter++;
                            setActionModeTitle(actionMode);
                        } else {
                            selectedCounter--;
                            if (selectedCounter == 0) {
                                actionMode.finish();
                            } else {
                                setActionModeTitle(actionMode);
                            }
                        }
                    });

            return convertView;
        }

        void setInActionMode(boolean inActionMode) {
            this.inActionMode = inActionMode;
            notifyDataSetChanged();
        }

        @Override
        public void refreshList(List<Event> newEvents) {
            events.clear();
            checkedForDeletion.clear();
            if (newEvents.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
            } else {
                events.addAll(newEvents);
                for (int i = 0; i < newEvents.size(); i++) {
                    checkedForDeletion.add(false);
                }
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {

        TextView titleView;
        TextView scheduledForView;
        TextView endsAtView;
        Button editButtonView;
        CheckBox deleteCheckbox;
    }
}
