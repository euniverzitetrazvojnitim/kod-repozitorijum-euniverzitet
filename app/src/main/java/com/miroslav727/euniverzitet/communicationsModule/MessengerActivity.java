package com.miroslav727.euniverzitet.communicationsModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.material.textfield.TextInputEditText;
import com.firebase.ui.database.FirebaseRecyclerAdapter;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.Message;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.util.Util;

import java.util.Calendar;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.util.Constants.CHATS_META_CHILD_LAST_MESSAGE;
import static com.miroslav727.euniverzitet.util.Constants.CHATS_META_CHILD_READ;
import static com.miroslav727.euniverzitet.util.Constants.CHATS_META_CHILD_SENDER_EMAIL;
import static com.miroslav727.euniverzitet.util.Constants.CHATS_META_CHILD_SENDER_ID;
import static com.miroslav727.euniverzitet.util.Constants.CHATS_META_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.MESSAGES_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_RECEIVED;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_SENT;
import static com.miroslav727.euniverzitet.util.Constants.USERS_TABLE;

public class MessengerActivity extends AppCompatActivity {

    public static final String FLAG_OTHER_USER_ID = "other_user_id";

    private TextView title;
    private TextView emptyMessages;
    private RecyclerView messages;
    private LinearLayoutManager linearLayoutManager;
    private TextInputEditText messageSender;
    private Button sendMessage;

    private DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private CompositeDisposable compositeDisposable;
    private FirebaseRecyclerAdapter<Message, RecyclerView.ViewHolder> adapter;

    private String currentUserId;
    private String otherUserId;

    private User currentUser;
    private User otherUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);

        otherUserId = getIntent().getStringExtra(FLAG_OTHER_USER_ID);

        Scheduler thread = Schedulers.newThread();

        databaseReference = Util.getRootReference();
        auth = FirebaseAuth.getInstance();
        compositeDisposable = new CompositeDisposable();

        currentUserId = auth.getUid();

        initViews();

        compositeDisposable.add(
                Util.observeUserFromDatabase(currentUserId)
                        .subscribeOn(thread)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(foundUser -> currentUser = foundUser)
        );

        compositeDisposable.add(
                Util.observeUserFromDatabase(otherUserId)
                        .subscribeOn(thread)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(foundUser -> {
                            otherUser = foundUser;
                            String otherUserDisplayName = otherUser.getEmail()
                                    .substring(0, otherUser.getEmail().indexOf("@"));
                            String titleText = getString(R.string.chat_with)
                                    .concat(otherUserDisplayName);
                            title.setText(titleText);
                        })
        );

    }

    @Override
    protected void onResume() {
        super.onResume();

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        messages.setLayoutManager(linearLayoutManager);

        SnapshotParser<Message> parser = snapshot -> {
            Message message = snapshot.getValue(Message.class);
            if (message != null) {
                message.setId(snapshot.getKey());
            }
            return message;
        };

        DatabaseReference watcherReference = databaseReference.child(USERS_TABLE)
                .child(currentUserId).child(CHATS_META_TABLE)
                .child(otherUserId).child(MESSAGES_TABLE);

        FirebaseRecyclerOptions<Message> options = new FirebaseRecyclerOptions.Builder<Message>()
                .setQuery(watcherReference, parser)
                .build();

        adapter = createAdapter(options);

        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int messageCount = adapter.getItemCount();
                int lastVisiblePosition =
                        linearLayoutManager.findLastCompletelyVisibleItemPosition();

                if (lastVisiblePosition == -1 ||
                        (positionStart >= (messageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    messages.scrollToPosition(positionStart);
                }
            }
        });

        messages.setAdapter(adapter);

        addListeners();

        adapter.startListening();
        if (adapter.getItemCount() > 0) {
            emptyMessages.setVisibility(View.GONE);
            messages.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        adapter.stopListening();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    private void initViews() {
        title = findViewById(R.id.textview_messenger_title);
        emptyMessages = findViewById(R.id.textview_messenger_noMessages);
        messages = findViewById(R.id.recyclerview_messenger_messages);
        messageSender = findViewById(R.id.tiledittext_messenger_sendMessage);
        sendMessage = findViewById(R.id.button_messenger_send);
    }

    private void addListeners() {
        messageSender.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //not needed, must override
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    sendMessage.setEnabled(true);
                } else {
                    sendMessage.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //not needed, must override
            }
        });

        sendMessage.setOnClickListener(view -> {

            Calendar calendar = Calendar.getInstance();
            long time = calendar.getTimeInMillis();
            String id = ""+time;

            DatabaseReference receiverPreReference = databaseReference.child(USERS_TABLE)
                    .child(otherUserId).child(CHATS_META_TABLE).child(currentUserId);
            DatabaseReference senderReference = databaseReference.child(USERS_TABLE)
                    .child(currentUserId).child(CHATS_META_TABLE).child(otherUserId)
                    .child(MESSAGES_TABLE).child(id);

            String messageText = messageSender.getText().toString();

            Message newMessage = new Message(currentUser.getUid(), currentUser.getEmail(),
                    otherUserId, otherUser.getEmail(), time, messageText, id);
            senderReference.setValue(newMessage);
            receiverPreReference.child(MESSAGES_TABLE).child(id).setValue(newMessage);
            receiverPreReference.child(CHATS_META_CHILD_READ).setValue(false);
            receiverPreReference.child(CHATS_META_CHILD_LAST_MESSAGE).setValue(messageText);
            receiverPreReference.child(CHATS_META_CHILD_SENDER_EMAIL).setValue(currentUser.getEmail());
            receiverPreReference.child(CHATS_META_CHILD_SENDER_ID).setValue(currentUserId);
            messageSender.setText("");
            hideEmptyAndShowRecycler();
        });
    }

    private FirebaseRecyclerAdapter<Message, RecyclerView.ViewHolder>
            createAdapter(FirebaseRecyclerOptions<Message> options) {
        return new FirebaseRecyclerAdapter<Message, RecyclerView.ViewHolder>(options) {
            @Override
            protected void onBindViewHolder
                    (@NonNull RecyclerView.ViewHolder viewHolder,
                     int position, @NonNull Message message) {
                switch (viewHolder.getItemViewType()) {
                    case TYPE_SENT:
                        ((SentMessageHolder) viewHolder).bind(message);
                        break;
                    case TYPE_RECEIVED:
                        ((ReceivedMessageHolder) viewHolder).bind(message);
                        break;
                }

            }

            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder
                    (@NonNull ViewGroup parent, int viewType) {
                View view;

                if (viewType == TYPE_SENT) {
                    view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.message_sent_list_item, parent, false);
                    return new SentMessageHolder(view);
                } else {
                    view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.message_received_list_item, parent, false);
                    return new ReceivedMessageHolder(view);
                }
            }

            @Override
            public int getItemViewType(int position) {
                Message message = this.getItem(position);
                return message.getSenderId().equals(currentUserId) ? TYPE_SENT : TYPE_RECEIVED;
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (getItemCount() > 0) {
                    hideEmptyAndShowRecycler();
                }
                databaseReference.child(USERS_TABLE).child(currentUserId).child(CHATS_META_TABLE)
                        .child(otherUserId).child(CHATS_META_CHILD_READ).setValue(true);
            }
        };
    }

    private void hideEmptyAndShowRecycler() {
        if (emptyMessages.getVisibility() == View.VISIBLE) {
            emptyMessages.setVisibility(View.GONE);
            messages.setVisibility(View.VISIBLE);
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {

        TextView usernameView;
        TextView timestampView;
        TextView messageView;

        public SentMessageHolder(@NonNull View itemView) {
            super(itemView);
            usernameView = itemView.findViewById(R.id.textview_sentMessage_username);
            timestampView = itemView.findViewById(R.id.textview_sentMessage_timestamp);
            messageView = itemView.findViewById(R.id.textview_sentMessage_message);
        }

        void bind(Message message) {
            messageView.setText(message.getText());
            String usernameText = message.getSenderEmail()
                    .substring(0, message.getSenderEmail().indexOf("@"));
            usernameView.setText(usernameText);
            timestampView.setText(Util.formShortTimestamp(message.getSentAt(), 0, 0));
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {

        TextView usernameView;
        TextView timestampView;
        TextView messageView;

        public ReceivedMessageHolder(@NonNull View itemView) {
            super(itemView);
            usernameView = itemView.findViewById(R.id.textview_receivedMessage_username);
            timestampView = itemView.findViewById(R.id.textview_receivedMessage_timestamp);
            messageView = itemView.findViewById(R.id.textview_receivedMessage_message);
        }

        void bind(Message message) {
            messageView.setText(message.getText());
            String usernameText = message.getSenderEmail()
                    .substring(0, message.getSenderEmail().indexOf("@"));
            usernameView.setText(usernameText);
            timestampView.setText(Util.formShortTimestamp(message.getSentAt(), 0, 0));
        }
    }


}
