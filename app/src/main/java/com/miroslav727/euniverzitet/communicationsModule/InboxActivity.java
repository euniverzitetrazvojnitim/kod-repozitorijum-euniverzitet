package com.miroslav727.euniverzitet.communicationsModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.ChatMeta;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.communicationsModule.MessengerActivity.FLAG_OTHER_USER_ID;
import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_CONTACTS;
import static com.miroslav727.euniverzitet.util.Constants.USERS_TABLE;

public class InboxActivity extends AppCompatActivity {

    private ActionMode actionMode;
    private int selectedCounter;

    private DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private CompositeDisposable compositeDisposable;

    private TextView title;
    private Button switchView;

    private RelativeLayout contacts;
    private TextView emptyContacts;
    private ListView listContacts;
    private ContactsArrayAdapter contactsAdapter;
    private List<Boolean> checkedForDeletion;
    private FloatingActionButton newContact;

    private RelativeLayout newMessages;
    private TextView emptyNewMessages;
    private ListView listNewMessages;
    private NewMessagesArrayAdapter messagesAdapter;

    //region action mode
    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.general_action_menu, menu);
            setActionModeTitle(mode);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menuitem_delete:

                    ArrayList<User> remainingContacts = new ArrayList<>();

                    for (int i = 0; i < checkedForDeletion.size(); i++) {
                        User contact = contactsAdapter.userContacts.get(i);
                        if (checkedForDeletion.get(i)) {
                            removeContact(contact.getUid());
                        } else {
                            remainingContacts.add(contact);
                        }
                    }
                    checkedForDeletion.clear();
                    contactsAdapter.refreshList(remainingContacts);
                    for (int i = 0; i < remainingContacts.size(); i++) {
                        checkedForDeletion.set(i, false);
                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            contactsAdapter.setInActionMode(false);
            selectedCounter = 0;
            for (int i = 0; i < checkedForDeletion.size(); i++) {
                checkedForDeletion.set(i, false);
            }
            actionMode = null;
        }
    };

    private void setActionModeTitle (@NonNull ActionMode mode) {
        mode.setTitle(getResources()
                .getQuantityString(R.plurals.selected, selectedCounter, selectedCounter));
    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);

        databaseReference = Util.getRootReference();
        auth = FirebaseAuth.getInstance();
        compositeDisposable = new CompositeDisposable();

        initViews();

        contactsAdapter = new ContactsArrayAdapter(this, new ArrayList<>());
        listContacts.setAdapter(contactsAdapter);

        messagesAdapter = new NewMessagesArrayAdapter(this, new ArrayList<>());
        listNewMessages.setAdapter(messagesAdapter);

        checkedForDeletion = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setupContacts();
        displayContacts();

        addListeners();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    private void initViews() {
        title = findViewById(R.id.textview_inbox_title);
        switchView = findViewById(R.id.button_inbox_switch);

        contacts = findViewById(R.id.layout_inbox_contacts);
        emptyContacts = findViewById(R.id.textview_inbox_contactsEmptyList);
        listContacts = findViewById(R.id.listview_inbox_contactsList);
        newContact = findViewById(R.id.fab_inbox_newContact);

        newMessages = findViewById(R.id.layout_inbox_newMessages);
        emptyNewMessages = findViewById(R.id.textview_inbox_newMessagesEmptyList);
        listNewMessages = findViewById(R.id.listview_inbox_newMessagesList);
    }

    private void addListeners() {
        switchView.setOnClickListener(view -> {
            if (title.getText().toString().equals(getString(R.string.contacts))) {
                displayNewMessages();
            } else {
                displayContacts();
            }
        });

        newContact.setOnClickListener(view -> {
            startActivity(new Intent(this, SearchContactsActivity.class));
        });

        listContacts.setOnItemClickListener((AdapterView<?> parent, View view,
                                         int position, long id) -> {
            ContactViewHolderItem holder = (ContactViewHolderItem) view.getTag();
            if (actionMode != null) {
                if (holder.deleteCheckbox.isChecked()) {
                    holder.deleteCheckbox.setChecked(false);
                    checkedForDeletion.set(position, false);
                } else {
                    holder.deleteCheckbox.setChecked(true);
                    checkedForDeletion.set(position, true);
                }
            } else {
                User contact = contactsAdapter.userContacts.get(position);
                showMessenger(contact.getUid());
            }
        });

        listContacts.setOnItemLongClickListener((AdapterView<?> parent, View view,
                                             int position, long id) -> {
            if (actionMode == null) {
                actionMode = ((AppCompatActivity) view.getContext())
                        .startSupportActionMode(actionModeCallback);
                contactsAdapter.setInActionMode(true);
                ContactViewHolderItem holder = (ContactViewHolderItem) view.getTag();
                checkedForDeletion.set(position, true);
                holder.deleteCheckbox.setChecked(true);
                selectedCounter = 1;
            }
            return true;
        });

        listNewMessages.setOnItemClickListener((AdapterView<?> parent, View view,
                                                int position, long id) -> {
            ChatMeta chatMeta = messagesAdapter.unreadChats.get(position);
            String senderId = chatMeta.getSenderId();
            Scheduler thread = Schedulers.newThread();
            compositeDisposable.add(
                    Util.observeUserFromDatabase(senderId)
                    .subscribeOn(thread)
                    .observeOn(thread)
                    .subscribe(sender -> {
                        if (!contactsAdapter.userContacts.contains(sender)) {
                            Util.addContact(auth.getUid(), senderId);
                        }
                    })
            );
            showMessenger(senderId);

        });
    }

    private void displayNewMessages() {
        switchView.setEnabled(false);
        compositeDisposable.add(
                Util.observeUnreadChats(auth.getUid())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(unreadChats -> {
                    Util.setViewVisibilityAndUpdateAdapter(unreadChats, emptyNewMessages,
                            listNewMessages, messagesAdapter);
                    title.setText(getString(R.string.new_messages));
                    switchView.setText(getString(R.string.contacts));
                    switchView.setEnabled(true);
                    contacts.setVisibility(View.GONE);
                    newMessages.setVisibility(View.VISIBLE);
                })
        );

    }

    private void displayContacts() {
        title.setText(getString(R.string.contacts));
        switchView.setText(getString(R.string.new_messages));
        contacts.setVisibility(View.VISIBLE);
        newMessages.setVisibility(View.GONE);
    }

    private void setupContacts() {
        compositeDisposable.add(
                Util.observeUserContacts(auth.getUid())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(foundContacts -> Util.setViewVisibilityAndUpdateAdapter
                        (foundContacts, emptyContacts, listContacts, contactsAdapter))
        );
    }

    private void removeContact(String contactId) {
        databaseReference.child(USERS_TABLE).child(auth.getUid()).child(USERS_CHILD_CONTACTS)
                .child(contactId).removeValue();
    }

    private void showMessenger(String contactId) {
        Intent messengerIntent = new Intent(this, MessengerActivity.class);
        messengerIntent.putExtra(FLAG_OTHER_USER_ID, contactId);
        startActivity(messengerIntent);
    }

    private class ContactsArrayAdapter extends RefreshAdapter<User> {

        private Context context;
        private List<User> userContacts;
        private boolean inActionMode;

        private ContactsArrayAdapter(Context context, List<User> userContacts) {
            super(context, userContacts);
            this.context = context;
            this.userContacts = userContacts;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ContactViewHolderItem holder;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.contact_list_item, parent, false);

                holder = new ContactViewHolderItem();
                holder.contactView = convertView.findViewById(R.id.textview_contactDisplay_contact);
                holder.roleView = convertView.findViewById(R.id.textview_contactDisplay_role);
                holder.deleteCheckbox = convertView.findViewById(R.id.checkbox_contactDisplay_delete);

                convertView.setTag(holder);
            } else {
                holder = (ContactViewHolderItem) convertView.getTag();
                holder.deleteCheckbox.setOnCheckedChangeListener(null);
                holder.deleteCheckbox.setChecked(checkedForDeletion.get(position));
            }

            holder.contactView.setText(userContacts.get(position).getEmail());
            holder.roleView.setText(Util.roleMapper(context, userContacts.get(position).getRole()));

            holder.deleteCheckbox.setVisibility(inActionMode ? View.VISIBLE : View.GONE);
            holder.deleteCheckbox.setTag(String.valueOf(position));

            holder.deleteCheckbox.setOnCheckedChangeListener(
                    (CompoundButton buttonView, boolean isChecked) -> {
                        int pos = Integer.parseInt(buttonView.getTag().toString());
                        checkedForDeletion.set(pos, isChecked);
                        if (isChecked) {
                            selectedCounter++;
                            setActionModeTitle(actionMode);
                        } else {
                            selectedCounter--;
                            if (selectedCounter == 0) {
                                actionMode.finish();
                            } else {
                                setActionModeTitle(actionMode);
                            }
                        }
                    });

            return convertView;
        }

        void setInActionMode(boolean inActionMode) {
            this.inActionMode = inActionMode;
            notifyDataSetChanged();
        }

        @Override
        public void refreshList(List<User> users) {
            userContacts.clear();
            checkedForDeletion.clear();
            if (users.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyContacts, listContacts);
            } else {
                userContacts.addAll(users);
                for (int i = 0; i < users.size(); i++) {
                    checkedForDeletion.add(false);
                }
                notifyDataSetChanged();
            }
        }
    }

    private static class ContactViewHolderItem {

        TextView contactView;
        TextView roleView;
        CheckBox deleteCheckbox;
    }

    private class NewMessagesArrayAdapter extends RefreshAdapter<ChatMeta> {

        private Context context;
        private List<ChatMeta> unreadChats;

        private NewMessagesArrayAdapter(Context context, List<ChatMeta> unreadChats) {
            super(context, unreadChats);
            this.context = context;
            this.unreadChats = unreadChats;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            NewMessagesViewHolderItem holder;
            ChatMeta unreadChat = unreadChats.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.new_message_list_item, parent, false);

                holder = new NewMessagesViewHolderItem();
                holder.fromView = convertView.findViewById(R.id.textview_newMessageDisplay_from);
                holder.messageView = convertView.findViewById(R.id.textview_newMessageDisplay_message);

                convertView.setTag(holder);
            } else {
                holder = (NewMessagesViewHolderItem) convertView.getTag();
            }

            holder.fromView.setText(unreadChat.getSenderEmail());
            holder.messageView.setText(unreadChat.getLastMessage());

            return convertView;
        }

        @Override
        public void refreshList(List<ChatMeta> unreadChats) {
            this.unreadChats.clear();
            if (unreadChats.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyNewMessages, listNewMessages);
            } else {
                this.unreadChats.addAll(unreadChats);
                notifyDataSetChanged();
            }
        }
    }

    private static class NewMessagesViewHolderItem {

        TextView fromView;
        TextView messageView;
    }
}
