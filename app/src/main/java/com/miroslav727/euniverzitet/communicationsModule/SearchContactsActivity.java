package com.miroslav727.euniverzitet.communicationsModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.User;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.util.Constants.USERS_CHILD_EMAIL;
import static com.miroslav727.euniverzitet.util.Constants.USERS_TABLE;

public class SearchContactsActivity extends AppCompatActivity {

    private TextInputEditText searchContacts;
    private Spinner roleFilter;
    private int selectedUserRole;
    private TextView emptyListView;
    private ListView listView;
    private SearchContactsArrayAdapter searchAdapter;
    private List<String> contactIds;

    private FirebaseAuth auth;
    private DatabaseReference databaseReference;
    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contacts);

        databaseReference = Util.getRootReference();
        auth = FirebaseAuth.getInstance();
        compositeDisposable = new CompositeDisposable();

        contactIds = new ArrayList<>();

        Scheduler thread = Schedulers.newThread();
        compositeDisposable.add(
                Util.observeUserContactIds(auth.getUid())
                .observeOn(thread)
                .subscribeOn(thread)
                .subscribe(ids -> contactIds.addAll(ids))
        );


        initViews();

        addListeners();

        final String[] roles = getResources().getStringArray(R.array.allActiveRoles);
        ArrayAdapter<String> roleSpinnerAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item,
                roles);
        roleFilter.setAdapter(roleSpinnerAdapter);

        searchAdapter = new SearchContactsArrayAdapter(this, new ArrayList<>());
        listView.setAdapter(searchAdapter);
    }

    private void initViews() {
        searchContacts = findViewById(R.id.tiledittext_searchContacts_userSearch);
        roleFilter = findViewById(R.id.spinner_searchContacts_role);
        emptyListView = findViewById(R.id.textview_searchContacts_emptyList);
        listView = findViewById(R.id.listview_searchContacts_searchResults);
    }

    private void addListeners() {
        searchContacts.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchText = searchContacts.getText().toString().trim();
                safeSearchUserDatabaseAndListResults(searchText);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        searchContacts.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                handled = true;
                String searchText = searchContacts.getText().toString().trim();
                if (TextUtils.isEmpty(searchText)) {
                    Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
                } else {
                    searchUserDatabaseForExactResult(searchText);
                }
                Util.closeKeyboard(this);
            }
            return handled;
        });

        roleFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (selectedUserRole != position) {
                    selectedUserRole = position;
                    String searchText = searchContacts.getText().toString().trim();
                    safeSearchUserDatabaseAndListResults(searchText);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //must override, not needed
            }
        });

        listView.setOnItemClickListener((adapterView, view, position, l) -> {
            User selectedUser = searchAdapter.users.get(position);
            addContact(selectedUser.getUid());
            finish();
        });
    }

    //region Firebase interactions
    private void safeSearchUserDatabaseAndListResults(String searchText) {
        if (TextUtils.isEmpty(searchText) && roleFilter.getSelectedItemPosition() == 0) {
            Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
        } else {
            searchUserDatabaseAndListResults(searchText);
        }
    }

    private void searchUserDatabaseAndListResults(String searchText) {
        databaseReference.child(USERS_TABLE).orderByChild(USERS_CHILD_EMAIL)
                .startAt(searchText)
                .endAt(searchText+"\uf8ff")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundUsers(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(SearchContactsActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void searchUserDatabaseForExactResult(String searchText) {
        databaseReference.child(USERS_TABLE).orderByChild(USERS_CHILD_EMAIL)
                .equalTo(searchText)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        displayFoundUsers(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(SearchContactsActivity.class.getSimpleName(),
                                databaseError.getDetails());
                    }
                });
    }

    private void displayFoundUsers(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<User> users = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            User user = snapshot.getValue(User.class);
            if (user == null) {
                Log.e("display users", "failed to create user from snapshot");
                return;
            }
            int searchRole = roleFilter.getSelectedItemPosition() - 1;
            if ((searchRole == -1 || searchRole == user.getRole())
                    && !auth.getUid().equals(snapshot.getKey())
                    && !contactIds.contains(snapshot.getKey())) {
                user.setUid(snapshot.getKey());
                users.add(user);
            }
        }
        Util.setViewVisibilityAndUpdateAdapter
                (users, emptyListView, listView, searchAdapter);
    }

    private void addContact(String contactId) {
        Util.addContact(auth.getUid(), contactId);
    }
    //endregion

    private class SearchContactsArrayAdapter extends RefreshAdapter<User> {

        private Context context;
        private List<User> users;

        private SearchContactsArrayAdapter(Context context, List<User> users) {
            super(context, users);
            this.context = context;
            this.users = users;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;
            User user = users.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.contact_search_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.emailView = convertView.findViewById(R.id.textview_contactSearch_email);
                holder.roleView = convertView.findViewById(R.id.textview_contactSearch_role);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
            }

            holder.emailView.setText(user.getEmail());
            holder.roleView.setText(Util.roleMapper(context, user.getRole()));

            return convertView;
        }

        @Override
        public void refreshList(List<User> users) {
            this.users.clear();
            if (users.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
            } else {
                this.users.addAll(users);
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {

        TextView emailView;
        TextView roleView;
    }
}
