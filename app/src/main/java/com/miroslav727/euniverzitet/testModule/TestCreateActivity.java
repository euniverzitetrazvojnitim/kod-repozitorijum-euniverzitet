package com.miroslav727.euniverzitet.testModule;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.Course;
import com.miroslav727.euniverzitet.model.Test;
import com.miroslav727.euniverzitet.util.ExactSearchHandler;
import com.miroslav727.euniverzitet.util.SearchMetadata;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.testModule.QuestionCreateActivity.FLAG_COURSE_ID;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_TEST_ID;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_USER_ROLE;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_TESTS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_ADMINISTRATOR;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEACHING_STAFF;

import static com.miroslav727.euniverzitet.testModule.SimpleCourseArrayAdapter.CourseViewHolderItem;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_CHILD_ACTIVE;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_CHILD_DURATION;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_CHILD_NUMBER;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_CHILD_QUESTION_NUMBER;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_CHILD_TYPE;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_CHILD_VALUE;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_TABLE;

public class TestCreateActivity extends AppCompatActivity {

    private RelativeLayout adminSearchLayout;
    private RelativeLayout teacherSearchLayout;
    private ScrollView detailsLayout;

    private TextInputEditText courseSearch;
    private TextView emptyView;
    private ListView courseSearchResults;
    private SimpleCourseArrayAdapter adapter;

    private Spinner courseSpinner;
    private Button confirmCourse;

    private Spinner typeSpinner;

    private TextView selectedCourse;
    private TextInputLayout testNumberWrapper;
    private TextInputLayout testDurationWrapper;
    private TextInputLayout testQuestionNumberWrapper;
    private TextInputLayout testValueWrapper;
    private TextInputEditText testNumber;
    private TextInputEditText testDuration;
    private TextInputEditText testQuestionNumber;
    private TextInputEditText testValue;
    private CheckBox testActive;
    private Button finish;

    private boolean editTest;
    private int role;

    private Test draftTest;

    private FirebaseAuth auth;

    private String lastSearch;
    private boolean exactSearch;

    private List<Course> teachersCourses;

    private DatabaseReference databaseReference;

    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_create);

        auth = FirebaseAuth.getInstance();

        compositeDisposable = new CompositeDisposable();

        adminSearchLayout = findViewById(R.id.layout_testCreate_searchCourse);
        courseSearch = findViewById(R.id.tiledittext_testCreate_courseSearch);
        emptyView = findViewById(R.id.textview_testCreate_courseSearchEmptyList);
        courseSearchResults = findViewById(R.id.listview_testCreate_courseSearchResults);

        teacherSearchLayout = findViewById(R.id.layout_testCreate_pickCourse);
        courseSpinner = findViewById(R.id.spinner_testCreate_course);
        confirmCourse = findViewById(R.id.button_testCreate_confirmCourse);

        detailsLayout = findViewById(R.id.layout_testCreate_details);
        selectedCourse = findViewById(R.id.textview_testCreate_selectedCourse);
        typeSpinner = findViewById(R.id.spinner_testCreate_type);
        testNumberWrapper = findViewById(R.id.til_testCreate_number);
        testDurationWrapper = findViewById(R.id.til_testCreate_duration);
        testQuestionNumberWrapper = findViewById(R.id.til_testCreate_questionNumber);
        testValueWrapper = findViewById(R.id.til_testCreate_points);
        testNumber = findViewById(R.id.tiledittext_testCreate_number);
        testDuration = findViewById(R.id.tiledittext_testCreate_duration);
        testQuestionNumber = findViewById(R.id.tiledittext_testCreate_questionNumber);
        testValue = findViewById(R.id.tiledittext_testCreate_points);
        testActive = findViewById(R.id.checkbox_testCreate_active);
        finish = findViewById(R.id.button_testCreate_finish);

        String testId = getIntent().getStringExtra(FLAG_TEST_ID);
        editTest = !TextUtils.isEmpty(testId);

        String courseId = getIntent().getStringExtra(FLAG_COURSE_ID);

        role = getIntent().getIntExtra(FLAG_USER_ROLE, -1);

        TextView title = findViewById(R.id.textview_testCreate_title);
        draftTest = new Test();
        if (TextUtils.isEmpty(courseId) && !editTest) {
            title.setText(getString(R.string.create_new_test));
            switch (role) {
                case ROLE_ADMINISTRATOR:
                    adminSearchLayout.setVisibility(View.VISIBLE);
                    break;
                case ROLE_TEACHING_STAFF:
                    teacherSearchLayout.setVisibility(View.VISIBLE);
                    teachersCourses = new ArrayList<>();
                    compositeDisposable.add(Util.observeTeachersCourses(auth.getUid())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(courses -> {
                                teachersCourses = courses;
                                String[] coursesArray = new String[courses.size()];
                                for (int i = 0; i < courses.size(); i++) {
                                    coursesArray[i] = courses.get(i).formCourseIdentifier();
                                }
                                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                                        R.layout.custom_spinner_item,
                                        coursesArray);
                                courseSpinner.setAdapter(spinnerAdapter);
                            }));
                    break;
                default:
                    Log.d(TestCreateActivity.class.getSimpleName(), "Invalid user role");
                    finish();
                    break;
            }
        } else if (editTest) {
            compositeDisposable.add(
                Util.observeTestById(testId)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(test -> {
                        draftTest = test;

                        compositeDisposable.add(Util.getCourseCode(draftTest.getCourse())
                                .subscribe(code ->
                                        title.setText(getString(R.string.edit_existing_test,
                                                test.formName(code, this)))));
                        compositeDisposable.add(Util.getCourseIdentifier(draftTest.getCourse())
                                .subscribe(courseIdentifier -> {
                                    selectedCourse.setText(courseIdentifier);
                                    typeSpinner.setSelection(draftTest.getType());
                                    testNumber.setText(""+draftTest.getNumber());
                                    testDuration.setText(""+draftTest.getDuration());
                                    testQuestionNumber.setText(""+draftTest.getQuestionNumber());
                                    testValue.setText(""+draftTest.getPointsValue());

                                    detailsLayout.setVisibility(View.VISIBLE);
                                })
                        );
                        compositeDisposable.add(
                                Util.getTestNumberOfQuestions(draftTest.getId())
                                .subscribe(number -> {
                                    if (number >= draftTest.getQuestionNumber()) {
                                        testActive.setChecked(draftTest.isActive());
                                        testActive.setVisibility(View.VISIBLE);
                                    } else {
                                        testActive.setChecked(false);
                                        testActive.setVisibility(View.GONE);
                                    }
                                })
                        );
                    })
            );
        } else {
            title.setText(getString(R.string.create_new_test));
            compositeDisposable.add(
                    Util.getCourseIdentifier(courseId)
                    .subscribe(courseIdentifier -> {
                        detailsLayout.setVisibility(View.VISIBLE);
                        draftTest.setCourse(courseId);
                        selectedCourse.setText(courseIdentifier);
                    })
            );
        }

        final String[] testTypeList = getResources().getStringArray(R.array.testTypes);
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item,
                testTypeList);

        typeSpinner.setAdapter(typeAdapter);

        adapter = new SimpleCourseArrayAdapter
                (this, new ArrayList<>(), emptyView, courseSearchResults);
        courseSearchResults.setAdapter(adapter);

        databaseReference = Util.getRootReference();
        lastSearch = "";

        addListeners();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    private void addListeners() {

        courseSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SearchMetadata searchMetadata = Util.onSearchTextChanged(courseSearch, null,
                        lastSearch, exactSearch, databaseReference,
                        emptyView, courseSearchResults, adapter);
                lastSearch = searchMetadata.getLastSearch();
                exactSearch = searchMetadata.isExactSearch();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        courseSearch.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            ExactSearchHandler handler = new ExactSearchHandler(lastSearch, exactSearch);
            handler.performSearch(actionId, courseSearch, databaseReference, emptyView,
                    courseSearchResults, adapter, this);
            lastSearch = handler.getLastSearch();
            exactSearch = handler.isExactSearch();
            return handler.isHandled();
        });

        courseSearchResults.setOnItemClickListener((adapterView, view, position, l) -> {
            adminSearchLayout.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);
            CourseViewHolderItem holder = (CourseViewHolderItem) view.getTag();
            String course = holder.resultView.getText().toString();
            draftTest.setCourse(adapter.getCourses().get(position).getId());
            selectedCourse.setText(course);
        });

        confirmCourse.setOnClickListener(view -> {
            teacherSearchLayout.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);
            String course = courseSpinner.getSelectedItem().toString();
            draftTest.setCourse(teachersCourses.get(courseSpinner.getSelectedItemPosition()).getId());
            selectedCourse.setText(course);
        });

        finish.setOnClickListener(view -> {
            ArrayList<TextInputLayout> wrappers = new ArrayList<>();
            wrappers.add(testNumberWrapper);
            wrappers.add(testDurationWrapper);
            wrappers.add(testQuestionNumberWrapper);
            wrappers.add(testValueWrapper);
            if (!setErrorOnWrappers(wrappers)) {
                draftTest.setType(typeSpinner.getSelectedItemPosition());
                draftTest.setNumber(Integer.parseInt(testNumber.getText().toString()));
                draftTest.setDuration(Integer.parseInt(testDuration.getText().toString()));
                draftTest.setQuestionNumber(Integer.parseInt(testQuestionNumber.getText().toString()));
                draftTest.setPointsValue(Double.parseDouble(testValue.getText().toString()));
                draftTest.setActive(testActive.isChecked());
                if (editTest) {
                    editTestInDatabase();
                } else {
                    addTestToDatabase();
                }
                finish();
            }
        });
    }

    private boolean setErrorOnWrappers(List<TextInputLayout> wrappers) {
        boolean hasError = false;
        for (TextInputLayout wrapper : wrappers) {
            if (TextUtils.isEmpty(wrapper.getEditText().getText().toString())) {
                wrapper.setError(getString(R.string.mandatory_field));
                hasError = true;
            } else {
                wrapper.setError(null);
                wrapper.setErrorEnabled(false);
            }
        }
        return hasError;
    }

    private void addTestToDatabase() {
        DatabaseReference dr = databaseReference.child(TESTS_TABLE).push();
        String testId = dr.getKey();
        dr.setValue(draftTest).addOnSuccessListener(task -> {
            addTestToCourse(testId);
        });
    }

    private void editTestInDatabase() {
        databaseReference.child(TESTS_TABLE).child(draftTest.getId()).child(TESTS_CHILD_ACTIVE)
                .setValue(draftTest.isActive());
        databaseReference.child(TESTS_TABLE).child(draftTest.getId()).child(TESTS_CHILD_DURATION)
                .setValue(draftTest.getDuration());
        databaseReference.child(TESTS_TABLE).child(draftTest.getId()).child(TESTS_CHILD_NUMBER)
                .setValue(draftTest.getNumber());
        databaseReference.child(TESTS_TABLE).child(draftTest.getId()).child(TESTS_CHILD_QUESTION_NUMBER)
                .setValue(draftTest.getQuestionNumber());
        databaseReference.child(TESTS_TABLE).child(draftTest.getId()).child(TESTS_CHILD_TYPE)
                .setValue(draftTest.getType());
        databaseReference.child(TESTS_TABLE).child(draftTest.getId()).child(TESTS_CHILD_VALUE)
                .setValue(draftTest.getPointsValue());
    }

    private void addTestToCourse(String testId) {
        databaseReference.child(COURSES_TABLE)
                .child(draftTest.getCourse())
                .child(COURSES_CHILD_TESTS)
                .child(testId)
                .setValue(true);
    }
}
