package com.miroslav727.euniverzitet.testModule;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.miroslav727.euniverzitet.R;

import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_TEST_ID;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.KEY_TEST_DURATION;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.KEY_TEST_TITLE;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.KEY_TEST_VALUE;

public class StartTestDialog extends DialogFragment {


    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        TestsDisplayActivity activity = (TestsDisplayActivity)getActivity();
        Bundle arguments = getArguments();

        String testId = arguments.getString(FLAG_TEST_ID, "id1");
        String title = arguments.getString(KEY_TEST_TITLE, "TST111 W11");
        String duration = arguments.getString(KEY_TEST_DURATION, "111");
        String value = arguments.getString(KEY_TEST_VALUE, "111.5");

        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogLayout = inflater.inflate(R.layout.start_test_dialog, null);

        TextView durationView = dialogLayout.findViewById(R.id.textview_startTest_duration);
        TextView valueView = dialogLayout.findViewById(R.id.textview_startTest_points);

        durationView.setText(duration);
        valueView.setText(value);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity,
                R.style.generalDialogTheme);

        builder.setTitle(title)
                .setView(dialogLayout)
                .setPositiveButton(R.string.start_test, (DialogInterface dialog, int id) -> {
                    Intent startTest = new Intent(activity, TestTakingActivity.class);
                    startTest.putExtra(FLAG_TEST_ID, testId);
                    startTest.putExtra(KEY_TEST_TITLE, title);
                    startActivity(startTest);
                })
                .setNegativeButton(R.string.cancel_dialog,  (DialogInterface dialog, int id) -> {
                    Toast.makeText(activity, "test canceled", Toast.LENGTH_SHORT).show();
                });
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView title = this.getDialog()
                .findViewById(androidx.appcompat.R.id.alertTitle);
        if (title != null) {
            title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
    }
}
