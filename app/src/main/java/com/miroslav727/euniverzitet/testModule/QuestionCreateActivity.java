package com.miroslav727.euniverzitet.testModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.Answer;
import com.miroslav727.euniverzitet.model.Course;
import com.miroslav727.euniverzitet.model.Question;
import com.miroslav727.euniverzitet.model.Test;
import com.miroslav727.euniverzitet.util.ExactSearchHandler;
import com.miroslav727.euniverzitet.util.SearchMetadata;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_USER_ROLE;
import static com.miroslav727.euniverzitet.util.Constants.ANSWERS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.QUESTIONS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.QUESTION_CHILD_CORRECT_ANSWERS;
import static com.miroslav727.euniverzitet.util.Constants.QUESTION_CHILD_INCORRECT_ANSWERS;
import static com.miroslav727.euniverzitet.util.Constants.QUESTION_CHILD_TESTS;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_ADMINISTRATOR;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEACHING_STAFF;

import static com.miroslav727.euniverzitet.testModule.SimpleCourseArrayAdapter.CourseViewHolderItem;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_CHILD_QUESTIONS;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_FREE_ANSWER;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_MULTIPLE_CORRECT;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_SINGLE_CORRECT;

public class QuestionCreateActivity extends AppCompatActivity {

    public static final String FLAG_QUESTION_ID = "question_id";
    public static final String FLAG_COURSE_ID = "course_id";

    private static final String STATE_COURSE = "state_course";
    private static final String STATE_TEST = "state_test";
    private static final String STATE_DETAILS = "state_details";
    private static final String STATE_METADATA = "state_metadata";
    private static final String STATE_ANSWERS = "state_answers";

    private String currentState;

    private TextView title;

    private RelativeLayout adminCourseSearchLayout;
    private RelativeLayout teacherCourseSearchLayout;
    private LinearLayout buttonLayout;
    private ScrollView detailsLayout;
    private ScrollView answersLayout;
    private LinearLayout metadataLayout;

    private TextInputEditText courseSearch;
    private TextView emptyCourseView;
    private ListView courseSearchResults;
    private SimpleCourseArrayAdapter courseAdapter;

    private Spinner courseSpinner;
    private Button confirmCourse;

    private Button previousButton;
    private Button nextButton;

    private TextView emptyTestView;
    private ListView testSearchResults;
    private TestArrayAdapter testAdapter;
    private List<Boolean> checkedTests;

    private TextView selectedCourse;
    private Spinner typeSpinner;
    private TextInputLayout descriptionWrapper;
    private TextInputLayout valueWrapper;
    private TextInputEditText descriptionField;
    private TextInputEditText valueField;

    private TextInputLayout correctAnswerWrapper;
    private TextInputLayout incorrectAnswerWrapper;
    private TextInputEditText correctAnswerField;
    private TextInputEditText incorrectAnswerField;

    private TextInputLayout numberOfCorrectWrapper;
    private TextInputLayout numberOfIncorrectWrapper;
    private TextInputEditText numberOfCorrectField;
    private TextInputEditText numberOfIncorrectField;

    private int role;
    private boolean editQuestion;

    private Question draftQuestion;

    private List<Answer> correctAnswers;
    private List<Answer> incorrectAnswers;

    private int numberOfCorrect;
    private int numberOfIncorrect;

    private int startingNumberOfCorrect;
    private int startingNumberOfIncorrect;

    private String lastCourseSearch;
    private boolean exactCourseSearch;

    private List<String> testIds;

    private List<Course> teachersCourses;

    private FirebaseAuth auth;
    private DatabaseReference rootReference;
    private DatabaseReference questionReference;
    private String questionId;

    private List<Test> questionTests;

    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_create);

        auth = FirebaseAuth.getInstance();

        compositeDisposable = new CompositeDisposable();

        checkedTests = new ArrayList<>();
        questionTests = new ArrayList<>();

        role = getIntent().getIntExtra(FLAG_USER_ROLE, -1);
        questionId = getIntent().getStringExtra(FLAG_QUESTION_ID);
        editQuestion = !TextUtils.isEmpty(questionId);

        initViews();

        initialState();

        if (editQuestion) {

            compositeDisposable.add(
                    Util.getQuestionFilteredAnswerIds(questionId, true)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(ids -> compositeDisposable.add(
                                    Util.observeQuestionsFilteredAnswers(questionId, ids)
                                            .subscribe(correctAnswers ->
                                                    this.correctAnswers = correctAnswers)
                            ))
            );

            compositeDisposable.add(
                    Util.getQuestionFilteredAnswerIds(questionId, false)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(ids -> compositeDisposable.add(
                                    Util.observeQuestionsFilteredAnswers(questionId, ids)
                                            .subscribe(incorrectAnswers ->
                                                    this.incorrectAnswers = incorrectAnswers)
                            ))
            );

            compositeDisposable.add(
                    Util.observeQuestionById(questionId)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(question -> {
                                draftQuestion = question;
                                compositeDisposable.add(
                                        Util.getCourseIdentifier(draftQuestion.getCourse())
                                        .subscribe(course -> selectedCourse.setText(course))
                                );
                                descriptionField.setText(question.getDescription());
                                valueField.setText(""+question.getPointsValue());
                                typeSpinner.setSelection(question.getType());
                                stateFromCourseToTest();
                            })
            );
            compositeDisposable.add(
                    Util.observeQuestionsTests(questionId)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(tests -> questionTests = tests)
            );
        } else {
            draftQuestion = new Question();
            draftQuestion.setType(0);
        }

        addListeners();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (currentState.equals(STATE_TEST)) {
            performTestSearch();
        }
    }

    private void initViews() {
        title = findViewById(R.id.textview_questionCreate_title);

        adminCourseSearchLayout = findViewById(R.id.layout_questionCreate_searchCourse);
        courseSearch = findViewById(R.id.tiledittext_questionCreate_courseSearch);
        emptyCourseView = findViewById(R.id.textview_questionCreate_courseSearchEmptyList);
        courseSearchResults = findViewById(R.id.listview_questionCreate_courseSearchResults);

        teacherCourseSearchLayout = findViewById(R.id.layout_questionCreate_pickCourse);
        courseSpinner = findViewById(R.id.spinner_questionCreate_course);
        confirmCourse = findViewById(R.id.button_questionCreate_confirmCourse);

        buttonLayout = findViewById(R.id.layout_questionCreate_buttons);
        previousButton = findViewById(R.id.button_questionCreate_previous);
        nextButton = findViewById(R.id.button_questionCreate_next);

        emptyTestView = findViewById(R.id.textview_questionCreate_testSearchEmptyList);
        testSearchResults = findViewById(R.id.listview_questionCreate_testSearchResults);

        detailsLayout = findViewById(R.id.layout_questionCreate_details);
        selectedCourse = findViewById(R.id.textview_questionCreate_selectedCourse);
        typeSpinner = findViewById(R.id.spinner_questionCreate_type);
        descriptionWrapper = findViewById(R.id.til_questionCreate_description);
        valueWrapper = findViewById(R.id.til_questionCreate_points);
        descriptionField = findViewById(R.id.tiledittext_questionCreate_description);
        valueField = findViewById(R.id.tiledittext_questionCreate_points);

        answersLayout = findViewById(R.id.layout_questionCreate_answers);
        correctAnswerWrapper = findViewById(R.id.til_questionCreate_correctAnswer);
        incorrectAnswerWrapper = findViewById(R.id.til_questionCreate_incorrectAnswer);
        correctAnswerField = findViewById(R.id.tiledittext_questionCreate_correctAnswer);
        incorrectAnswerField = findViewById(R.id.tiledittext_questionCreate_incorrectAnswer);

        metadataLayout = findViewById(R.id.layout_questionCreate_metadata);
        numberOfCorrectWrapper = findViewById(R.id.til_questionCreate_numberOfCorrectAnswers);
        numberOfIncorrectWrapper = findViewById(R.id.til_questionCreate_numberOfIncorrectAnswers);
        numberOfCorrectField = findViewById(R.id.tiledittext_questionCreate_numberOfCorrectAnswers);
        numberOfIncorrectField = findViewById
                (R.id.tiledittext_questionCreate_numberOfIncorrectAnswers);
    }

    private void addListeners() {

        courseSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //must override, not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SearchMetadata searchMetadata = Util.onSearchTextChanged(courseSearch, null,
                        lastCourseSearch, exactCourseSearch, rootReference,
                        emptyCourseView, courseSearchResults, courseAdapter);
                lastCourseSearch = searchMetadata.getLastSearch();
                exactCourseSearch = searchMetadata.isExactSearch();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //must override, not needed
            }
        });
        courseSearch.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            ExactSearchHandler handler = new ExactSearchHandler(lastCourseSearch, exactCourseSearch);
            handler.performSearch(actionId, courseSearch, rootReference, emptyCourseView,
                    courseSearchResults, courseAdapter, this);
            lastCourseSearch = handler.getLastSearch();
            exactCourseSearch = handler.isExactSearch();
            return handler.isHandled();
        });

        courseSearchResults.setOnItemClickListener((adapterView, view, i, l) -> {
            adminCourseSearchLayout.setVisibility(View.GONE);
            CourseViewHolderItem holder = (CourseViewHolderItem) view.getTag();
            String course = holder.resultView.getText().toString();
            draftQuestion.setCourse(courseAdapter.getCourses().get(i).getId());
            selectedCourse.setText(course);
            stateFromCourseToTest();
        });

        confirmCourse.setOnClickListener(view -> {
            teacherCourseSearchLayout.setVisibility(View.GONE);
            String course = courseSpinner.getSelectedItem().toString();
            draftQuestion.setCourse
                    (teachersCourses.get(courseSpinner.getSelectedItemPosition()).getId());
            selectedCourse.setText(course);
            stateFromCourseToTest();
        });

        testSearchResults.setOnItemClickListener((adapterView, view, i, l) -> {
            TestViewHolderItem holder = (TestViewHolderItem)view.getTag();
            holder.selectedCheckbox.setChecked(!holder.selectedCheckbox.isChecked());
        });

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (draftQuestion.getType() != position) {
                    draftQuestion.setType(position);
                    if (editQuestion) {
                        correctAnswers.clear();
                        incorrectAnswers.clear();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //must override, not needed
            }
        });

        emptyTestView.setOnClickListener(view -> {
            Intent startTestCreate = new Intent(this, TestCreateActivity.class);
            startTestCreate.putExtra(FLAG_USER_ROLE, role);
            startTestCreate.putExtra(FLAG_COURSE_ID, draftQuestion.getCourse());
            startActivity(startTestCreate);
        });

        previousButton.setOnClickListener(view -> {
            Util.closeKeyboard(this);
            switch (currentState) {
                case STATE_TEST:
                    stateFromTestToCourse();
                    break;
                case STATE_DETAILS:
                    stateFromDetailsToTest();
                    break;
                case STATE_METADATA:
                    stateFromMetadataToDetails();
                    break;
                case STATE_ANSWERS:
                    if (draftQuestion.getType() == TYPE_FREE_ANSWER) {
                        stateFromAnswersToDetails();
                    } else {
                        stateFromAnswersToMetadata();
                    }
                    break;
            }
        });

        nextButton.setOnClickListener(view -> {
            Util.closeKeyboard(this);
            switch (currentState) {
                case STATE_TEST:
                    for (boolean check : checkedTests) {
                        if (check) {
                            stateFromTestToDetails();
                            return;
                        }
                    }
                    break;
                case STATE_DETAILS:
                    if (TextUtils.isEmpty(descriptionField.getText().toString())) {
                        descriptionWrapper.setError(getString(R.string.mandatory_field));
                    } else {
                        descriptionWrapper.setError(null);
                        descriptionWrapper.setErrorEnabled(false);
                    }

                    if (TextUtils.isEmpty(valueField.getText().toString())) {
                        valueWrapper.setError(getString(R.string.mandatory_field));
                    } else {
                        valueWrapper.setError(null);
                        valueWrapper.setErrorEnabled(false);
                    }

                    if (!valueWrapper.isErrorEnabled() && !descriptionWrapper.isErrorEnabled()) {
                        draftQuestion.setDescription(descriptionField.getText().toString());
                        draftQuestion.setPointsValue
                                (Double.parseDouble(valueField.getText().toString()));

                        if (draftQuestion.getType() == TYPE_FREE_ANSWER) {
                            stateFromDetailsToAnswers();
                        } else {
                            stateFromDetailsToMetadata();
                        }
                    }
                    break;
                case STATE_METADATA:

                    if (TextUtils.isEmpty(numberOfIncorrectField.getText().toString())) {
                        numberOfIncorrectWrapper.setError(getString(R.string.mandatory_field));
                    } else {
                        numberOfIncorrect = Integer
                                .parseInt(numberOfIncorrectField.getText().toString());
                        int min = draftQuestion.getType() == TYPE_SINGLE_CORRECT ? 1 : 0;
                        if (numberOfIncorrect < min) {
                            numberOfIncorrectWrapper
                                    .setError(getString(R.string.must_be_higher_than, min));
                        } else if (numberOfIncorrect > 9) {
                            numberOfIncorrectWrapper
                                    .setError(getString(R.string.must_be_lower_than, 10));
                        } else {
                            numberOfIncorrectWrapper.setError(null);
                            numberOfIncorrectWrapper.setErrorEnabled(false);
                            if (editQuestion) {
                                startingNumberOfIncorrect = numberOfIncorrect;
                                if (numberOfIncorrect < incorrectAnswers.size()) {
                                    incorrectAnswers = incorrectAnswers
                                            .subList(0, numberOfIncorrect);
                                }
                            }
                        }

                    }

                    if (draftQuestion.getType() == TYPE_MULTIPLE_CORRECT) {
                        if (TextUtils.isEmpty(numberOfCorrectField.getText().toString())) {
                            numberOfCorrectWrapper.setError(getString(R.string.mandatory_field));
                        } else {
                            numberOfCorrect = Integer
                                    .parseInt(numberOfCorrectField.getText().toString());
                            if (numberOfCorrect < 1) {
                                numberOfCorrectWrapper
                                        .setError(getString(R.string.must_be_higher_than, 1));
                            } else if (numberOfCorrect > 9) {
                                numberOfCorrectWrapper
                                        .setError(getString(R.string.must_be_lower_than, 10));
                            } else if ((numberOfIncorrect + numberOfCorrect) < 2) {
                                String error = getString(R.string.must_have_more_than_one_answer);
                                numberOfCorrectWrapper.setError(error);
                                numberOfIncorrectWrapper.setError(error);
                            } else {
                                numberOfCorrectWrapper.setError(null);
                                numberOfCorrectWrapper.setErrorEnabled(false);
                                if (editQuestion) {
                                    startingNumberOfCorrect = numberOfCorrect;
                                    if (numberOfCorrect < correctAnswers.size()) {
                                        correctAnswers = correctAnswers.subList(0, numberOfCorrect);
                                    }
                                }
                            }
                        }
                    } else {
                        numberOfCorrect = 1;
                        if (editQuestion) {
                            startingNumberOfCorrect = numberOfCorrect;
                        }
                    }

                    if (!numberOfCorrectWrapper.isErrorEnabled() &&
                            !numberOfIncorrectWrapper.isErrorEnabled()) {
                        stateFromMetadataToAnswers();
                    }
                    break;
                case STATE_ANSWERS:
                    switch (draftQuestion.getType()) {
                        case TYPE_FREE_ANSWER:
                            String answer = correctAnswerField.getText().toString();
                            if (TextUtils.isEmpty(answer)) {
                                correctAnswerWrapper.setError(getString(R.string.mandatory_field));
                            } else {
                                correctAnswerWrapper.setError(null);
                                correctAnswerWrapper.setErrorEnabled(false);
                                if (!editQuestion || correctAnswers.isEmpty()) {
                                    Answer correctAnswer = new Answer();
                                    correctAnswer.setAnswer(answer);
                                    correctAnswer.setQuestion(questionId);
                                    correctAnswers.add(correctAnswer);
                                } else {
                                    correctAnswers.get(0).setAnswer(answer);
                                }
                                createQuestion();
                            }
                            break;
                        case TYPE_SINGLE_CORRECT:
                        case TYPE_MULTIPLE_CORRECT:
                            if (numberOfCorrect > 0) {
                                createCorrectAnswer();
                                if (numberOfCorrect == 0) {
                                    hideViews(correctAnswerWrapper);
                                    if (numberOfIncorrect > 0) {
                                        showViews(incorrectAnswerWrapper);
                                    } else {
                                        createQuestion();
                                    }
                                }
                            } else {
                                createIncorrectAnswer();
                                if (numberOfIncorrect == 0) {
                                    createQuestion();
                                }
                            }
                            break;
                    }
                    break;
            }
        });
    }

    private void initialState() {
        currentState = STATE_COURSE;
        title.setText(getString(R.string.select_course));
        rootReference = Util.getRootReference();
        if (!editQuestion) {
            questionReference = rootReference.child(QUESTIONS_TABLE).push();
            questionId = questionReference.getKey();
            switch (role) {
                case ROLE_ADMINISTRATOR:
                    courseAdapter = new SimpleCourseArrayAdapter
                            (this, new ArrayList<>(), emptyCourseView, courseSearchResults);
                    adminCourseSearchLayout.setVisibility(View.VISIBLE);
                    courseSearchResults.setAdapter(courseAdapter);
                    break;
                case ROLE_TEACHING_STAFF:
                    compositeDisposable.add(Util.observeTeachersCourses(auth.getUid())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(courses -> {
                                teachersCourses = courses;
                                String[] coursesArray = new String[courses.size()];
                                for (int i = 0; i < courses.size(); i++) {
                                    coursesArray[i] = courses.get(i).formCourseIdentifier();
                                }
                                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                                        R.layout.custom_spinner_item,
                                        coursesArray);
                                courseSpinner.setAdapter(spinnerAdapter);
                                teacherCourseSearchLayout.setVisibility(View.VISIBLE);
                            }));
                    break;
                default:
                    Log.d(QuestionCreateActivity.class.getSimpleName(), "Invalid user role");
                    finish();
                    break;
            }

        } else {
            questionReference = rootReference.child(QUESTIONS_TABLE).child(questionId);
        }
        final String[] questionTypes = getResources().getStringArray(R.array.questionTypes);
        ArrayAdapter<String> typeSpinnerAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item,
                questionTypes);
        typeSpinner.setAdapter(typeSpinnerAdapter);

        correctAnswers = new ArrayList<>();
        incorrectAnswers = new ArrayList<>();
        testIds = new ArrayList<>();
    }

    private void stateFromCourseToTest() {
        currentState = STATE_TEST;
        title.setText(getString(R.string.select_tests));
        testAdapter = new TestArrayAdapter(this, new ArrayList<>());
        testSearchResults.setAdapter(testAdapter);

        buttonLayout.setVisibility(View.VISIBLE);
        if (editQuestion) {
            previousButton.setEnabled(false);
        }

        performTestSearch();
    }

    private void stateFromTestToCourse() {
        currentState = STATE_COURSE;
        title.setText(getString(R.string.select_course));
        hideViews(buttonLayout, emptyTestView, testSearchResults);
        checkedTests.clear();
        if (role == ROLE_ADMINISTRATOR) {
            adminCourseSearchLayout.setVisibility(View.VISIBLE);
        } else {
            teacherCourseSearchLayout.setVisibility(View.VISIBLE);
        }
    }

    private void stateFromTestToDetails() {
        currentState = STATE_DETAILS;
        title.setText(getString(R.string.input_question_details));
        if (editQuestion) {
            previousButton.setEnabled(true);
        }
        for (int i = 0; i < testAdapter.tests.size(); i++) {
            if (checkedTests.get(i)) {
                testIds.add(testAdapter.tests.get(i).getId());
            }
        }
        hideViews(emptyTestView, testSearchResults);
        detailsLayout.setVisibility(View.VISIBLE);
    }

    private void stateFromDetailsToTest() {
        currentState = STATE_TEST;
        title.setText((getString(R.string.select_tests)));
        hideViews(detailsLayout);
        if (editQuestion) {
            previousButton.setEnabled(false);
        }

        testIds.clear();
        Util.hideEmptyViewAndShowList(emptyTestView, testSearchResults);
    }

    private void stateFromDetailsToMetadata() {
        currentState = STATE_METADATA;
        title.setText(getString(R.string.input_number_of_answers));

        hideViews(detailsLayout);

        if (editQuestion && !incorrectAnswers.isEmpty()) {
            numberOfIncorrectField.setText(""+incorrectAnswers.size());
        }
        if (draftQuestion.getType() == TYPE_SINGLE_CORRECT) {
            showViews(answersLayout, metadataLayout, numberOfIncorrectWrapper);
            hideViews(numberOfCorrectWrapper);
        } else if (draftQuestion.getType() == TYPE_MULTIPLE_CORRECT) {
            if (editQuestion && !correctAnswers.isEmpty()) {
                numberOfCorrectField.setText(""+correctAnswers.size());
            }
            showViews(answersLayout, metadataLayout, numberOfIncorrectWrapper,
                    numberOfCorrectWrapper);
        }
    }

    private void stateFromDetailsToAnswers() {
        currentState = STATE_ANSWERS;
        title.setText(getString(R.string.input_answer_details));

        if (editQuestion && !correctAnswers.isEmpty()) {
            correctAnswerField.setText(correctAnswers.get(0).getAnswer());
        }
        hideViews(detailsLayout);
        showViews(answersLayout, correctAnswerWrapper);
    }

    private void stateFromMetadataToDetails() {
        currentState = STATE_DETAILS;
        title.setText(getString(R.string.input_question_details));

        if (draftQuestion.getType() == TYPE_SINGLE_CORRECT) {
            hideViews(answersLayout, metadataLayout, numberOfIncorrectWrapper);
        } else if (draftQuestion.getType() == TYPE_MULTIPLE_CORRECT) {
            hideViews(answersLayout, metadataLayout, numberOfIncorrectWrapper,
                    numberOfCorrectWrapper);
        }
        showViews(detailsLayout);
    }

    private void stateFromAnswersToDetails() {
        currentState = STATE_DETAILS;
        title.setText(getString(R.string.input_question_details));

        hideViews(answersLayout, correctAnswerWrapper);
        detailsLayout.setVisibility(View.VISIBLE);
    }

    private void stateFromMetadataToAnswers() {
        currentState = STATE_ANSWERS;
        title.setText(getString(R.string.input_answer_details));

        if (editQuestion && !correctAnswers.isEmpty()) {
            correctAnswerField.setText(correctAnswers.get(0).getAnswer());
        }
        if (editQuestion && !incorrectAnswers.isEmpty()) {
            incorrectAnswerField.setText(incorrectAnswers.get(0).getAnswer());
        }
        hideViews(metadataLayout);
        showViews(correctAnswerWrapper);
    }

    private void stateFromAnswersToMetadata() {
        currentState = STATE_METADATA;
        title.setText(getString(R.string.input_number_of_answers));

        hideViews(correctAnswerWrapper, incorrectAnswerWrapper);

        correctAnswers.clear();
        incorrectAnswers.clear();
        correctAnswerField.setText("");
        correctAnswerWrapper.setError(null);
        correctAnswerWrapper.setErrorEnabled(false);
        incorrectAnswerField.setText("");
        incorrectAnswerWrapper.setError(null);
        incorrectAnswerWrapper.setErrorEnabled(false);

        if (draftQuestion.getType() == TYPE_SINGLE_CORRECT) {
            showViews(answersLayout, metadataLayout, numberOfIncorrectWrapper);
            hideViews(numberOfCorrectWrapper);
        } else if (draftQuestion.getType() == TYPE_MULTIPLE_CORRECT) {
            showViews(answersLayout, metadataLayout, numberOfIncorrectWrapper,
                    numberOfCorrectWrapper);
        }
    }

    private void createAnswer(Answer answer, boolean correct) {
        DatabaseReference drA = rootReference.child(ANSWERS_TABLE).push();
        drA.setValue(answer);
        addAnswerToQuestion(drA.getKey(), correct);
    }

    private void createQuestion() {
        questionReference.setValue(draftQuestion).addOnSuccessListener(aVoid -> {
            if (editQuestion) {
                Scheduler backgroundThread = Schedulers.newThread();
                compositeDisposable.add(Util.removeQuestionAnswers
                        (questionId, backgroundThread, compositeDisposable)
                        .subscribeOn(backgroundThread)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            for (Answer correctAnswer : correctAnswers) {
                                createAnswer(correctAnswer, true);
                            }
                            for (Answer incorrectAnswer : incorrectAnswers) {
                                createAnswer(incorrectAnswer, false);
                            }
                            compositeDisposable.add(
                                    Util.removeQuestionFromTests(questionId, compositeDisposable)
                                            .subscribeOn(backgroundThread)
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(() -> {
                                                for (String tid : testIds) {
                                                    removeTestsFromQuestion()
                                                            .addOnSuccessListener(aVoid1 ->
                                                                    linkQuestionToTest(tid));
                                                }
                                                finish();
                                            })
                            );
                        })
                );
            } else {
                for (Answer correctAnswer : correctAnswers) {
                    createAnswer(correctAnswer, true);
                }
                for (Answer incorrectAnswer : incorrectAnswers) {
                    createAnswer(incorrectAnswer, false);
                }
                for (String tid : testIds) {
                    linkQuestionToTest(tid);
                }
                finish();
            }
        });
    }

    private void addAnswerToQuestion(String aid, boolean correct) {
        rootReference.child(QUESTIONS_TABLE).child(questionId)
                .child(correct ? QUESTION_CHILD_CORRECT_ANSWERS : QUESTION_CHILD_INCORRECT_ANSWERS)
                .child(aid).setValue(true);
    }

    private void addQuestionToTest(String tid) {
        rootReference.child(TESTS_TABLE).child(tid)
                .child(TESTS_CHILD_QUESTIONS).child(questionId).setValue(true);
    }

    private void addTestToQuestion(String tid) {
        rootReference.child(QUESTIONS_TABLE).child(questionId)
                .child(QUESTION_CHILD_TESTS).child(tid).setValue(true);
    }

    private Task<Void> removeTestsFromQuestion() {
        return rootReference.child(QUESTIONS_TABLE).child(questionId)
                .child(QUESTION_CHILD_TESTS).removeValue();
    }

    private void linkQuestionToTest(String tid) {
        addQuestionToTest(tid);
        addTestToQuestion(tid);
    }

    private void createCorrectAnswer() {
        String answer = correctAnswerField.getText().toString();
        if (TextUtils.isEmpty(answer)) {
            correctAnswerWrapper.setError(getString(R.string.mandatory_field));
        } else {
            correctAnswerWrapper.setError(null);
            correctAnswerWrapper.setErrorEnabled(false);
            if (!editQuestion || correctAnswers.isEmpty()
                    || (startingNumberOfCorrect - numberOfCorrect) >= correctAnswers.size() ) {
                Answer correctAnswer = new Answer();
                correctAnswer.setAnswer(answer);
                correctAnswer.setQuestion(questionId);
                correctAnswerField.setText("");
                correctAnswers.add(correctAnswer);
                numberOfCorrect--;
            } else {
                correctAnswers.get(startingNumberOfCorrect - numberOfCorrect).setAnswer(answer);
                numberOfCorrect--;
                if (numberOfCorrect > 0) {
                    if ((startingNumberOfCorrect - numberOfCorrect) < correctAnswers.size()) {
                        correctAnswerField.setText(correctAnswers
                                .get(startingNumberOfCorrect - numberOfCorrect).getAnswer());
                    } else {
                        correctAnswerField.setText("");
                    }
                }
            }
        }

    }

    private void createIncorrectAnswer() {
        String answer = incorrectAnswerField.getText().toString();
        if (TextUtils.isEmpty(answer)) {
            incorrectAnswerWrapper.setError(getString(R.string.mandatory_field));
        } else {
            incorrectAnswerWrapper.setError(null);
            incorrectAnswerWrapper.setErrorEnabled(false);
            if (!editQuestion || incorrectAnswers.isEmpty()
                    || (startingNumberOfIncorrect - numberOfIncorrect) >= incorrectAnswers.size() ) {
                Answer incorrectAnswer = new Answer();
                incorrectAnswer.setAnswer(answer);
                incorrectAnswer.setQuestion(questionId);
                incorrectAnswerField.setText("");
                incorrectAnswers.add(incorrectAnswer);
                numberOfIncorrect--;
            } else {
                incorrectAnswers.get(startingNumberOfIncorrect - numberOfIncorrect).setAnswer(answer);
                numberOfIncorrect--;
                if (numberOfIncorrect > 0) {
                    if ((startingNumberOfIncorrect - numberOfIncorrect) < incorrectAnswers.size()) {
                        incorrectAnswerField.setText(incorrectAnswers
                                .get(startingNumberOfIncorrect - numberOfIncorrect).getAnswer());
                    } else {
                        incorrectAnswerField.setText("");
                    }
                }
            }
        }

    }

    private void performTestSearch() {
        compositeDisposable.add(Util.observeCoursesTests(draftQuestion.getCourse())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tests -> {
                    if (checkedTests.size() != tests.size()) {
                        checkedTests.clear();
                        for(int i = 0; i < tests.size(); i++) {
                            checkedTests.add(i, questionTests.contains(tests.get(i)));
                        }
                    }
                    Util.setViewVisibilityAndUpdateAdapter
                            (tests, emptyTestView, testSearchResults, testAdapter);
                })
        );
    }

    private void showViews(View... views) {
        for (View view : views) {
            view.setVisibility(View.VISIBLE);
        }
    }

    private void hideViews(View... views) {
        for (View view : views) {
            view.setVisibility(View.GONE);
        }
    }

    private class TestArrayAdapter extends RefreshAdapter<Test> {

        private Context context;
        private List<Test> tests;

        public TestArrayAdapter(@NonNull Context context, @NonNull List<Test> tests) {
            super(context, tests);
            this.context = context;
            this.tests = tests;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            TestViewHolderItem holder;
            Test test = tests.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.simple_test_list_item, parent, false);

                holder = new TestViewHolderItem();
                holder.nameView = convertView.findViewById(R.id.textview_simpleTest_test);
                holder.selectedCheckbox = convertView
                        .findViewById(R.id.checkbox_simpleTest_selected);
                if (checkedTests != null) {
                    holder.selectedCheckbox.setChecked(checkedTests.get(position));
                }

                convertView.setTag(holder);
            } else {
                holder = (TestViewHolderItem) convertView.getTag();
                holder.selectedCheckbox.setOnCheckedChangeListener(null);
                holder.selectedCheckbox.setChecked(checkedTests.get(position));
            }

            compositeDisposable.add(
                    Util.getCourseCode(test.getCourse())
                    .subscribe(courseCode ->
                            holder.nameView.setText(test.formName(courseCode, context))
                    )
            );


            holder.selectedCheckbox.setTag(String.valueOf(position));

            holder.selectedCheckbox.setOnCheckedChangeListener(
                    (CompoundButton buttonView, boolean isChecked) -> {
                        int pos = Integer.parseInt(buttonView.getTag().toString());
                        checkedTests.set(pos, isChecked);
                    });

            return convertView;

        }

        @Override
        public void refreshList(List<Test> refreshedList) {
            tests.clear();
            if (refreshedList.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyTestView, testSearchResults);
            } else {
                tests.addAll(refreshedList);
                notifyDataSetChanged();
            }
        }
    }

    private static class TestViewHolderItem {
        TextView nameView;
        CheckBox selectedCheckbox;
    }
}
