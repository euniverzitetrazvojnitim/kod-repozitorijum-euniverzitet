package com.miroslav727.euniverzitet.testModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.Question;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.testModule.QuestionCreateActivity.FLAG_QUESTION_ID;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_TEST_ID;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_USER_ROLE;

public class TestQuestionsDisplayActivity extends AppCompatActivity {

    public static final String FLAG_TEST_QUESTIONS_TITLE = "test_questions_title";

    private ActionMode actionMode;

    private int selectedCounter;

    private int role;

    private String testId;

    private List<Boolean> checkedForDeletion;

    private TextView emptyListView;

    private ListView listView;

    private QuestionArrayAdapter adapter;

    private CompositeDisposable compositeDisposable;

    //region action mode
    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.general_action_menu, menu);
            setActionModeTitle(mode);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menuitem_delete:

                    ArrayList<Question> remainingQuestions = new ArrayList<>();

                    for (int i = 0; i < checkedForDeletion.size(); i++) {
                        Question question = adapter.questions.get(i);
                        if (checkedForDeletion.get(i)) {
                            removeQuestion(question.getId());
                        } else {
                            remainingQuestions.add(question);
                        }
                    }
                    checkedForDeletion.clear();
                    adapter.refreshList(remainingQuestions);
                    for (int i = 0; i < remainingQuestions.size(); i++) {
                        checkedForDeletion.set(i, false);
                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.setInActionMode(false);
            selectedCounter = 0;
            for (int i = 0; i < checkedForDeletion.size(); i++) {
                checkedForDeletion.set(i, false);
            }
            actionMode = null;
        }
    };

    private void setActionModeTitle (@NonNull ActionMode mode) {
        mode.setTitle(getResources()
                .getQuantityString(R.plurals.selected, selectedCounter, selectedCounter));
    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_questions_display);

        compositeDisposable = new CompositeDisposable();

        checkedForDeletion = new ArrayList<>();

        TextView title = findViewById(R.id.textview_testQuestionsDisplay_title);
        title.setText(getIntent().getStringExtra(FLAG_TEST_QUESTIONS_TITLE));

        role = getIntent().getIntExtra(FLAG_USER_ROLE, -1);

        testId = getIntent().getStringExtra(FLAG_TEST_ID);

        emptyListView = findViewById(R.id.textview_testQuestionsDisplay_emptyList);
        listView = findViewById(R.id.listview_testQuestionsDisplay_results);

        adapter = new QuestionArrayAdapter(this, new ArrayList<>());
        listView.setAdapter(adapter);

        addListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        compositeDisposable.add(
                Util.observeTestsQuestions(testId)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(questions -> Util.setViewVisibilityAndUpdateAdapter
                                (questions, emptyListView, listView, adapter))
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }


    private void addListeners() {
        FloatingActionButton fab = findViewById(R.id.fab_testQuestionsDisplay_createNewTestQuestion);
        fab.setOnClickListener(view -> {
            Intent startQuestionCreate = new Intent( this, QuestionCreateActivity.class);
            startQuestionCreate.putExtra(FLAG_USER_ROLE, role);
            startActivity(startQuestionCreate);
        });

        listView.setOnItemClickListener((AdapterView<?> parent, View view,
                                         int position, long id) -> {
            ViewHolderItem holder = (ViewHolderItem) view.getTag();
            if (actionMode != null) {
                if (holder.deleteCheckbox.isChecked()) {
                    holder.deleteCheckbox.setChecked(false);
                    checkedForDeletion.set(position, false);
                } else {
                    holder.deleteCheckbox.setChecked(true);
                    checkedForDeletion.set(position, true);
                }
            } else {
                Intent startQuestionEdit = new Intent(this, QuestionCreateActivity.class);
                startQuestionEdit.putExtra(FLAG_QUESTION_ID, adapter.questions.get(position).getId());
                startQuestionEdit.putExtra(FLAG_USER_ROLE, role);
                startActivity(startQuestionEdit);
            }
        });

        listView.setOnItemLongClickListener((AdapterView<?> parent, View view,
                                             int position, long id) -> {
            if (actionMode == null) {
                actionMode = ((AppCompatActivity) view.getContext())
                        .startSupportActionMode(actionModeCallback);
                adapter.setInActionMode(true);
                ViewHolderItem holder = (ViewHolderItem) view.getTag();
                checkedForDeletion.set(position, true);
                holder.deleteCheckbox.setChecked(true);
                selectedCounter = 1;
            }
            return true;
        });
    }

    private void removeQuestion(String questionId) {
        Scheduler thread = Schedulers.newThread();
        Util.removeQuestion(questionId).addOnSuccessListener(aVoid -> {
            compositeDisposable.add(Util.removeQuestionAnswers(questionId, thread, compositeDisposable)
                    .subscribe(() -> removeQuestionFromTests(questionId))
            );

        });
    }

    private void removeQuestionFromTests(String questionId) {
        compositeDisposable.add(Util.removeQuestionFromTests(questionId, compositeDisposable)
                .subscribe()
        );
    }

    private class QuestionArrayAdapter extends RefreshAdapter<Question> {

        private Context context;
        private List<Question> questions;
        private boolean inActionMode;

        private QuestionArrayAdapter(Context context, List<Question> questions) {
            super(context, questions);
            this.context = context;
            this.questions = questions;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;
            Question question = questions.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.test_question_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.questionView = convertView
                        .findViewById(R.id.textview_testQuestionDisplay_question);
                holder.pointsView = convertView
                        .findViewById(R.id.textview_testQuestionDisplay_points);
                holder.deleteCheckbox = convertView
                        .findViewById(R.id.checkbox_testQuestionDisplay_delete);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
                holder.deleteCheckbox.setOnCheckedChangeListener(null);
                holder.deleteCheckbox.setChecked(checkedForDeletion.get(position));
            }

            holder.questionView.setText(question.getDescription());
            String poi = "" + question.getPointsValue();
            holder.pointsView.setText(poi);

            holder.deleteCheckbox.setVisibility(inActionMode ? View.VISIBLE : View.GONE);
            holder.deleteCheckbox.setTag(String.valueOf(position));

            holder.deleteCheckbox.setOnCheckedChangeListener(
                    (CompoundButton buttonView, boolean isChecked) -> {
                        int pos = Integer.parseInt(buttonView.getTag().toString());
                        checkedForDeletion.set(pos, isChecked);
                        if (isChecked) {
                            selectedCounter++;
                            setActionModeTitle(actionMode);
                        } else {
                            selectedCounter--;
                            if (selectedCounter == 0) {
                                actionMode.finish();
                            } else {
                                setActionModeTitle(actionMode);
                            }
                        }
                    });

            return convertView;
        }

        void setInActionMode(boolean inActionMode) {
            this.inActionMode = inActionMode;
            notifyDataSetChanged();
        }

        @Override
        public void refreshList(List<Question> newQuestions) {
            questions.clear();
            checkedForDeletion.clear();
            if (newQuestions.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
            } else {
                questions.addAll(newQuestions);
                for (int i = 0; i < newQuestions.size(); i++) {
                    checkedForDeletion.add(false);
                }
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {

        TextView questionView;
        TextView pointsView;
        CheckBox deleteCheckbox;
    }
}
