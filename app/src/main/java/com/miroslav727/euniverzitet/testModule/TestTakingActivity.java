package com.miroslav727.euniverzitet.testModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.model.Answer;
import com.miroslav727.euniverzitet.model.Question;
import com.miroslav727.euniverzitet.model.Test;
import com.miroslav727.euniverzitet.util.Util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.testModule.TestResultsActivity.KEY_TEST_RESULT;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_TEST_ID;
import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.KEY_TEST_TITLE;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_FREE_ANSWER;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_MULTIPLE_CORRECT;
import static com.miroslav727.euniverzitet.util.Constants.TYPE_SINGLE_CORRECT;

public class TestTakingActivity extends AppCompatActivity {

    private TextView remainingTime;

    private boolean started;
    private CountDownTimer timer;

    private ViewPager2 questionsPager;
    private PagerAdapter adapter;

    private String recievedTitle;

    private CompositeDisposable compositeDisposable;

    private Test takenTest;

    private List<Question> testQuestions;
    private List<List<Answer>> testAnswers;

    private SparseArray<SparseArray<Boolean>> correctSingleAnswers;
    private SparseArray<SparseArray<Boolean>> correctMultiAnswers;

    private DecimalFormat formatter = new DecimalFormat("#.##");

    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_taking);
        compositeDisposable = new CompositeDisposable();
        auth = FirebaseAuth.getInstance();

        testAnswers = new ArrayList<>();

        correctSingleAnswers = new SparseArray<>();
        correctMultiAnswers = new SparseArray<>();

        questionsPager = findViewById(R.id.viewpager_testTaking_questionsPager);
        questionsPager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);

        remainingTime = findViewById(R.id.textview_testTaking_remainingTime);
        recievedTitle = getIntent().getStringExtra(KEY_TEST_TITLE);
        String title = getString(R.string.test_label) + recievedTitle;
        String testId = getIntent().getStringExtra(FLAG_TEST_ID);
        Scheduler thread = Schedulers.newThread();

        compositeDisposable.add(Util.observeTestById(testId)
                .subscribeOn(thread)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(test -> {
                    takenTest = test;
                    long durationInMillis = takenTest.getDuration() * 60 * 1000;
                    displayRemainingTime(durationInMillis);

                    timer = new CountDownTimer(durationInMillis, 1000) {
                        @Override
                        public void onTick(long milliseconds) {
                            displayRemainingTime(milliseconds);
                        }

                        @Override
                        public void onFinish() {
                            this.cancel();
                            goToResults();
                        }
                    };
                    compositeDisposable.add(Util.observeTestsQuestions(testId)
                            .subscribeOn(thread)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(questions -> {
                                Collections.shuffle(questions);
                                testQuestions = new ArrayList<>();
                                List<Question> tempQuestions = questions.subList
                                        (0, takenTest.getQuestionNumber());
                                for (Question question : tempQuestions) {
                                    compositeDisposable.add(Util.observeQuestionsAnswers
                                            (question.getId())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribeOn(thread)
                                            .subscribe(answers -> {
                                                if (question.getType() != TYPE_FREE_ANSWER) {
                                                    Collections.shuffle(answers);
                                                }
                                                testQuestions.add(question);
                                                testAnswers.add(answers);
                                                if (question.getType() != TYPE_FREE_ANSWER) {
                                                    mapCorrectAnswers(question, thread, answers);
                                                }
                                            })
                                    );
                                }
                                adapter = new PagerAdapter(this,
                                        testQuestions, testAnswers, questionsPager);
                                questionsPager.setAdapter(adapter);
                            })
                    );
                })
        );

        TextView titleView = findViewById(R.id.textview_testTaking_title);
        titleView.setText(title);

        Button startFinish = findViewById(R.id.button_testTaking_startFinishTest);
        startFinish.setOnClickListener(view -> {
            if (started) {
                timer.cancel();
                started = false;
                questionsPager.setVisibility(View.GONE);
                goToResults();
            } else {
                compositeDisposable.add(
                        Util.rxSaveTestResults(takenTest.getId(), auth.getUid(), "0")
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            startFinish.setText(getString(R.string.finish_test));
                            timer.start();
                            started = true;
                            questionsPager.setVisibility(View.VISIBLE);
                        })
                );
            }
        });
    }

    private void mapCorrectAnswers(Question question, Scheduler thread, List<Answer> answers) {
        compositeDisposable.add(
                Util.getQuestionFilteredAnswerIds(question.getId(), true)
                        .subscribeOn(thread)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(ids -> {
                            SparseArray answerMap = new SparseArray();
                            for (int i = 0; i < answers.size(); i++) {
                                Answer answer = answers.get(i);
                                answerMap.append(i, ids.contains(answer.getId()));
                            }
                            if (question.getType() == TYPE_SINGLE_CORRECT) {
                                correctSingleAnswers
                                        .append(testQuestions.indexOf(question), answerMap);
                            } else if (question.getType() == TYPE_MULTIPLE_CORRECT) {
                                correctMultiAnswers
                                        .append(testQuestions.indexOf(question), answerMap);
                            }
                        })
        );
    }

    private void goToResults() {
        String maxPoints = formatter.format
                (takenTest.getPointsValue() * takenTest.getQuestionNumber());
        maxPoints = maxPoints.replace(",",".");
        String result = saveTestResults() + " / " + maxPoints;
        Intent resultIntent = new Intent(TestTakingActivity.this, TestResultsActivity.class);
        resultIntent.putExtra(KEY_TEST_TITLE, recievedTitle);
        resultIntent.putExtra(KEY_TEST_RESULT, result);
        startActivity(resultIntent);
        finish();
    }

    private String saveTestResults() {
        double result = 0.0;
        double pointsPerCorrectAnswer = takenTest.getPointsValue();
        for (int i = 0; i < testQuestions.size(); i++) {
            Question question = testQuestions.get(i);
            List<Answer> answers = testAnswers.get(i);
            switch (question.getType()) {
                case TYPE_FREE_ANSWER: {
                    String userAnswer = adapter.getFreeAnswers().get(i);
                    if (userAnswer != null) {
                        String correctAnswer = answers.get(0).getAnswer();
                        if (userAnswer.equals(correctAnswer)) {
                            result += pointsPerCorrectAnswer;
                        }
                    }
                    break;
                }
                case TYPE_SINGLE_CORRECT: {
                    SparseArray<Boolean> userAnswerMap = adapter.getSingleAnswers().get(i);
                    if (userAnswerMap != null) {
                        SparseArray<Boolean> correctAnswerMap = correctSingleAnswers.get(i);
                        boolean correct = true;
                        for (int j = 0; j < correctAnswerMap.size(); j++) {
                            boolean correctAnswer = correctAnswerMap.get(j);
                            boolean userAnswer = userAnswerMap.get(j);
                            if (userAnswer != correctAnswer) {
                                correct = false;
                                break;
                            }
                        }
                        if (correct) {
                            result += pointsPerCorrectAnswer;
                        }
                    }
                    break;
                }
                case TYPE_MULTIPLE_CORRECT:
                    SparseArray<Boolean> userAnswerMap = adapter.getMultiAnswers().get(i);
                    if (userAnswerMap != null) {
                        SparseArray<Boolean> correctAnswerMap = correctMultiAnswers.get(i);
                        boolean correct = true;
                        for (int j = 0; j < correctAnswerMap.size(); j++) {
                            boolean correctAnswer = correctAnswerMap.get(j);
                            boolean userAnswer = userAnswerMap.get(j);
                            if (userAnswer != correctAnswer) {
                                correct = false;
                                break;
                            }
                        }
                        if (correct) {
                            result += pointsPerCorrectAnswer;
                        }
                    }
                    break;
            }
        }

        String finalResult = formatter.format(result);
        finalResult = finalResult.replace(",", ".");

        Util.saveTestResults(takenTest.getId(), auth.getUid(), finalResult);

        return finalResult;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (started) {
            saveTestResults();
        }
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        timer.cancel();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (!started) {
            super.onBackPressed();
        } else if (questionsPager.getCurrentItem() > 0) {
            questionsPager.setCurrentItem(questionsPager.getCurrentItem() - 1);
        }
    }

    private void displayRemainingTime(long milliseconds) {
        long minutes = milliseconds / (60 * 1000);
        long seconds = milliseconds / 1000 % 60;
        String minutesStr = String.format("%02d", minutes);
        String secondStr = String.format("%02d", seconds);
        String toShow = minutesStr + ":"+secondStr;
        remainingTime.setText(toShow);
    }

    private class PagerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<Question> questions;
        private List<List<Answer>> answers;
        private LayoutInflater inflater;
        private ViewPager2 viewPager;
        private SparseArray<String> freeAnswers;
        private SparseArray<SparseArray<Boolean>> singleAnswers;
        private SparseArray<SparseArray<Boolean>> multiAnswers;

        PagerAdapter(Context context, List<Question> questions, List<List<Answer>> answers,
                     ViewPager2 viewPager) {
            inflater = LayoutInflater.from(context);
            this.questions = questions;
            this.answers = answers;
            this.viewPager = viewPager;

            freeAnswers = new SparseArray<>();
            singleAnswers = new SparseArray<>();
            multiAnswers = new SparseArray<>();
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case TYPE_FREE_ANSWER: {
                    View view = inflater
                            .inflate(R.layout.test_question_free_answer_item, parent, false);
                    return new FreeAnswerHolder(view);
                }
                case TYPE_SINGLE_CORRECT: {
                    View view = inflater
                            .inflate(R.layout.test_question_single_correct_item, parent, false);
                    return new SingleCorrectHolder(view);
                }
                case TYPE_MULTIPLE_CORRECT: {
                    View view = inflater
                            .inflate(R.layout.test_question_multiple_correct_item, parent, false);
                    return new MultipleCorrectHolder(view);
                }
                default:
                    throw new RuntimeException("No such question type");
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            Question question = questions.get(position);
            switch (question.getType()) {
                case TYPE_FREE_ANSWER:
                    ((FreeAnswerHolder)holder).bind(question, position,
                            freeAnswers.get(position, ""));
                    break;
                case TYPE_SINGLE_CORRECT:
                    ((SingleCorrectHolder)holder).bind(question, answers.get(position), position,
                            singleAnswers.get(position, null));
                    break;
                case TYPE_MULTIPLE_CORRECT:
                    ((MultipleCorrectHolder)holder).bind(question, answers.get(position), position,
                            multiAnswers.get(position, null));
                    break;
                default:
                    break;
            }
        }

        @Override
        public int getItemViewType(int position) {
            return questions.get(position).getType();
        }

        @Override
        public int getItemCount() {
            return questions.size();
        }

        @Override
        public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
            int position = holder.getAdapterPosition();
            if (holder instanceof FreeAnswerHolder) {
                FreeAnswerHolder freeAnswerHolder = (FreeAnswerHolder) holder;
                freeAnswers.append(position, freeAnswerHolder.freeAnswerField.getText().toString());
            } else if (holder instanceof SingleCorrectHolder) {
                SingleCorrectHolder singleCorrectHolder = (SingleCorrectHolder) holder;
                SparseArray checked = new SparseArray();
                for (int i = 0; i < singleCorrectHolder.radioHolder.getChildCount(); i++) {
                    RadioButton radio = (RadioButton) singleCorrectHolder.radioHolder.getChildAt(i);
                    checked.append(i, radio.isChecked());
                }
                singleAnswers.append(position, checked);
            } else if (holder instanceof MultipleCorrectHolder) {
                MultipleCorrectHolder multipleCorrectHolder = (MultipleCorrectHolder) holder;
                SparseArray checked = new SparseArray();
                for (int i = 0; i < multipleCorrectHolder.checkboxHolder.getChildCount(); i++) {
                    CheckBox checkbox = (CheckBox) multipleCorrectHolder
                            .checkboxHolder.getChildAt(i);
                    checked.append(i, checkbox.isChecked());
                }
                multiAnswers.append(position, checked);
            }
        }

        public SparseArray<String> getFreeAnswers() {
            return freeAnswers;
        }

        public SparseArray<SparseArray<Boolean>> getSingleAnswers() {
            return singleAnswers;
        }

        public SparseArray<SparseArray<Boolean>> getMultiAnswers() {
            return multiAnswers;
        }

        private class FreeAnswerHolder extends RecyclerView.ViewHolder {
            TextView questionView;
            TextInputLayout freeAnswerWrapper;
            TextInputEditText freeAnswerField;
            Button saveButton;

            FreeAnswerHolder(View itemView){
                super(itemView);
                questionView = itemView
                        .findViewById(R.id.textview_pagerFreeAnswer_questionDisplay);
                freeAnswerWrapper = itemView.findViewById(R.id.til_pagerFreeAnswer_freeAnswer);
                freeAnswerField = itemView.findViewById(R.id.tiledittext_pagerFreeAnswer_freeAnswer);
                saveButton = itemView.findViewById(R.id.button_pagerFreeAnswer_saveAnswer);
                saveButton.setOnClickListener(view -> {
                    if (viewPager.getAdapter().getItemCount() - getAdapterPosition() > 1) {
                        viewPager.setCurrentItem(getAdapterPosition()+1, true);
                    } else {
                        viewPager.setCurrentItem(0, true);
                    }
                });
            }

            public void bind(Question question, int position, String input) {
                String questionString = (position + 1) +". "+ question.getDescription();
                questionView.setText(questionString);
                freeAnswerField.setText(input);
            }

        }

        private class SingleCorrectHolder extends RecyclerView.ViewHolder {

            TextView questionView;
            RadioGroup radioHolder;
            Button saveButton;

            public SingleCorrectHolder(@NonNull View itemView) {
                super(itemView);
                questionView = itemView
                        .findViewById(R.id.textview_pagerSingleCorrect_questionDisplay);
                radioHolder = itemView.findViewById(R.id.radiogroup_pagerSingleCorrect_radioHolder);
                saveButton = itemView.findViewById(R.id.button_pagerSingleCorrect_saveAnswer);
                saveButton.setOnClickListener(view -> {
                    if (viewPager.getAdapter().getItemCount() - getAdapterPosition() > 1) {
                        viewPager.setCurrentItem(getAdapterPosition()+1, true);
                    } else {
                        viewPager.setCurrentItem(0, true);
                    }
                });
            }

            public void bind(Question question, List<Answer> answers, int position,
                             SparseArray<Boolean> checked) {
                String questionString = (position + 1) +". "+ question.getDescription();
                questionView.setText(questionString);
                fillRadioHolder(answers);
                restoreChecks(checked);
            }

            private void fillRadioHolder(List<Answer> answers) {

                if (radioHolder.getChildCount() == 0) {
                    createNewViews(answers);
                } else if (radioHolder.getChildCount() != answers.size()) {
                    radioHolder.removeAllViews();
                    createNewViews(answers);
                } else {
                    for (int i = 0; i < answers.size(); i++) {
                        RadioButton radio = (RadioButton) radioHolder.getChildAt(i);
                        radio.setText(answers.get(i).getAnswer());
                    }
                }
            }

            private void createNewViews(List<Answer> answers) {
                for (Answer answer : answers) {
                    RadioButton radio = new RadioButton(radioHolder.getContext());
                    radio.setText(answer.getAnswer());
                    radio.setTextColor(getResources().getColor(R.color.plainWhite));
                    radioHolder.addView(radio);
                }
            }

            private void restoreChecks(SparseArray<Boolean> checked) {
                if (radioHolder.getChildCount() > 0 ) radioHolder.clearCheck();
                boolean checkedExists = checked != null;
                for (int i = 0; i < radioHolder.getChildCount(); i++) {
                    RadioButton radio = (RadioButton) radioHolder.getChildAt(i);
                    radio.setChecked(checkedExists ? checked.get(i) : false);
                }
            }
        }

        private class MultipleCorrectHolder extends RecyclerView.ViewHolder {

            TextView questionView;
            LinearLayout checkboxHolder;
            Button saveButton;

            public MultipleCorrectHolder(@NonNull View itemView) {
                super(itemView);
                questionView = itemView
                        .findViewById(R.id.textview_pagerMultipleCorrect_questionDisplay);
                checkboxHolder = itemView
                        .findViewById(R.id.layout_pagerMultipleCorrect_checkboxHolder);
                saveButton = itemView.findViewById(R.id.button_pagerMultipleCorrect_saveAnswer);
                saveButton.setOnClickListener(view -> {
                    if (viewPager.getAdapter().getItemCount() - getAdapterPosition() > 1) {
                        viewPager.setCurrentItem(getAdapterPosition()+1, true);
                    } else {
                        viewPager.setCurrentItem(0, true);
                    }
                });
            }

            public void bind(Question question, List<Answer> answers, int position,
                             SparseArray<Boolean> checked) {
                String questionString = (position + 1) +". "+ question.getDescription();
                questionView.setText(questionString);
                fillCheckboxHolder(answers);
                restoreChecks(checked);
            }

            private void fillCheckboxHolder(List<Answer> answers) {

                if (checkboxHolder.getChildCount() == 0) {
                    createNewViews(answers);
                } else if (checkboxHolder.getChildCount() != answers.size()) {
                    checkboxHolder.removeAllViews();
                    createNewViews(answers);
                } else {
                    for (int i = 0; i < answers.size(); i++) {
                        CheckBox checkBox = (CheckBox) checkboxHolder.getChildAt(i);
                        checkBox.setText(answers.get(i).getAnswer());
                    }
                }
            }

            private void createNewViews(List<Answer> answers) {
                for (Answer answer : answers) {
                    CheckBox checkBox = new CheckBox(checkboxHolder.getContext());
                    checkBox.setText(answer.getAnswer());
                    checkBox.setTextColor(getResources().getColor(R.color.plainWhite));
                    checkboxHolder.addView(checkBox);
                }
            }

            private void restoreChecks(SparseArray<Boolean> checked) {
                boolean checkedExists = checked != null;
                for (int i = 0; i < checkboxHolder.getChildCount(); i++) {
                    CheckBox checkbox = (CheckBox) checkboxHolder.getChildAt(i);
                    checkbox.setChecked(checkedExists ? checked.get(i) : false);
                }
            }
        }
    }
}
