package com.miroslav727.euniverzitet.testModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.Course;
import com.miroslav727.euniverzitet.model.Question;
import com.miroslav727.euniverzitet.model.Test;
import com.miroslav727.euniverzitet.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.miroslav727.euniverzitet.testModule.TestQuestionsDisplayActivity.FLAG_TEST_QUESTIONS_TITLE;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_CHILD_TESTS;
import static com.miroslav727.euniverzitet.util.Constants.COURSES_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.QUESTIONS_TABLE;
import static com.miroslav727.euniverzitet.util.Constants.QUESTION_CHILD_TESTS;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_ADMINISTRATOR;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_STUDENT;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEACHING_STAFF;
import static com.miroslav727.euniverzitet.util.Constants.TESTS_TABLE;

public class TestsDisplayActivity extends AppCompatActivity {

    public static final String FLAG_USER_ROLE = "user_role";

    public static final String KEY_TEST_TITLE = "test_title";
    public static final String KEY_TEST_DURATION = "test_duration";
    public static final String KEY_TEST_VALUE = "test_value";

    public static final String FLAG_TEST_ID = "test_id";

    int role = -1;

    private Spinner activeFilter;
    private Spinner courseFilter;
    private Spinner typeFilter;

    private ActionMode actionMode;

    private int selectedCounter;

    private List<Boolean> checkedForDeletion;

    private TextView emptyListView;

    private ListView listView;

    private TestArrayAdapter adapter;

    private FirebaseAuth auth;

    private CompositeDisposable compositeDisposable;

    private List<Course> userCourses;

    private Map<String, Double> passedTests;

    //region action mode
    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.general_action_menu, menu);
            setActionModeTitle(mode);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menuitem_delete:

                    ArrayList<Test> remainingTests = new ArrayList<>();

                    for (int i = 0; i < checkedForDeletion.size(); i++) {
                        Test test = adapter.tests.get(i);
                        if (checkedForDeletion.get(i)) {
                            removeTest(test.getId(), test.getCourse());
                        } else {
                            remainingTests.add(test);
                        }
                    }
                    checkedForDeletion.clear();
                    adapter.refreshList(remainingTests);
                    for (int i = 0; i < remainingTests.size(); i++) {
                        checkedForDeletion.set(i, false);
                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.setInActionMode(false);
            selectedCounter = 0;
            for (int i = 0; i < checkedForDeletion.size(); i++) {
                checkedForDeletion.set(i, false);
            }
            actionMode = null;
        }
    };

    private void setActionModeTitle (@NonNull ActionMode mode) {
        mode.setTitle(getResources()
                .getQuantityString(R.plurals.selected, selectedCounter, selectedCounter));
    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tests_display);

        auth = FirebaseAuth.getInstance();

        compositeDisposable = new CompositeDisposable();

        FloatingActionButton fab = findViewById(R.id.fab_testDisplay_createNewTest);

        checkedForDeletion = new ArrayList<>();
        userCourses = new ArrayList<>();

        activeFilter = findViewById(R.id.spinner_testDisplay_active);
        courseFilter = findViewById(R.id.spinner_testDisplay_course);
        typeFilter = findViewById(R.id.spinner_testDisplay_type);

        listView = findViewById(R.id.listview_testDisplay_searchResults);
        emptyListView = findViewById(R.id.textview_testDisplay_emptyList);

        adapter = new TestArrayAdapter(this, new ArrayList<>());
        listView.setAdapter(adapter);

        role = getIntent().getIntExtra(FLAG_USER_ROLE, -1);

        final String[] activeList;
        if (role == ROLE_STUDENT) {
            fab.setVisibility(View.GONE);
            activeList = getResources().getStringArray(R.array.allStudentActiveTests);
        } else if (role == ROLE_ADMINISTRATOR || role == ROLE_TEACHING_STAFF){
            fab.setVisibility(View.VISIBLE);
            activeList = getResources().getStringArray(R.array.allTeacherActiveTests);
        } else {
            Toast.makeText(this, "invalid role", Toast.LENGTH_SHORT).show();
            activeList = new String[]{getString(R.string.all)};
        }
        ArrayAdapter<String> activeAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item_gravity_end,
                activeList);
        final String[] testTypeList = getResources().getStringArray(R.array.allTestTypes);
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item_gravity_end,
                testTypeList);

        activeFilter.setAdapter(activeAdapter);
        typeFilter.setAdapter(typeAdapter);

        addListeners(fab);
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (role == ROLE_TEACHING_STAFF) {
            compositeDisposable.add(Util.observeTeachersCourses(auth.getUid())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(courses -> {
                        setCourseFilterAdapter(courses);
                        TeacherFilterListener teacherFilterListener = new TeacherFilterListener();
                        activeFilter.setOnItemSelectedListener(teacherFilterListener);
                        courseFilter.setOnItemSelectedListener(teacherFilterListener);
                        typeFilter.setOnItemSelectedListener(teacherFilterListener);
                    }));
        } else if (role == ROLE_ADMINISTRATOR) {
            TextView courseFilterLabel = findViewById(R.id.textview_testDisplay_courseLabel);
            courseFilterLabel.setVisibility(View.GONE);
            courseFilter.setVisibility(View.GONE);
            compositeDisposable.add(Util.observeFilteredAllTests(getTestType(), getTestActive())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tests -> {
                        Util.setViewVisibilityAndUpdateAdapter(tests, emptyListView, listView,
                                adapter);
                        AdminFilterListener adminFilterListener = new AdminFilterListener();
                        activeFilter.setOnItemSelectedListener(adminFilterListener);
                        typeFilter.setOnItemSelectedListener(adminFilterListener);
                    })
            );
        } else if (role == ROLE_STUDENT) {
            passedTests = new HashMap<>();
            compositeDisposable.add(
                    Util.observeUserPassedTests(auth.getUid())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(passedTests -> this.passedTests = passedTests)
            );
            compositeDisposable.add(Util.observeStudentsCourses(auth.getUid())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(courses -> {
                        setCourseFilterAdapter(courses);
                        StudentFilterListener studentFilterListener = new StudentFilterListener();
                        activeFilter.setOnItemSelectedListener(studentFilterListener);
                        courseFilter.setOnItemSelectedListener(studentFilterListener);
                        typeFilter.setOnItemSelectedListener(studentFilterListener);
                    }));
        }
    }

    private void setCourseFilterAdapter(List<Course> courses) {
        userCourses = courses;
        String[] coursesArray = new String[courses.size()];
        for (int i = 0; i < courses.size(); i++) {
            coursesArray[i] = courses.get(i).formCourseIdentifier();
        }
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                R.layout.custom_spinner_item_gravity_end,
                coursesArray);
        courseFilter.setAdapter(spinnerAdapter);
    }

    private void addListeners(FloatingActionButton fab) {
        fab.setOnClickListener(view -> {
            Intent startTestCreate = new Intent(this, TestCreateActivity.class);
            startTestCreate.putExtra(FLAG_USER_ROLE, role);
            startActivity(startTestCreate);
        });

        listView.setOnItemClickListener((AdapterView<?> parent, View view,
                                         int position, long id) -> {
            ViewHolderItem holder = (ViewHolderItem) view.getTag();
            if (actionMode != null) {
                if (holder.deleteCheckbox.isChecked()) {
                    holder.deleteCheckbox.setChecked(false);
                    checkedForDeletion.set(position, false);
                } else {
                    holder.deleteCheckbox.setChecked(true);
                    checkedForDeletion.set(position, true);
                }
            } else {
                String title = holder.nameView.getText().toString();
                String testId = adapter.tests.get(position).getId();
                if (role == ROLE_STUDENT && !passedTests.keySet().contains(testId)) {
                    showStartDialog(title, holder.durationView.getText().toString(),
                            holder.pointsView.getText().toString(), testId);
                } else if (role == ROLE_TEACHING_STAFF || role == ROLE_ADMINISTRATOR) {
                    Intent startTestQuestions = new Intent(this,
                            TestQuestionsDisplayActivity.class);
                    startTestQuestions.putExtra(FLAG_TEST_QUESTIONS_TITLE, title);
                    startTestQuestions.putExtra(FLAG_TEST_ID, testId);
                    startTestQuestions.putExtra(FLAG_USER_ROLE, role);
                    startActivity(startTestQuestions);
                }

            }
        });

        listView.setOnItemLongClickListener((AdapterView<?> parent, View view,
                                             int position, long id) -> {
            if (actionMode == null && (role == ROLE_ADMINISTRATOR || role == ROLE_TEACHING_STAFF)) {
                actionMode = ((AppCompatActivity) view.getContext())
                        .startSupportActionMode(actionModeCallback);
                adapter.setInActionMode(true);
                ViewHolderItem holder = (ViewHolderItem) view.getTag();
                checkedForDeletion.set(position, true);
                holder.deleteCheckbox.setChecked(true);
                selectedCounter = 1;
            }
            return true;
        });
    }

    private void showStartDialog(String title, String duration, String points, String testId) {
        StartTestDialog dialog = new StartTestDialog();
        Bundle arguments = new Bundle();
        arguments.putString(FLAG_TEST_ID, testId);
        arguments.putString(KEY_TEST_TITLE, title);
        arguments.putString(KEY_TEST_DURATION, duration);
        arguments.putString(KEY_TEST_VALUE, points);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), StartTestDialog.class.getSimpleName());
    }

    private String getCourseId() {
        return userCourses.get(courseFilter.getSelectedItemPosition()).getId();
    }

    private int getTestType() {
        return typeFilter.getSelectedItemPosition() - 1;
    }

    private Boolean getTestActive() {
        Boolean active = null;
        if (activeFilter.getSelectedItemPosition() == 1) {
            return true;
        } else if (activeFilter.getSelectedItemPosition() == 2) {
            return false;
        }
        return active;
    }

    private void removeTest(String testId, String courseId) {
        Util.getRootReference().child(TESTS_TABLE).child(testId).removeValue()
                .addOnSuccessListener(aVoid -> {
            removeTestFromCourse(testId, courseId);
            removeTestFromQuestions(testId);
        });
    }

    private void removeTestFromCourse(String testId, String courseId) {
        Util.getRootReference().child(COURSES_TABLE).child(courseId).child(COURSES_CHILD_TESTS)
                .child(testId).removeValue();
    }

    private void removeTestFromQuestion(String testId, String questionId) {
        Util.getRootReference().child(QUESTIONS_TABLE).child(questionId).child(QUESTION_CHILD_TESTS)
                .child(testId).removeValue().addOnSuccessListener(aVoid -> {
                    removeQuestionIfNecessary(questionId);
        });
    }

    /**
     * Question that is not assigned to any test needs to be removed as questions have to be
     * associated with at least one test
     */
    private void removeQuestionIfNecessary(String questionId) {
        Scheduler thread = Schedulers.newThread();
        compositeDisposable.add(Util.observeQuestionsTests(questionId)
                .observeOn(thread)
                .subscribeOn(thread)
                .subscribe(tests -> {
                    if (tests.isEmpty()) {
                        Util.removeQuestionAndItsAnswers(questionId, thread, compositeDisposable);
                    }
                })
        );
    }

    private void removeTestFromQuestions(String testId) {
        Scheduler thread = Schedulers.newThread();
        compositeDisposable.add(Util.observeTestsQuestions(testId)
                .observeOn(thread)
                .subscribeOn(thread)
                .subscribe(questions -> {
                    for (Question question : questions) {
                        removeTestFromQuestion(testId, question.getId());
                    }
                })
        );
    }

    private class TestArrayAdapter extends RefreshAdapter<Test> {

        private Context context;
        private List<Test> tests;
        private boolean inActionMode;

        private TestArrayAdapter(Context context, List<Test> tests) {
            super(context, tests);
            this.context = context;
            this.tests = tests;
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ViewHolderItem holder;
            Test test = tests.get(position);

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.test_list_item, parent, false);

                holder = new ViewHolderItem();
                holder.nameView = convertView.findViewById(R.id.textview_testDisplay_test);
                holder.durationView = convertView.findViewById(R.id.textview_testDisplay_duration);
                holder.editLayout = convertView.findViewById(R.id.layout_testDisplay_edit);
                holder.pointsView = convertView.findViewById(R.id.textview_testDisplay_points);
                holder.editButtonView = convertView.findViewById(R.id.button_testDisplay_edit);
                holder.deleteCheckbox = convertView.findViewById(R.id.checkbox_testDisplay_delete);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolderItem) convertView.getTag();
                holder.deleteCheckbox.setOnCheckedChangeListener(null);
                holder.deleteCheckbox.setChecked(checkedForDeletion.get(position));
            }

            String dur = test.getDuration()+getString(R.string.minutes_acronym);
            String poi = "" + (test.getPointsValue() * test.getQuestionNumber());
            if (role == ROLE_STUDENT && passedTests.keySet().contains(test.getId())) {
                double score = passedTests.get(test.getId());
                poi = score + " / " + poi;
            }

            compositeDisposable.add(
                    Util.getCourseCode(test.getCourse())
                            .subscribe(courseCode ->
                                    holder.nameView.setText(test.formName(courseCode, context))
                            )
            );
            holder.durationView.setText(dur);
            holder.pointsView.setText(poi);

            if (role != ROLE_ADMINISTRATOR && role != ROLE_TEACHING_STAFF) {
                holder.editLayout.setVisibility(View.GONE);
            } else {
                holder.editButtonView.setOnClickListener( (View v) -> {
                    Intent startTestEdit = new Intent(context, TestCreateActivity.class);
                    startTestEdit.putExtra(FLAG_TEST_ID, test.getId());
                    startTestEdit.putExtra(FLAG_USER_ROLE, role);
                    startActivity(startTestEdit);
                });
            }

            holder.deleteCheckbox.setVisibility(inActionMode ? View.VISIBLE : View.GONE);
            holder.deleteCheckbox.setTag(String.valueOf(position));

            holder.deleteCheckbox.setOnCheckedChangeListener(
                    (CompoundButton buttonView, boolean isChecked) -> {
                        int pos = Integer.parseInt(buttonView.getTag().toString());
                        checkedForDeletion.set(pos, isChecked);
                        if (isChecked) {
                            selectedCounter++;
                            setActionModeTitle(actionMode);
                        } else {
                            selectedCounter--;
                            if (selectedCounter == 0) {
                                actionMode.finish();
                            } else {
                                setActionModeTitle(actionMode);
                            }
                        }
                    });

            return convertView;
        }

        void setInActionMode(boolean inActionMode) {
            this.inActionMode = inActionMode;
            notifyDataSetChanged();
        }

        @Override
        public void refreshList(List<Test> newTests) {
            tests.clear();
            checkedForDeletion.clear();
            if (newTests.isEmpty()) {
                Util.hideEmptyListAndShowEmptyView(emptyListView, listView);
            } else {
                tests.addAll(newTests);
                for (int i = 0; i < newTests.size(); i++) {
                    checkedForDeletion.add(false);
                }
                notifyDataSetChanged();
            }
        }
    }

    private static class ViewHolderItem {

        TextView nameView;
        TextView durationView;
        TextView pointsView;
        LinearLayout editLayout;
        Button editButtonView;
        CheckBox deleteCheckbox;
    }

    private class TeacherFilterListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            compositeDisposable.add(
                    Util.observeFilteredCoursesTests(getCourseId(), getTestType(), getTestActive())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(tests -> Util
                                    .setViewVisibilityAndUpdateAdapter
                                            (tests, emptyListView, listView, adapter)
                            )
            );
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class AdminFilterListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            compositeDisposable.add(
                    Util.observeFilteredAllTests(getTestType(), getTestActive())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(tests -> Util
                                    .setViewVisibilityAndUpdateAdapter
                                            (tests, emptyListView, listView, adapter)
                            )
            );
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class StudentFilterListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            switch (activeFilter.getSelectedItemPosition()) {
                case 0:
                    compositeDisposable.add(
                            Util.observeFilteredCoursesTests(getCourseId(), getTestType(), null)
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(tests -> {
                                        List<Test> activeOrPassedTests = new ArrayList<>();
                                        for (Test test : tests) {
                                            if (test.isActive()
                                                    || passedTests.keySet().contains(test.getId())) {
                                                activeOrPassedTests.add(test);
                                            }
                                        }
                                        Util.setViewVisibilityAndUpdateAdapter
                                                (activeOrPassedTests, emptyListView, listView, adapter);
                                            }
                                    )
                    );
                    break;
                case 1:
                    compositeDisposable.add(
                            Util.observeFilteredCoursesTests(getCourseId(), getTestType(), true)
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(tests -> {
                                        List<Test> activeTests = new ArrayList<>();
                                        for (Test test : tests) {
                                            if (test.isActive()
                                                    && !passedTests.keySet().contains(test.getId())) {
                                                activeTests.add(test);
                                            }
                                        }
                                        Util.setViewVisibilityAndUpdateAdapter
                                                (activeTests, emptyListView, listView, adapter);
                                            }
                                    )
                    );
                    break;
                case 2:
                    Scheduler backgroundThread = Schedulers.newThread();
                    compositeDisposable.add(
                            Util.observePassedTestIds(auth.getUid())
                                    .subscribeOn(backgroundThread)
                                    .observeOn(backgroundThread)
                                    .subscribe(passedIds -> compositeDisposable.add(
                                            Util.observeFilteredCoursesTests(getCourseId(),
                                                    getTestType(),null)
                                                    .subscribeOn(backgroundThread)
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(tests -> {
                                                        List<Test> passed = new ArrayList<>();
                                                        for (Test test : tests) {
                                                            if (passedIds.contains(test.getId())) {
                                                                passed.add(test);
                                                            }
                                                        }
                                                        Util.setViewVisibilityAndUpdateAdapter
                                                                (passed, emptyListView,
                                                                        listView, adapter);
                                                    })
                                    ))
                    );
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}
