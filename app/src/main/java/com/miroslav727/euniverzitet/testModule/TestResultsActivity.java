package com.miroslav727.euniverzitet.testModule;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.miroslav727.euniverzitet.R;

import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.KEY_TEST_TITLE;

public class TestResultsActivity extends AppCompatActivity {

    public static final String KEY_TEST_RESULT = "key_test_result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_results);

        String title = getString(R.string.finished_test_label)
                + getIntent().getStringExtra(KEY_TEST_TITLE);
        String result = getIntent().getStringExtra(KEY_TEST_RESULT);
        TextView titleView = findViewById(R.id.textview_testResults_title);
        titleView.setText(title);

        TextView resultView = findViewById(R.id.textview_testResults_result);
        resultView.setText(result);

        Button finish = findViewById(R.id.button_testResults_finish);
        finish.setOnClickListener(view -> {
            onBackPressed();
        });
    }
}
