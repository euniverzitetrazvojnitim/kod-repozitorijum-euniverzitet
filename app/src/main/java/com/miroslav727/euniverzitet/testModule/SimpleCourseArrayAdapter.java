package com.miroslav727.euniverzitet.testModule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.miroslav727.euniverzitet.R;
import com.miroslav727.euniverzitet.adminModule.RefreshAdapter;
import com.miroslav727.euniverzitet.model.Course;
import com.miroslav727.euniverzitet.util.Util;

import java.util.List;

public class SimpleCourseArrayAdapter extends RefreshAdapter<Course> {
    private Context context;
    private List<Course> courses;
    private TextView emptyView;
    private ListView courseSearchResults;

    public SimpleCourseArrayAdapter(@NonNull Context context, @NonNull List<Course> courses,
                             TextView emptyView, ListView courseSearchResults) {
        super(context, courses);
        this.context = context;
        this.courses = courses;
        this.emptyView = emptyView;
        this.courseSearchResults = courseSearchResults;
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        CourseViewHolderItem holder;

        Course course = courses.get(position);
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate
                    (R.layout.test_search_course_list_item, parent, false);

            holder = new CourseViewHolderItem();
            holder.resultView = convertView.findViewById
                    (R.id.textview_testCreate_courseSearch_item);

            convertView.setTag(holder);
        } else {
            holder = (CourseViewHolderItem) convertView.getTag();
        }

        holder.resultView.setText(course.formCourseIdentifier());


        return convertView;
    }

    @Override
    public void refreshList(List<Course> refreshedList) {
        courses.clear();
        if (refreshedList.isEmpty()) {
            Util.hideEmptyListAndShowEmptyView(emptyView, courseSearchResults);
        } else {
            courses.addAll(refreshedList);
            notifyDataSetChanged();
        }
    }

    public List<Course> getCourses() {
        return courses;
    }


    public static class CourseViewHolderItem {
        public TextView resultView;
    }
}
