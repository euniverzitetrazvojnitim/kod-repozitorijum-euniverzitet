package com.miroslav727.euniverzitet.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Question {

    private String course;
    private String description;
    private int type;
    private double pointsValue;
    private String correctAnswerId;
    @Exclude
    private String id;

    public Question() {
    }

    public Question(String course, String description, int type, double pointsValue,
                    String correctAnswerId, String id) {
        this.setCourse(course);
        this.setDescription(description);
        this.setType(type);
        this.setPointsValue(pointsValue);
        this.setCorrectAnswerId(correctAnswerId);
        this.setId(id);
    }


    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public double getPointsValue() {
        return pointsValue;
    }

    public void setPointsValue(double pointsValue) {
        this.pointsValue = pointsValue;
    }

    public String getCorrectAnswerId() {
        return correctAnswerId;
    }

    public void setCorrectAnswerId(String correctAnswerId) {
        this.correctAnswerId = correctAnswerId;
    }

    @Exclude
    public String getId() {
        return id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Question && this.id.equals(((Question)obj).id);
    }
}
