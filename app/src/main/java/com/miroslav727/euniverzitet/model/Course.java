package com.miroslav727.euniverzitet.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Course {

    private String code;
    private String name;
    @Exclude
    private String id;

    public Course() {
    }

    public Course(String code, String name, String id) {
        this.code = code;
        this.name = name;
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String formCourseIdentifier() {
        return getCode() + " - " + getName();
    }

    @Exclude
    public String getId() {
        return id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Course && this.id.equals(((Course)obj).id);
    }
}
