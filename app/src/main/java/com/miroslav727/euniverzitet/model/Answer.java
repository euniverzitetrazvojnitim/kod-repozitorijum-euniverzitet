package com.miroslav727.euniverzitet.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Answer {

    private String answer;
    private String question;
    @Exclude
    private String id;

    public Answer() {
    }

    public Answer(String answer, String question, String id) {
        this.setAnswer(answer);
        this.setQuestion(question);
        this.setId(id);
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Exclude
    public String getId() {
        return id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Answer && this.id.equals(((Answer)obj).id);
    }
}
