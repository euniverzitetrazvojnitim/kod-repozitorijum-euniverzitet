package com.miroslav727.euniverzitet.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Location {

    private String name;
    private String location;
    private int locationType;
    @Exclude
    private String id;

    public Location() {
    }

    public Location(String name, String location, int locationType, String id) {
        this.name = name;
        this.location = location;
        this.locationType = locationType;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getLocationType() {
        return locationType;
    }

    public void setLocationType(int locationType) {
        this.locationType = locationType;
    }

    @Exclude
    public String getId() {
        return id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Location && this.id.equals(((Location)obj).id);
    }
}
