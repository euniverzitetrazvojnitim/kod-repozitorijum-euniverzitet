package com.miroslav727.euniverzitet.model;

import androidx.annotation.Nullable;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.miroslav727.euniverzitet.util.Util;

import java.util.Date;

@IgnoreExtraProperties
public class Event implements Comparable<Event> {

    private String title;
    private String description;
    private long startDate;
    private int startHour;
    private int startMinute;
    private long endDate;
    private int endHour;
    private int endMinute;
    private int type;
    private String creatorId;
    private String locationId;
    private String courseId;
    @Exclude
    private String id;

    public Event() {
    }

    public Event(String title, String description, long startDate, int startHour, int startMinute,
                 long endDate, int endHour, int endMinute, int type, String creatorId,
                 String locationId, String courseId, String id) {
        this.title = title;
        this.description = description;
        this.startDate = startDate;
        this.startHour = startHour;
        this.startMinute = startMinute;
        this.endDate = endDate;
        this.endHour = endHour;
        this.endMinute = endMinute;
        this.type = type;
        this.creatorId = creatorId;
        this.locationId = locationId;
        this.courseId = courseId;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(int startMinute) {
        this.startMinute = startMinute;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(int endMinute) {
        this.endMinute = endMinute;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    @Exclude
    public String getId() {
        return id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return obj instanceof Event && this.id.equals(((Event)obj).id);
    }

    @Override
    public int compareTo(Event event) {
        Date start = new Date(Util.formTimeFromDateAndOffsets
                (this.startDate, this.startHour, this.startMinute));
        Date otherStart = new Date(Util.formTimeFromDateAndOffsets
                (event.startDate, event.startHour, event.startMinute));
        return start.compareTo(otherStart);
    }
}
