package com.miroslav727.euniverzitet.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User implements Comparable<User>{

    private String email;
    private int role;
    private String password;
    private boolean generic;
    private String locale;
    @Exclude
    private String uid;

    public User() {
    }

    public User(String email, int role, String uid, String password, boolean generic,
                String locale) {
        this.email = email;
        this.role = role;
        this.uid = uid;
        this.password = password;
        this.setGeneric(generic);
        this.setLocale(locale);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Exclude
    public String getUid() {
        return uid;
    }

    @Exclude
    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isGeneric() {
        return generic;
    }

    public void setGeneric(boolean generic) {
        this.generic = generic;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof User && this.uid.equals(((User)obj).uid);
    }

    @Override
    public int compareTo(User user) {
        return this.getEmail().compareTo(user.getEmail());
    }
}
