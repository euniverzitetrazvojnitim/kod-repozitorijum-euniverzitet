package com.miroslav727.euniverzitet.model;

import android.content.Context;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.miroslav727.euniverzitet.util.Util;

@IgnoreExtraProperties
public class Test {

    private String course;
    private int type;
    private int number;
    private int duration;
    private int questionNumber;
    private double pointsValue;
    private boolean active;
    @Exclude
    private String id;

    public Test() {}

    public Test(String course, int type, int number, int duration, int questionNumber,
                double pointsValue, boolean active, String id) {
        this.setCourse(course);
        this.setType(type);
        this.setNumber(number);
        this.setDuration(duration);
        this.setQuestionNumber(questionNumber);
        this.setPointsValue(pointsValue);
        this.setActive(active);
        this.setId(id);
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }

    public double getPointsValue() {
        return pointsValue;
    }

    public void setPointsValue(double pointsValue) {
        this.pointsValue = pointsValue;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Exclude
    public String getId() {
        return id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    public String formName(String courseCode, Context context) {
        return courseCode + " - " + Util.testTypeMapper(context, type) + " "+number;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Test && this.id.equals(((Test)obj).id);
    }
}
