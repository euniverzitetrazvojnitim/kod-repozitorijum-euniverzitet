package com.miroslav727.euniverzitet.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Date;

@IgnoreExtraProperties
public class Message implements Comparable<Message>{

    private String senderId;
    private String senderEmail;
    private String receiverId;
    private String receiverEmail;
    private long sentAt;
    private String text;
    @Exclude
    private String id;

    public Message() {}

    public Message(String senderId, String senderEmail, String receiverId, String receiverEmail,
                   long sentAt, String text, String id) {
        this.senderId = senderId;
        this.senderEmail = senderEmail;
        this.receiverId = receiverId;
        this.receiverEmail = receiverEmail;
        this.sentAt = sentAt;
        this.text = text;
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public long getSentAt() {
        return sentAt;
    }

    public void setSentAt(long sentAt) {
        this.sentAt = sentAt;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Exclude
    public String getId() {
        return id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Message && this.id.equals(((Message)obj).id);
    }

    @Override
    public int compareTo(Message message) {
        Date thisTime = new Date(this.sentAt);
        Date otherTime = new Date(message.sentAt);
        return thisTime.compareTo(otherTime);
    }
}
