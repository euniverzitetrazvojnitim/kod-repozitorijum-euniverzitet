package com.miroslav727.euniverzitet.model;

public class ChatMeta {

    private String lastMessage;
    private String senderEmail;
    private String senderId;
    private boolean read;

    public ChatMeta() {
    }

    public ChatMeta(String lastMessage, String senderEmail, String senderId, boolean read) {
        this.lastMessage = lastMessage;
        this.senderEmail = senderEmail;
        this.senderId = senderId;
        this.read = read;
    }


    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
