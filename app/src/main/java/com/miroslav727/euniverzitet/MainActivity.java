package com.miroslav727.euniverzitet;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.miroslav727.euniverzitet.adminModule.UsersActivity;
import com.miroslav727.euniverzitet.communicationsModule.InboxActivity;
import com.miroslav727.euniverzitet.eventsModule.EventsDisplayActivity;
import com.miroslav727.euniverzitet.login.LoginScreenActivity;
import com.miroslav727.euniverzitet.login.RegistrationActivity;
import com.miroslav727.euniverzitet.testModule.TestsDisplayActivity;
import com.miroslav727.euniverzitet.util.Util;

import java.util.Calendar;
import java.util.Set;

import io.reactivex.disposables.CompositeDisposable;

import static com.miroslav727.euniverzitet.testModule.TestsDisplayActivity.FLAG_USER_ROLE;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_ADMINISTRATOR;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_NON_TEACHING_STAFF;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_STUDENT;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEACHING_STAFF;
import static com.miroslav727.euniverzitet.util.Constants.ROLE_TEMP_USER;

public class MainActivity extends AppCompatActivity {

    public static final String FLAG_USER_REGISTERED = "user_registered";

    private FirebaseAuth auth;

    boolean loggedInUserExists;

    Button communicatorLinkButton;
    Button testsLinkButton;
    Button eventsLinkButton;
    Button administrationLinkButton;
    TextView privilegesView;

    private FirebaseUser loggedInUser;

    private boolean registered;

    private boolean isStartedFromLauncher;

    private int role;

    private int secretCounter;
    private long secretTime;
    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        secretCounter = 0;
        setContentView(R.layout.activity_main);

        compositeDisposable = new CompositeDisposable();

        isStartedFromLauncher = isStartedFromLauncher(savedInstanceState);

        auth = FirebaseAuth.getInstance();
        loggedInUser = auth.getCurrentUser();
        loggedInUserExists = auth.getCurrentUser() != null;

        registered = Util.getAppPreferences(this).getBoolean(FLAG_USER_REGISTERED, false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (loggedInUserExists && isStartedFromLauncher) {
            auth.signOut();
            loggedInUser = null;
            loggedInUserExists = false;
        }

        if (loggedInUser == null) {
            startActivity(new Intent(this, registered ? LoginScreenActivity.class :
                    RegistrationActivity.class));
        } else {
            String uid = loggedInUser.getUid();
            initSecretAdminEntry();
            initViews(uid);
            drawViews(uid);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        if (!compositeDisposable.isDisposed()) compositeDisposable.dispose();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_module_menu, menu);
        menu.findItem(R.id.menuitem_logout).setVisible(loggedInUserExists);
        if (loggedInUserExists) {
            String displayName = auth.getCurrentUser().getEmail();
            displayName = displayName.substring(0, displayName.indexOf("@"));
            menu.findItem(R.id.menuitem_username).setTitle(displayName);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case R.id.menuitem_logout:
                logoutActiveUser();
                startActivity(new Intent(this, LoginScreenActivity.class));
                return true;
            case R.id.menuitem_changePassword:
                Util.startPasswordChange(this, null, null);
                return true;
            default: return false;
        }
    }

    private void initViews(String uid) {
        communicatorLinkButton = findViewById(R.id.button_communicationsLink);
        testsLinkButton = findViewById(R.id.button_testsLink);
        eventsLinkButton = findViewById(R.id.button_eventsLink);
        administrationLinkButton = findViewById(R.id.button_administrationLink);
        privilegesView = findViewById(R.id.textview_privileges);

        communicatorLinkButton.setOnClickListener( (View v) -> {
            startActivity(new Intent(this, InboxActivity.class));
        });

        testsLinkButton.setOnClickListener( (View v) -> {
            Intent startTestIntent = new Intent(this, TestsDisplayActivity.class);
            startTestIntent.putExtra(FLAG_USER_ROLE, role);
            startActivity(startTestIntent);
        });

        eventsLinkButton.setOnClickListener( (View v) -> {
            Intent startEventsIntent = new Intent(this, EventsDisplayActivity.class);
            startEventsIntent.putExtra(FLAG_USER_ROLE, role);
            startActivity(startEventsIntent);
        });

        administrationLinkButton.setOnClickListener( (View v) -> {
            startActivity(new Intent(this, UsersActivity.class));
        });
    }

    private void drawViews(String uid) {
        privilegesView.setText(getString(R.string.checking_privileges));
        compositeDisposable.add(
                Util.getUserRole(uid).subscribe(role -> {
                    this.role = role;
                    switch (role) {
                        case ROLE_STUDENT:
                        case ROLE_TEACHING_STAFF:
                            communicatorLinkButton.setVisibility(View.VISIBLE);
                            testsLinkButton.setVisibility(View.VISIBLE);
                            eventsLinkButton.setVisibility(View.VISIBLE);
                            administrationLinkButton.setVisibility(View.GONE);
                            privilegesView.setVisibility(View.GONE);
                            break;
                        case ROLE_NON_TEACHING_STAFF:
                            communicatorLinkButton.setVisibility(View.VISIBLE);
                            testsLinkButton.setVisibility(View.GONE);
                            eventsLinkButton.setVisibility(View.VISIBLE);
                            administrationLinkButton.setVisibility(View.GONE);
                            privilegesView.setVisibility(View.GONE);
                            break;
                        case ROLE_ADMINISTRATOR:
                            communicatorLinkButton.setVisibility(View.VISIBLE);
                            testsLinkButton.setVisibility(View.VISIBLE);
                            eventsLinkButton.setVisibility(View.VISIBLE);
                            administrationLinkButton.setVisibility(View.VISIBLE);
                            privilegesView.setVisibility(View.GONE);
                            break;
                        case ROLE_TEMP_USER:
                        default:
                            communicatorLinkButton.setVisibility(View.GONE);
                            testsLinkButton.setVisibility(View.GONE);
                            eventsLinkButton.setVisibility(View.GONE);
                            administrationLinkButton.setVisibility(View.GONE);
                            privilegesView.setText(getString(R.string.no_privileges));
                            privilegesView.setVisibility(View.VISIBLE);
                            break;
                    }
                })
        );
    }

    private void logoutActiveUser() {
        auth.signOut();
    }

    /**
     * For testing purposes, allows any user to enter administration module by tapping on logo
     * 10 times in 5 seconds
     */
    private void initSecretAdminEntry() {
        ImageView logo = findViewById(R.id.image_mainModuleHeader);
        logo.setOnClickListener(view -> {
                secretCounter++;
                if (secretCounter == 1) {
                    secretTime = Calendar.getInstance().getTimeInMillis() / 1000;
                } else if (secretCounter >= 10) {
                    if ((Calendar.getInstance().getTimeInMillis() / 1000) - secretTime > 5) {
                        secretCounter = 0;
                        Toast.makeText(this, "not this time", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "welcome back 727", Toast.LENGTH_SHORT)
                                .show();
                        startActivity(new Intent(this, UsersActivity.class));
                    }

                }
        });
    }

    private boolean isStartedFromLauncher(Bundle savedInstanceState) {
        if (getIntent() == null) return false;
        boolean checkAction = Intent.ACTION_MAIN.equals(getIntent().getAction());
        Set<String> categories = getIntent().getCategories();
        boolean checkCategories = categories != null
                && categories.contains(Intent.CATEGORY_LAUNCHER);
        boolean checkBundle = savedInstanceState == null;
        return checkAction && checkCategories && checkBundle;
    }
}
